{extends file="frontend/index/index.tpl"}

{block name="frontend_index_content"}

    <div id="payment">
        <h3>Please wait while we redirect you...</h3>
        <form method="POST" action="https://finanzieren-tus.consorsfinanz.de/web/auto/calculator">
            <input type="hidden" name="vendorid" value="bundk">
            <input type="hidden" name="vehicleImageURL" value="https://bit.ly/2MUYqh4">
            <input type="hidden" name="prospectid" value="{$formData['prospect_id']}">
            <input type="hidden" name="Authorization" value="{$formData['token']}">
            ​
            {*<input type="submit">*}
        </form>
    </div>

    <script>
        setTimeout(function(){
            $('#payment form').submit();
        }, 1000);
    </script>
{/block}
