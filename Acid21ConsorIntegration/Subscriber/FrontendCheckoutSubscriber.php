<?php

namespace Acid21ConsorIntegration\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Controller_Request_RequestHttp;
use Enlight_Controller_Response_ResponseHttp;

class FrontendCheckoutSubscriber implements SubscriberInterface
{

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (position defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     * <code>
     * return array(
     *     'eventName0' => 'callback0',
     *     'eventName1' => array('callback1'),
     *     'eventName2' => array('callback2', 10),
     *     'eventName3' => array(
     *         array('callback3_0', 5),
     *         array('callback3_1'),
     *         array('callback3_2')
     *     )
     * );
     *
     * </code>
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_Checkout' => 'onFrontendCheckout',
            'Enlight_Controller_Action_PostDispatch_Frontend_Checkout' => 'onFrontendCheckout'
        ];
    }

    public function onFrontendCheckout(\Enlight_Event_EventArgs $args)
    {
        /** @var $request Enlight_Controller_Request_RequestHttp */
        $request = $args->getRequest();

        /** @var $response Enlight_Controller_Response_ResponseHttp */
        $response = $args->getResponse();

        if ($request->getActionName() !== 'shippingPayment'){
            return;
        }

        //Skip payment method selection to checkout final step
        $response->setRedirect('/checkout');
    }
}