<?php

namespace Acid21ConsorIntegration;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\DeactivateContext;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Models\Payment\Payment;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Shopware-Plugin Acid21ConsorIntegration.
 */
class Acid21ConsorIntegration extends Plugin
{

    const CONSOR_PAYMENT_NAME = 'acid21_consor_payment';

    const CONSOR_ACTION_NAME = 'Acid21ConsorPayment';

    const CONSOR_PLUGIN_NAME = 'Acid21ConsorIntegration';

    /**
    * @param ContainerBuilder $container
    */
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('acid21_consor_integration.plugin_dir', $this->getPath());
        parent::build($container);
    }

    /**
     * @param InstallContext $context
     */
    public function install(InstallContext $context)
    {
        $this->createConsorsPaymentMethod();
        $this->writeTranslation();
    }

    /**
     * @param UninstallContext $context
     */
    public function uninstall(UninstallContext $context)
    {
        $this->setActiveFlag($context->getPlugin()->getPayments(), false);
    }

    /**
     * @param DeactivateContext $context
     */
    public function deactivate(DeactivateContext $context)
    {
        $this->setActiveFlag($context->getPlugin()->getPayments(), false);
    }

    /**
     * @param ActivateContext $context
     */
    public function activate(ActivateContext $context)
    {
        $this->setActiveFlag($context->getPlugin()->getPayments(), true);
    }

    /**
     * @param Payment[] $payments
     * @param $active bool
     */
    private function setActiveFlag($payments, $active)
    {
        $em = $this->container->get('models');

        foreach ($payments as $payment) {
            $payment->setActive($active);
        }
        $em->flush();
    }

    private function createConsorsPaymentMethod()
    {
        $modelManager = $this->container->get('models');
        $payment = $modelManager->getRepository(Payment::class)->findOneBy([
            'name' => self::CONSOR_PAYMENT_NAME,
        ]);

        if ($payment === null) {
            //If the payment does not exist, we create a new one
            $payment = new Payment();
            $payment->setName(self::CONSOR_PAYMENT_NAME);
        }


        $payment->setActive(false);
        $payment->setPosition(-99);
        $payment->setDescription('Consors Finance Payment');
        $payment->setAdditionalDescription(
            "<img src='{link file='custom/plugins/Acid21ConsorIntegration/_public/images/header_logo.svg' fullPath}'/>"
            . '<div id="payment_desc">'
            . 'Bezahle mit Consors Finanz'
            . '</div>');
        $payment->setAction(self::CONSOR_ACTION_NAME);

        $modelManager->persist($payment);
        $modelManager->flush($payment);
    }

    private function writeTranslation(){
        /** @var \Shopware_Components_Translation $translation */
        $translation = $this->container->has('translation') ? $this->container->get('translation') : new \Shopware_Components_Translation();
        $translationKeys = $this->getTranslationKeys();

        $translation->write(
            2,
            'config_payment',
            $translationKeys[self::CONSOR_PLUGIN_NAME],
            [
                'additionalDescription' =>
                    "<img src='{link file='custom/plugins/Acid21ConsorIntegration/_public/images/header_logo.svg' fullPath}'/>"
                    . '<div id="payment_desc">'
                    . 'Pay with Consor '
                    . '</div>'
            ],
            true
        );
    }

    /**
     * @return array
     */
    private function getTranslationKeys()
    {
        $modelManager = $this->container->get('models');
        return $modelManager->getDBALQueryBuilder()
            ->select('name, id')
            ->from('s_core_paymentmeans', 'pm')
            ->where("pm.name = '" . self::CONSOR_PLUGIN_NAME . "'")
            ->execute()
            ->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

}
