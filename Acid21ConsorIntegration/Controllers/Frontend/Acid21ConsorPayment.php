<?php

use Acid21ConsorIntegration\Service\ConsorFormDataService;

class Shopware_Controllers_Frontend_Acid21ConsorPayment extends Shopware_Controllers_Frontend_Payment
{
    const PAYMENTSTATUSPAID = 12;

    public function preDispatch()
    {
        /** @var \Shopware\Components\Plugin $plugin */
        $plugin = $this->get('kernel')->getPlugins()['Acid21ConsorIntegration'];

        $this->get('template')->addTemplateDir($plugin->getPath() . '/Resources/views/');
    }

    /**
     * Index action method.
     *
     * Forwards to the correct action.
     */
    public function indexAction()
    {
        try {
            return $this->redirect(['action' => 'gateway', 'forceSecure' => true]);
        } catch (Exception $e) {
        }
    }

    /**
     * Gateway action method.
     *
     * Collects the payment information and transmit it to the payment provider.
     */
    public function gatewayAction()
    {
        $this->foreSaveOrder();
        $this->View()->assign('formData', $this->getFormData());
    }

    /**
     * Get the formData
     */
    private function getFormData()
    {
        /** @var ConsorFormDataService $service */
        $service = $this->container->get('acid21_consor_integration.form_data_service');

        $token = $service->getToken();
        if(!$token){
            $this->redirect(['controller' => 'checkout', 'action' => 'confirm']);
        }

        $prospect = $service->createProspect($token);
        if(!$prospect){
            $this->redirect(['controller' => 'checkout', 'action' => 'confirm']);
        }

        $prospect = $service->updateProspect($token, $prospect);
        if(!$prospect){
            $this->redirect(['controller' => 'checkout', 'action' => 'confirm']);
        }

        return [
            'token' => $token,
            'prospect_id' => $prospect
        ];
    }

    private function foreSaveOrder(){
        $user = $this->getUser();
        $basket = $this->getBasket();

        $order = Shopware()->Modules()->Order();
        $order->sUserData = $user;
        $order->sComment = Shopware()->Session()->sComment;
        $order->sBasketData = $basket;
        $order->sAmount = $basket['sAmount'];
        $order->sAmountWithTax = !empty($basket['AmountWithTaxNumeric']) ? $basket['AmountWithTaxNumeric'] : $basket['AmountNumeric'];
        $order->sAmountNet = $basket['AmountNetNumeric'];
        $order->sShippingcosts = $basket['sShippingcosts'];
        $order->sShippingcostsNumeric = $basket['sShippingcostsWithTax'];
        $order->sShippingcostsNumericNet = $basket['sShippingcostsNet'];
        $order->dispatchId = Shopware()->Session()->sDispatch;
        $order->sNet = empty($user['additional']['charge_vat']);
        $order->deviceType = $this->Request()->getDeviceType();

        try {
            $order->sSaveOrder();

        } catch (Enlight_Exception $e) {
            /** @var \Shopware\Components\Logger $logger */
            $logger = $this->container->get('pluginlogger');
            $logger->addError($e->getMessage());

            $this->redirect(['controller' => 'checkout', 'action' => 'confirm']);
        }
    }
}
