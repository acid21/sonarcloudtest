<?php

namespace Acid21ConsorIntegration\Service;

use Shopware\Components\HttpClient\GuzzleHttpClient;
use Shopware\Components\HttpClient\RequestException;
use Shopware\Components\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConsorFormDataService
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var GuzzleHttpClient
     */
    private $client;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @param ContainerInterface $container
     * @param GuzzleHttpClient $client
     * @param Logger $logger
     */
    public function __construct(
        ContainerInterface $container,
        GuzzleHttpClient $client,
        Logger $logger
    )
    {
        $this->container = $container;
        $this->client = $client;
        $this->logger = $logger;
    }

    public function getToken()
    {
        try {
            $response = $this->client->post(
                'https://services.consorsfinanz.de/common-services/cfg/token/bundk',
                [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                [
                    'username' => 'bundk',
                    'password' => 'Arnqv+1320',
                ]
            );

            $body = $response->getBody();
            $content = json_decode($body, true);
            return $content['token'];
        } catch (RequestException $e) {
            $this->logger->addError($e->getMessage());
            return null;
        }
    }

    public function createProspect($token)
    {

        $basket = Shopware()->Session()->sOrderVariables['sBasket'];

        $deliveryDate = new \DateTime();
        $deliveryDate->modify('+1 year');

        $registrationDate = new \DateTime($basket['content'][0]['datum']);

        $payload = json_encode([
            "nationalVehicleCode"=> 10384229,
            "type"=> "Mini",
            "registrationDate"=> $registrationDate->format('Y-m-d'),
            "mileage"=> 400,
            "brand"=> $basket['content'][0]['additional_details']['supplierName'],
            "model"=> substr($basket['content'][0]['articlename'],0,10),
            "price"=> $basket['content'][0]['additional_details']['price_numeric'],
            "downPaymentAmount"=> 500,
            "accessoriesPrice"=> 0,
            "deliveringDate"=> $deliveryDate->format('Y-m-d'),
            "partnersProspectIdentifier"=> "bundk",
            "effInterestRate"=> 5.99,
            "vin" => ""
        ]);

        $this->logger->addError("token: " . $token);
        $this->logger->addError($payload);

        try {
            $response = $this->client->post(
                'https://services.consorsfinanz.de/ratanet-api/cfg/prospect/vehicle/bundk?version=4.0',
                [
                    'Content-Type' => 'application/json',
                    'Authorization' => $token
                ],
                $payload
            );


            $body = $response->getBody();
            $this->logger->addError("BODY: " . $body);
            $content = json_decode($body, true);
            return $content['prospectIdentifier'];

        } catch (RequestException $e) {
            $this->logger->addError($e->getMessage());
            return null;
        }
    }

    public function updateProspect($token, $prospectIdentifier)
    {
        $userData = Shopware()->Session()->sOrderVariables['sUserData'];
        $user = $userData['additional']['user'];
        $shippingAddress = $userData['shippingaddress'];

        $payload = json_encode([
            'prospects'=> [
                [
                    'rolePlaying' => 'MAIN',
                    'gender' => $user['salutation'] === 'mr' ? 'MALE' : 'FEMALE',
                    'firstName' => $user['firstname'],
                    'lastName' => $user['lastname'],
                    'email' => $user['email'],
                    'nationality' => '276',
                    'contactAddress' => [
                        'street' => $shippingAddress['street'],
                        'zipcode' => $shippingAddress['zipcode'],
                        'city' => $shippingAddress['city']
                    ]
                ]
            ]
        ]);
        try {
            $response = $this->client->put(
                'https://services.consorsfinanz.de/ratanet-api/cfg/prospect/bundk/' . $prospectIdentifier . '?version=4.0',
                [
                    'Content-Type' => 'application/json',
                    'Authorization' => $token
                ],
                $payload
            );

            $body = $response->getBody();
            $this->logger->addError("BODY: " . $body);
            $content = json_decode($body, true);
            return $content['prospectIdentifier'];

        } catch (RequestException $e) {
            $this->logger->addError($e->getMessage());
            return null;
        }
    }


}
