<?

$textXpos = array();

function rgb2hex2rgb($c){
	if(!$c) return false;
	$c = trim($c);
	$out = false;
	if(preg_match("/^[0-9ABCDEFabcdef\#]+$/i", $c)){
		$c = str_replace('#','', $c);
		$l = strlen($c) == 3 ? 1 : (strlen($c) == 6 ? 2 : false);

		if($l){
			unset($out);
			$out[0] = $out['r'] = $out['red'] = hexdec(substr($c, 0,1*$l));
			$out[1] = $out['g'] = $out['green'] = hexdec(substr($c, 1*$l,1*$l));
			$out[2] = $out['b'] = $out['blue'] = hexdec(substr($c, 2*$l,1*$l));
		}else $out = false;

	}elseif (preg_match("/^[0-9]+(,| |.)+[0-9]+(,| |.)+[0-9]+$/i", $c)){
		$spr = str_replace(array(',',' ','.'), ':', $c);
		$e = explode(":", $spr);
		if(count($e) != 3) return false;
		$out = '#';
		for($i = 0; $i<3; $i++)
		$e[$i] = dechex(($e[$i] <= 0)?0:(($e[$i] >= 255)?255:$e[$i]));

		for($i = 0; $i<3; $i++)
		$out .= ((strlen($e[$i]) < 2)?'0':'').$e[$i];

		$out = strtoupper($out);
	}else $out = false;

	return $out;
}

function generateQr($vlg)
{
	global $textXpos,$font,$pdf_h,$pdf_b;
	$dpi=DPI;
	$qrFields = array(
		"firstname"=>"Vorname",
		"lastname"=>"Nachname",
		"title"=>"Anrede",
		"organization"=>"Firma",
		"role"=>"Position",
		"url"=>"Website",
		"email"=>"E-Mail",
		"email2"=>"Alternative E-Mail",
		"phone"=>"Telefon",
		"fax"=>"Telefax",
		"mobile"=>"Mobil",
		"street"=>"Stra�e und Haus-Nr.",
		"zip"=>"Mobil",
		"city"=>"Stadt",
		"state"=>"Bundesland",
		"country"=>"Land"
	);
	foreach($qrFields as $k=>$v)
	{
		if (!(strpos($vlg->qr_fields,$k)>0))
		$_SESSION['sess_fe_qrfield' . $k] = "";
		else
		$_SESSION['sess_fe_qrfield' . $k] = $_POST['qrField_' . $k];
	}

	require_once("qr/lib/qr_pdf.php");
	$pdf = pdf_new();
	pdf_open_file($pdf);



	// Creates a 50mm x 40mm PDF page
	pdf_begin_page($pdf, $pdf_b/$dpi, $pdf_h/$dpi);

	$farbe = explode(";", $vlg->qr_bgcolor);
	if (@$farbe[0] == "") $farbe[0] = 0;
	if (@$farbe[1] == "") $farbe[1] = 0;
	if (@$farbe[2] == "") $farbe[2] = 0;
	if (@$farbe[3] == "") $farbe[3] = 0;
	pdf_setcolor($pdf, "both", "cmyk", $farbe[0]/100,$farbe[1]/100,$farbe[2]/100,$farbe[3]/100);

	PDF_rect($pdf,0,0,$pdf_b/$dpi, $pdf_h/$dpi);
	PDF_fill($pdf);


	$contact = array();
	foreach($qrFields as $k=>$v)
	{
		$contact[$k]=$v;
	}

	$farbe = explode(";", $vlg->qr_farbe);
	if (@$farbe[0] == "") $farbe[0] = 0;
	if (@$farbe[1] == "") $farbe[1] = 0;
	if (@$farbe[2] == "") $farbe[2] = 0;
	$fgcolor = rgb2hex2rgb($farbe[0] . "," . $farbe[1] . "," . $farbe[2]);
	#echo $fgcolor;

	if (empty($vlg->qr_breite)||$vlg->qr_breite==0) die ("Bitte geben Sie eine korrekte Gr��e des QR-Codes an!");


	$qr_placed = placeQrPDF($pdf, array(
	        "contact" => $contact,
	        "qr" => array(
	            "left" => $vlg->qr_xpos, // in mm
	            "top" => $vlg->qr_ypos, // in mm
	            "size" => $vlg->qr_breite, // in mm
	            "filename" => dirname(__FILE__)."/temp/qr/qr2.png", // absolute path
	            "fg_color" => $fgcolor, // black
	//"bg_color" => "#FFF", // white
	            "bg_color" => "-1" // transparent
	)
	)
	);

	if(!$qr_placed)
	die("something went wrong, QR-code was not placed");

	pdf_end_page($pdf);
	pdf_close($pdf);

	$pdf_bin_data = pdf_get_buffer($pdf);

	return $pdf_bin_data;
}



include("lib/lib.inc.php");

$vid=$_GET['vid'];
if (!is_numeric($vid)) die("Bitte speichern Sie die Visitenkarte zun�chst ab!");
$v = new vorlage($vid);

$sql = "SELECT * FROM " . TBL_VORLAGEN . " WHERE id = $vid";

$res = new dbquery($sql);
$daten = $res->getNextRow();

$pdf_b = $daten['breite'];
$pdf_h = $daten['hoehe'];


$data = generateQr($v);


Header("Content-type: application/pdf");
header("Content-disposition: inline; filename=dummy.pdf");
header("Content-length: " . strlen($data));

echo $data;

?>

