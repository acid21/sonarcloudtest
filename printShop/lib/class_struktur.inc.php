<?php
class struktur extends assets
{
	var $tblName = TBL_STRUKTUR;
	var $tblNameAccess = "";
	
	var $isProtected = false;
	var $isProtectedByParent = false;
	
	var $benutzername = "";
	var $passwort = "";
	
	var $benutzernameFe = "";
	var $passwortFe = "";
	
	var $id;
	var $fields = array("parent","sprache","pos","area","inhalt", "hideInCms","name","hideInSite","contentType","cmsFile","contentFile","wlTarget","seo_description","seo_keywords","seo_title","seo_url","bild1","bild2");

	function __construct($cId = "new") 
	{
		$this->id = $cId;
		parent::__construct();
		
		$this->tblNameAccess = $this->tblName . "_access";
		if ($this->id != "new") $this->getData(); else $this->area="1";
	}
	

	function save()
	{
		$sql = "";
		$data = $this->getDataAsSQL();
		if ($this->id == "new")
		{
			$data = $this->getDataAsSQL();
			$sql = "INSERT INTO " . $this->tblName . " SET " . $data . ";";
			$query = new dbquery($sql);
			$this->id = mysql_insert_id($query->dbHandle);
		} else {
			$sql = "UPDATE " . $this->tblName . " SET " . $data . " WHERE id = $this->id;";
			$res = new dbquery($sql);
		}
		$this->saveAccess();
	}

	function getDataAsSQL()
	{
		$s = "";
		foreach($this->fields as $f)
		{
			$s .= "$f = '" . $this->$f . "', ";
		}
		
		$s = substr($s, 0, -2);
		return $s;	
	}
	function getData()
	{
		$sql = "SELECT * FROM $this->tblName WHERE id = " . $this->id . ";";
		$res = new dbquery($sql);
		$row = $res->getNextRow();
		foreach($this->fields as $f)
		{
			$this->$f = $row[$f];
		}
		$this->getAccess();
		$this->initializeAssets();
	}
	function delete()
	{
		$sql = "SELECT id FROM " . $this->tblName . " WHERE parent = " . $this->id . ";";
		$res = new dbquery($sql);
		while($row=$res->getNextRow())
		{
			$d=new struktur($row['id']);
			$d->delete();
		}
		$sql = "DELETE FROM $this->tblName WHERE id = $this->id LIMIT 1;";
		$res = new dbquery($sql);
		//$row = $res->getNextRow();
	}
	
	function insertAfter($titel)
	{
		$myParent = $this->parent;
		$sql = "UPDATE " . $this->tblName . " SET pos=pos+1 WHERE pos > " . $this->pos .  " AND parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . ";";
		$res = new dbquery($sql);
		$sql = "INSERT INTO " . $this->tblName . " (parent, area, sprache, name, pos ) VALUES (" . $myParent . "," . $this->area . "," . $this->sprache . ",'" . $titel . "'," . ($this->pos+1) . ");";
		$res = new dbquery($sql);
		return $res->getInsertId();
	}
	function insertBefore($titel)
	{
		$myParent = $this->parent;
		$sql = "UPDATE " . $this->tblName . " SET pos=pos+1 WHERE pos >= " . $this->pos .  " AND parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . ";";
		$res = new dbquery($sql);
		$sql = "INSERT INTO " . $this->tblName . " (parent, area, sprache, name, pos ) VALUES (" . $myParent . "," . $this->area . "," . $this->sprache . ",'" . $titel . "'," . ($this->pos) . ");";
		$res = new dbquery($sql);
		return $res->getInsertId();
	}
	function insertSubAfter($titel)
	{
		$myParent = $this->id;
		$sql = "UPDATE " . $this->tblName . " SET pos=pos+1 WHERE parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . ";";
		$res = new dbquery($sql);
		$sql = "INSERT INTO " . $this->tblName . " (parent, area, sprache, name, pos ) VALUES (" . $myParent . "," . $this->area . "," . $this->sprache . ",'" . $titel . "',1);";
		$res = new dbquery($sql);
		return $res->getInsertId();
	}
	
	function moveUp()
	{
		$myParent = $this->parent;
		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos < " . $this->pos .  " AND parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . " ORDER BY pos DESC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$prev = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $this->pos . " WHERE id = " . $prev['id'] . ";");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $prev['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}
	function moveDown()
	{
		$myParent = $this->parent;
		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos > " . $this->pos .  " AND parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . " ORDER BY pos ASC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$next = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $this->pos . " WHERE id = " . $next['id'] . ";");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $next['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}
	function moveToTop()
	{
		$myParent = $this->parent;
		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos < " . $this->pos .  " AND parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . " ORDER BY pos ASC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$prev = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = pos + 1 WHERE parent = " . $myParent . " AND sprache = " . $this->sprache . " ;");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $prev['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}
	function moveToBottom()
	{
		$myParent = $this->parent;
		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos > " . $this->pos .  " AND parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . " ORDER BY pos DESC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$next = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = pos - 1 WHERE pos>=" . $this->pos . " AND parent = " . $myParent . " AND area = " . $this->area . " AND sprache = " . $this->sprache . ";");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $next['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}	
	
	function repairAll($area)
	{
		$res = new dbquery("SELECT DISTINCT parent FROM " . $this->tblName . " WHERE area = $area;");
		while($row=$res->getNextRow())
		{
			$this->repairGroup($row['parent'], $area);
		}
	}
	function repairGroup($p, $area)
	{
		$sql = "SELECT * FROM " . $this->tblName . " WHERE parent = " . $p . " AND area = $area ORDER BY pos ASC, id ASC;";
		$res = new dbquery($sql);
		$i=1;
		while($row=$res->getNextRow())
		{
			$res2 = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $i . " WHERE id = " . $row['id'] . ";");
			$i++; 
		}
	}
	
	function isWeiterleitung()
	{
		return ($this->wlTarget>0);
	}

	function saveAccess()
	{
		$access = 0;
		if ($this->isProtected()) $access = 1;
		
		$sql = "REPLACE INTO " . $this->tblNameAccess . " (id, benutzername, passwort, aktiv) VALUES (" . $this->id . ",'" . $this->benutzername . "', '" . $this->passwort . "'," . $access . ");";
		$res = new dbquery($sql);
		
	}

	function getAccess()
	{
		return;
		$this->isProtected = false;
		$this->benutzername = "";
		$this->passwort = "";
		$this->benutzernameFe = "";
		$this->passwortFe = "";
		$this->getAccessOfId($this->id);
		if (!empty($this->parent)&&($this->parent>0))
		{
			$s = new struktur($this->parent);
			if ($s->isProtected()||$s->isProtectedByParent())
			{
				$this->isProtectedByParent = true;
				$this->benutzernameFe = $s->benutzernameFe;
				$this->passwortFe = $s->passwortFe;
			}
		}
	}

	function getAccessOfId($id)
	{
		$sql = "SELECT * FROM " . $this->tblNameAccess . " WHERE id = " . $id;
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$row=$res->getNextRow();
			$this->benutzername = $row['benutzername'];
			$this->passwort = $row['passwort'];
				
			if ($row['aktiv']=="1")
			{
				$this->isProtected = true;
				$this->benutzernameFe = $row['benutzername'];
				$this->passwortFe = $row['passwort'];
			}
		}
	}

	function isProtected()
	{
		if ($this->isProtected=="1"||$this->isProtected==true) return true;
		else return false;
	}
	
	function isProtectedByParent()
	{
		if ($this->isProtectedByParent=="1"||$this->isProtectedByParent==true) return true;
		else return false;
	}
}

?>
