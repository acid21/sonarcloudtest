<?
class shoppingBasket
{
	var $type = "session";
	
	var $iteratorCount=0;

	function shoppingBasket() 
	{
		if (!is_array($_SESSION['sess_basketContent'])) $_SESSION['sess_basketContent'] = array();
	}

	function reset()
	{
		$_SESSION['sess_basketContent'] = array();
	}

	function addItem($artikelId, $artikelNr, $preis, $anzahl)
	{
		if ($this->basketHasItem($artikelId))
		{
			$anzahl+=$this->getAnzahlOfItem($artikelId);
			$this->setAnzahlOfItem($artikelId,$anzahl);
		} else {
			array_push($_SESSION['sess_basketContent'],array("artikelId"=>$artikelId,"artikelName"=>$artikelNr,"preis"=>$preis,"anzahl"=>$anzahl));
		}
	}
	
	function addItemIndi($name, $beschreibung, $preis, $anzahl)
	{
		array_push(
			$_SESSION['sess_basketContent'],
			array("artikelId"=>"9999-".$this->getNextIndiId(),"artikelName"=>$name,"artikelBeschreibung"=>$beschreibung,"preis"=>$preis,"anzahl"=>$anzahl)
		);
	}
	function getNextIndiId()
	{
		$myId = 1;
		foreach($_SESSION['sess_basketContent'] as $k=>$v)
		{
			if (substr($v['artikelId'],0,5)=="9999-")
			{
				$nId = substr($v['artikelId'],5);
				$nId++;
				if ($nId>$myId) $myId=$nId;
				
			}
		}
		return $myId;
	}
	
	function removeItem($artikelId)
	{
		$out=array();
		foreach($_SESSION['sess_basketContent'] as $k=>$v)
		{
			if ($v['artikelId']!=$artikelId) array_push($out,$v);
		}
		$_SESSION['sess_basketContent']=$out;
	}
	
	function recalculate()
	{
		$out = array();
		foreach($_SESSION['sess_basketContent'] as $k=>$v)
		{
			$anzahlNew = $_POST['anzahl_' . $v['artikelId']];
			$v['anzahl'] = $anzahlNew;
			if ($v['anzahl']>0) array_push($out,$v);
		}
		$_SESSION['sess_basketContent']=$out;
	}
	
	function getCount()
	{
		return count($_SESSION['sess_basketContent']);
	}
	
	function getCountAsString()
	{
		if ($this->getCount()==0) return "keine"; else return $this->getCount();
	}
	
	function getValueOfBasket()
	{
		$gesamt=0;
		foreach($_SESSION['sess_basketContent'] as $k=>$v)
		{
			$gesamt += $v['anzahl']*$v['preis'];
		}
		return $gesamt;
	}
	
	function basketHasItem($artikelId)
	{
		foreach($_SESSION['sess_basketContent'] as $k=>$v)
		{
			if ($v['artikelId']==$artikelId) return true;
		}
		return false;
	}
	function getAnzahlOfItem($artikelId)
	{
		foreach($_SESSION['sess_basketContent'] as $k=>$v)
		{
			if ($v['artikelId']==$artikelId) return $v['anzahl'];
		}
		return false;
	}
	function setAnzahlOfItem($artikelId, $anzahl)
	{
		foreach($_SESSION['sess_basketContent'] as $k=>$v)
		{
			if ($v['artikelId']==$artikelId)
			{
				$_SESSION['sess_basketContent'][$k]['anzahl'] = $anzahl;
			}
		}
	}

	function getNextItem()
	{	
		if ($this->iteratorCount>=$this->getCount()) return false;
		
		$v=$_SESSION['sess_basketContent'][$this->iteratorCount];
		
		$this->iteratorCount++;
		return array("id"=>$v['artikelId'],"anzahl"=>$v['anzahl'],"name"=>$v['artikelName'],"beschreibung"=>$v['artikelBeschreibung'],"preis"=>$v['preis']);
	}
}
?>