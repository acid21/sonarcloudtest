<?php
class config
{
	var $tblName = TBL_CONFIG;
	var $name;
	var $value;

	function config($cId = "new") 
	{
		$this->name	= $cId;
		$this->getValueFromDb();
	}

	function getValueFromDb()
	{
		$sql = "SELECT value FROM " . $this->tblName . " WHERE name = '" . $this->name . "';";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$row = $res->getNextRow();
			$this->value = $row['value'];
		}
	}

	function getValue()
	{
		return $this->value;
	}

	function setValue($v)
	{
		$this->value = $v;
		$v = addslashes($v);
		
		$sql = "REPLACE INTO " . $this->tblName . " (name, value) VALUES ('" . $this->name . "','" . $v . "');";
		$res = new dbquery($sql);
	}
	
	function __toString() {
		return strval($this->value);
	}
}
?>
