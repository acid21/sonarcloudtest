<?php
class sprache
{
	var $tblName = TBL_SPRACHEN;
	var $id;
	var $fields = array("name","kuerzel","url");

	function sprache($cId = "new") 
	{
		$this->id 			= $cId; 	
		if ($this->id != "new") $this->getData();
	}

	function save()
	{
		$sql = "";
		$data = $this->getDataAsSQL();
		if ($this->id == "new")
		{
			$data = $this->getDataAsSQL();
			$sql = "INSERT INTO " . $this->tblName . " SET " . $data . ";";
			$query = new dbquery($sql);
			$this->id = mysql_insert_id($query->dbHandle);
		} else {
			$sql = "UPDATE " . $this->tblName . " SET " . $data . " WHERE id = $this->id;";
			$res = new dbquery($sql);
		}
	}

	function getDataAsSQL()
	{
		$s = "";
		foreach($this->fields as $f)
		{
			$s .= "$f = '" . $this->$f . "', ";
		}
		
		$s = substr($s, 0, -2);
		return $s;	
	}
	function getData()
	{
		$sql = "SELECT * FROM $this->tblName WHERE id = " . $this->id . ";";
		$res = new dbquery($sql);
		$row = $res->getNextRow();
		foreach($this->fields as $f)
		{
			$this->$f = $row[$f];
		}
	}
	function delete()
	{
		$sql = "SELECT id FROM " . $this->tblName . " WHERE parent = " . $this->id . ";";
		$res = new dbquery($sql);
		while($row=$res->getNextRow())
		{
			$d=new struktur($row['id']);
			$d->delete();
		}
		$sql = "DELETE FROM $this->tblName WHERE id = $this->id LIMIT 1;";
		$res = new dbquery($sql);
		//$row = $res->getNextRow();
	}
	
	function __toString() {
		return strval($this->name);
	}
}
?>