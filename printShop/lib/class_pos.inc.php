<?php

class pos
{
	var $tblName;
	var $filter = array();
	var $sqlFilter = "";
	var $pos;

	function __construct($tblName, $id, $filter) 
	{
		$this->id = $id; 	
		$this->tblName = $tblName;
		$this->filter = $filter;
		$this->getMyPos();
		
		$this->makeSqlFilter();
		
		$this->repairOrder();
	}
	
	function repairOrder()
	{
		$res = new dbquery("SELECT id FROM " . $this->tblName . " WHERE 1=1 " . $this->sqlFilter . " ORDER BY pos ASC, id DESC");
		$i=1;
		while($row=$res->getNextRow())
		{
			$res2 = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $i . " WHERE id = " . $row['id']);
			$i++;
		}
		$this->getMyPos();
	}
	
	function getMyPos()
	{
		$res = new dbquery("SELECT pos FROM " . $this->tblName . " WHERE id = " . $this->id);
		$row = $res->getNextRow();
		$this->pos = $row['pos'];
		echo $this->pos;
	}
	
	function makeSqlFilter()
	{
		$out = "";
		foreach($this->filter as $k=>$v)
		{
			$out .= " AND " . $k . " = '" . $v . "'"; 
		}
		$this->sqlFilter = $out;
	}

	function moveUp()
	{
		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos < " . $this->pos . $this->sqlFilter . " ORDER BY pos DESC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$prev = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $this->pos . " WHERE id = " . $prev['id'] . ";");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $prev['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}
	function moveDown()
	{

		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos > " . $this->pos . $this->sqlFilter . " ORDER BY pos ASC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$next = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $this->pos . " WHERE id = " . $next['id'] . ";");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $next['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}
	function moveToTop()
	{
		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos < " . $this->pos . $this->sqlFilter . " ORDER BY pos ASC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$prev = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = pos + 1 WHERE 1=1 " .  $this->sqlFilter  . " ;");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $prev['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}
	function moveToBottom()
	{
		$sql = "SELECT id, pos FROM  " . $this->tblName . " WHERE pos > " . $this->pos . $this->sqlFilter . " ORDER BY pos DESC;";
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$next = $res->getNextRow();
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = pos - 1 WHERE pos >=" . $this->pos .  $this->sqlFilter . ";");
			$res = new dbquery("UPDATE " . $this->tblName . " SET pos = " . $next['pos'] . " WHERE id = " . $this->id . ";");
			return true;
		} else return false;
	}
}

?>