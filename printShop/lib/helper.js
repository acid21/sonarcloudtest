function overSub()
{
	var w = document.viewport.getWidth();
	if (w == 0) {
		w = document.body.offsetWidth;
	}
	var newW = Math.floor(w/2)-170;
	$('subnavi').style.left = newW;
	$('subnavi').show();
}

function outSub()
{
	$('subnavi').hide();
}

function overSubKontakt()
{
	var w = document.viewport.getWidth();
	if (w == 0) {
		w = document.body.offsetWidth;
	}
	var newW = Math.floor(w/2)+20;
	$('subnaviStandorte').style.left = newW;
	$('subnaviStandorte').show();
}

function outSubKontakt()
{
	$('subnaviStandorte').hide();
}
function doM(s, shift)	{
	location.href="mailto:" + getM(s, shift);
}

function getM(s, shift)
{
	shift = shift;
	var n=0;
	var r="";
	for(var i=0;i<s.length;i++) { 
		n=s.charCodeAt(i); 
		if (n>=8364) {n = 128;}
		r += String.fromCharCode(n-(shift)); 
	}
	return r;
}

function showM(s, shift, d)	{
	var d;
	if (d) { d = d; } else { d = s; }
	document.write("<a href=\"javascript:doM('" + s + "', " + shift + ");\">" + getM(d,shift) + "</a>");	
}

function printM(s, shift)
{
	document.write(getM(s,shift));
}