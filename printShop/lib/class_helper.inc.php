<?php

class webhelper extends helper
{
	function webhelper()
	{
		
	}
	
	function moveDataset($id,$tbl,$b,$filter="")
	{
		if (in_array($b,array("moveToTop","moveToBottom","moveUp","moveDown")))
		{
			$id = $_GET['id'];
			$t = new pos($tbl,$id,$filter);
			if ($b=="moveToTop") $t->moveToTop();
			if ($b=="moveToBottom") $t->moveToBottom();
			if ($b=="moveUp") $t->moveUp();
			if ($b=="moveDown") $t->moveDown();
		}
	}
	
	function getCmsBgColor($c)
	{
		if ($c==CMS_BGCOLOR_1) return CMS_BGCOLOR_2;
		else return CMS_BGCOLOR_1;
	}
	
	function getTblDir($tbl)
	{
		if ($tbl==TBL_NEWS) return "news";
		return "default";
	}
	
	function cryptPw($s)
	{
		$s = md5($s);
		return $s;
	}
	
	function bool2Int($b)
	{
		if ($b=="true") return "1";
		return "0";
	}
	
	function encodeQs($s)
	{
		return CMS_MAINFILE . base64_encode($s);
	}
	function decodeQs($s)
	{
		return base64_decode($s);
	}
	
	function hasSubNavi($s)
	{
		$res = new dbquery("SELECT COUNT(*) as anzahl FROM " . TBL_STRUKTUR . " WHERE parent = " . $s . ";");
		$row = $res->getNextRow();
		return ($row['anzahl']>0);
	}
	
	function getUrl($id,$seoUrl)
	{
		if (USE_URLREWRITING&&!empty($seoUrl))
		{
			$url = PFAD_ROOT . $seoUrl;// . URL_SUFFIX;
		} else {
			$url = PFAD_ROOT . "index.php?c=" . $id;
		}
		return $url;
	}
	
	function getPath($p)
	{
		return $p;
	}
	
	function getValidFilename($s)
	{
		return preg_replace("/[^a-zA-Z0-9 _.]/","",$s);
	}
	
	function convertSprache($s)
	{
		if (is_numeric($s))
		{
			$d = new sprache($s);
			return $d->kuerzel;
		} else {
			$sql = "SELECT * FROM " . TBL_SPRACHEN . " WHERE kuerzel ='" . $s . "';";
			$res = new dbquery($sql);
			$row=$res->getNextRow();
			return $row['id'];
		}
	}
	
	function getHtmlOfStruktur($sId)
	{
		$sql = "SELECT * FROM " . TBL_CONTENT . " WHERE strukturId = " . $sId . " ORDER BY pos ASC";
		$res = new dbquery($sql);
		$out = "";
		while($row=$res->getNextRow())
		{
			if ($row['dataTyp']=="rte")
			{
				$d = new dataRte($row['dataId']);
				$out .= $d->getAsHtml();
			}
		}
		$out = $this->replaceEmail($out);
		return $out;
	}
	
	
	function getStruktur($p="",$area="",$l="")
	{
		if (empty($p)) $sqlParent = "AND parent = 0"; else $sqlParent = "AND parent = " . $p;
		if (!empty($l)) $sqlLng = "AND sprache = " . $l; else $sqlLng = "";
		if (!empty($area)) $sqlLng = "AND area = " . $area; else $sqlLng = "";
		// hideInNavi = 0
		$sql = "SELECT * FROM " . TBL_STRUKTUR . " WHERE 1=1 $sqlParent $sqlLng ORDER BY area, pos ASC";
		//if(DEBUG) echo $sql;
		$res = new dbquery($sql);
		return $res;
	}
	
	function getStrukturCms($p="",$area="",$l="0")
	{
		if (empty($p)) $sqlParent = "AND parent = 0"; else $sqlParent = "AND parent = " . $p;
		if (!empty($l)) $sqlLng = "AND sprache = " . $l; else $sqlLng = "";
		if (!empty($area)) $sqlLng .= " AND area = " . $area; else $sqlLng .= "";
		$sql = "SELECT * FROM " . TBL_STRUKTUR . " WHERE hideInCms = 0 $sqlParent $sqlLng ORDER BY area, pos ASC";
		$res = new dbquery($sql);
		return $res;
	}
	
	function getSprachen()
	{
		$sql = "SELECT * FROM " . TBL_SPRACHEN . " ORDER BY pos ASC";
		$res = new dbquery($sql);
		return $res;
	}
	
	function getLink($id)
	{
		$s = new struktur($id);
		if(is_numeric($s->wlTarget)&&($s->wlTarget>0))
		{
			$id = $s->wlTarget;
			$s = new struktur($id);
		}
		if (!empty($s->seo_url)) return $s->seo_url . URL_SUFFIX; else
		return "index.php?c=" . $id;
	}
	
	function getNaviActive($id)
	{
		$retArray = array();
		$res = new dbquery("SELECT * FROM " . TBL_STRUKTUR . " WHERE id = $id;");
		$row = $res->getNextRow();
		$retArray['lang'] = $row['sprache'];
		$retArray['main'] = $row['id'];
		$retArray['current'] = $row['id'];
		$retArray['sub'] = $row['id'];
		$retArray['sub2'] = $row['id'];
		if (is_numeric($row['parent'])&&$row['parent']>0)
		{
			$res = new dbquery("SELECT * FROM " . TBL_STRUKTUR . " WHERE id = " . $row['parent'] . ";");
			$row = $res->getNextRow();
			$retArray['main'] = $row['id'];
		}
		if (is_numeric($row['parent'])&&$row['parent']>0)
		{
			$retArray['sub'] = $row['id'];
			$res = new dbquery("SELECT * FROM " . TBL_STRUKTUR . " WHERE id = " . $row['parent'] . ";");
			$row = $res->getNextRow();
			$retArray['main'] = $row['id'];
		}
		if (is_numeric($row['parent'])&&$row['parent']>0)
		{
			$retArray['sub'] = $row['id'];
			$res = new dbquery("SELECT * FROM " . TBL_STRUKTUR . " WHERE id = " . $row['parent'] . ";");
			$row = $res->getNextRow();
			$retArray['main'] = $row['id'];
		}
		
		return $retArray;
		
	}
	
	function getIdOfUrl($url)
	{
		if (is_numeric($url)) return $url; 
		/*$res = new dbquery("SELECT * FROM " . TBL_STRUKTUR . " WHERE urlId = '$url'");
		if ($res->getRowCount()>0)
		{
			$row = $res->getNextRow();
			return $row['id'];
		} else {*/
			$res = new dbquery("SELECT * FROM " . TBL_STRUKTUR . " WHERE seo_url = '$url'");
			if ($res->getRowCount()>0)
			{
				$row = $res->getNextRow();
				return $row['id'];
			} else {
				die("404");
				return false;
			}
		//}
	}

	function isUrlAvailable($url)
	{
		$res = new dbquery("SELECT COUNT(*) as anzahl FROM " . TBL_STRUKTUR . " WHERE seo_url = '" . $url . "'");
		$row = $res->getNextRow();
		if ($row['anzahl']>0) return false; else return true;
	}

	function findAnyLink($s)
	{
		return preg_replace_callback('/<a[\s]+[^>]*?href[\s]?=[\s\"\']+(.*?)[\"\']+.*?>([^<]+|.*?)?<\/a>/i',array('webhelper','encodeEmail'), $s);
		return preg_replace_callback('/[a-z0-9._-]+@([a-z0-9-]{2,255}\.)+[a-z]{2,5}/i', array('webhelper','encodeEmail'), $s);	
	}
	function replaceEmail($s)
	{
		$s = preg_replace_callback('/<a[\s]+[^>]*?href[\s]?=[\s\"\']mailto:+(.*?)[\"\']+.*?>([^<]+|.*?)?<\/a>/i',array('webhelper','encodeFullEmail'), $s);
		return preg_replace_callback('/[a-z0-9._-]+@([a-z0-9-]{2,255}\.)+[a-z]{2,5}/i', array('webhelper','encodeStringEmail'), $s);	
	}
	function encodeFullEmail($s)
	{
		$link = $s[0];
		$email = $s[1];
		$linktext = $s[2];
		$out = "";
		$emailCrypt = $this->encodeEmail($email);
		
		$out = str_ireplace("mailto:" . $email,"javascript:doM('" . $emailCrypt . "'," . SPAMPROTECT_EMAIL_OFFSET . ");", $link);
		$out = str_ireplace($email,"<script>" . CRLF . "<!--" . CRLF . "printM('" . $emailCrypt . "'," . SPAMPROTECT_EMAIL_OFFSET . ");" . CRLF . "//-->" . CRLF . "</script><noscript>Bitte aktivieren Sie Javascript zum Anzeigen der E-Mail Adresse. Please activate Javascript to show the e-mail address.</noscript>", $out);
		
		return $out;
	}
	
	function encodeStringEmail($s)
	{
		$email = $s[0];
		$out = "";
		$emailCrypt = $this->encodeEmail($email);
		
		$out = "<a href=\"javascript:doM('" . $emailCrypt . "'," . SPAMPROTECT_EMAIL_OFFSET . ");\"><script><!--" . CRLF . "printM('" . $emailCrypt . "'," . SPAMPROTECT_EMAIL_OFFSET . ");" . CRLF . "//--></script></a>";
		return $out;
	}
	function encodeEmail($email)
	{
		for($i=0;$i<strlen($email);$i++)
		{
			$v=$email[$i];
			$emailCrypt .= chr(ord($v)+SPAMPROTECT_EMAIL_OFFSET);
		}
		$emailCrypt = str_replace("'","\\'",$emailCrypt);
		return $emailCrypt;
	}
	
	function initializeTinymce($element="rteData",$width=CMS_RTE_WIDTH, $height=CMS_RTE_HEIGHT)
	{
?>
<script type="text/javascript" src="modules/tiny_mce/tiny_mce_gzip.js"></script> 
<script type="text/javascript"> 
<!--
tinyMCE_GZ.init({ 
	plugins : 'safari,media,advimage,print,contextmenu,paste,noneditable,nonbreaking,table,inlinepopups,advlinks', 
	themes : 'advanced', 
	languages : 'de', 
	disk_cache : true, 
	debug : false 
});
//-->
</script> 

<script type="text/javascript">
<!--

tinyMCE.init({
	mode : "exact",
	elements : "<? echo $element; ?>",
	language : "de",
	width : "<? echo $width; ?>",
	height : "<? echo $height; ?>",
	plugins : "safari,media,advimage,print,contextmenu,paste,noneditable,nonbreaking,table,inlinepopups,advlinks",
	theme : "advanced",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_blockformats : "p,h1,h2",
	content_css : "cms/rte_style.css",
	document_base_url : "<? echo CMS_RTE_DOCBASE; ?>",
	
	relative_urls : true,
	convert_urls:true,

	theme_advanced_buttons1 : "formatselect,bold,italic,underline,strikethrough,separator,redo,undo,separator,cut,copy,paste,separator,link,unlink,separatorredo,separator,bullist,numlist",
	theme_advanced_buttons2 : "image,table,separator,code",
	theme_advanced_buttons3 : "",

	file_browser_callback : 'myFileBrowser',
	
	external_link_list_url : "cms/linklist.php",

	paste_auto_cleanup_on_paste : true,
	paste_convert_headers_to_strong : true

});

  
  
function myFileBrowser (field_name, url, type, win) {

	// alert("Field_Name: " + field_name + "\nURL: " + url + "\nType: " + type + "\nWin: " + win); // debug/testing

	var assetmanager = "<? echo URL_CMS; ?>/modules/assetmanager/assetmanager.php";

	//var cmsURL = window.location.toString();    // script URL - use an absolute path!
	var cmsURL = assetmanager;
	if (cmsURL.indexOf("?") < 0) {
	    //add the type as the only query parameter
	    cmsURL = cmsURL  + "?type=" + type;
	}
	else {
	    //add the type as an additional query parameter
	    // (PHP session ID is now included if there is one at all)
	    cmsURL = cmsURL  + "&type=" + type;
	}

tinyMCE.activeEditor.windowManager.open({
    file : cmsURL,
    title : 'Datei-Manager',
    width : 640,  // Your dimensions may differ - toy around with them!
    height : 440,
    resizable : "yes",
    inline : "yes",  // This parameter only has an effect if you use the inlinepopups plugin!
    close_previous : "no"
    }, {
        window : win,
        input : field_name
    });
    return false;
  }

//-->
</script>
<?
	}

}



class helper 
{
	function helper() 
	{
		return true;
	}
	
	function niceHtml($s)
	{
		//return $s;
		$s = htmlentities($s, ENT_QUOTES);
		return $s;
	}
	
	function  niceDbString($s)
	{
		if (get_magic_quotes_gpc()==0) $s = addslashes($s);
		return $s;
	}
	
	function shortStr($s,$l,$suffix="...")
	{
		if (strlen($s)<=$l) return $s;
		return substr($s,0,$l).$suffix;
	}
	
	function removeFromQuerystring($qs, $param)
	{
		$qs = explode("&", $qs);
		$newqs = "";
		for($i=0;$i<count($qs);$i++)
		{
			$t = explode("=", $qs[$i]);
			if ($param!=$t[0]&&!empty($t[1])) $newqs .= $qs[$i] . "&";
		}
		if (substr($newqs,0,-1)=="&") $newqs = substr($newqs,0,strlen($newqs)-1);
		return $newqs;
	
	}
	
	function niceInputOut($s)
	{
		$s = htmlspecialchars($s);
		return $s;
	}
	
	function showJsMeldung($s)
	{
?>
<script type="text/javascript">
<!--
alert("<? echo str_replace("\"","'",$s); ?>");
//-->
</script>
<?
	}
	
	function niceInhalt($s)
	{
		//$s = htmlspecialchars($s, ENT_QUOTES);
		return $s;
		$s=stripslashes($s);
		$s = str_replace("<", ";;,;;", $s);
		$s = str_replace(">", ",,;,,", $s);
		$s = htmlentities($s, ENT_NOQUOTES);
		$s = str_replace(",,;,,", ">", $s);
		$s = str_replace(";;,;;", "<", $s);
		$s = str_replace("&amp;nbsp;", "&nbsp;", $s);
		$s = str_replace("&amp;amp;", "&amp;", $s);
		
		return $s;
	}
	
	function noUmlauteStr($s)
	{
		$s = str_replace(" ", "_", $s);
		$s = str_replace("�", "ae", $s);
		$s = str_replace("�", "ue", $s);
		$s = str_replace("�", "oe", $s);
		$s = str_replace("�", "Ae", $s);
		$s = str_replace("�", "Oe", $s);
		$s = str_replace("�", "Ue", $s);
		$s = str_replace("�", "ss", $s);
		$s = str_replace(",", "-", $s);
		$s = str_replace(".", "", $s);
		$s = str_replace("/", "-", $s);
		$s = str_replace("-", "-", $s);
		$s = str_replace("+", "und", $s);
	
		return $s;
	}	

	function zufallsstring($length, $possible_charactors = "ABCDEFGHIJKLMNPQRSTUVWXYZ1239456789")
	{
		$string = "";
		while(strlen($string)<$length) {
			$string .= substr($possible_charactors,(rand()%(strlen($possible_charactors))),1);
		}
		return($string);
	}
	
	function imgIsJPEG($file)
	{
		$imgInfo = getimagesize($file);
		if ($imgInfo[2] == 2) return true;
		return false;
	}
	
	function imgIsGIF($file)
	{
		$imgInfo = getimagesize($file);
		if ($imgInfo[2] == 1) return true;
		return false;
	}
	
	function imgGetHeight($file)
	{
		$imgInfo = getimagesize($file);
		return $imgInfo[1];
	}
	
	function imgGetWidth($file)
	{
		$imgInfo = getimagesize($file);
		return $imgInfo[0];
	}
	

	function getNiceSize($mySize)
	{
		$einheit = "Byte";
		$nkz=0;
		if ($mySize > 950)
		{
			$mySize = $mySize/1024;
			$einheit = "KB";
			$nkz=2;
		}
		if ($mySize > 950)
		{
			$mySize = $mySize/1024;
			$einheit = "MB";
			$nkz=2;
		}
		if ($mySize > 950)
		{
			$mySize = $mySize/1024;
			$einheit = "GB";
			$nkz=2;
		}
		return number_format($mySize,$nkz,",",".") . " " . $einheit;
	}

	function getNiceDate($myDate)
	{
		$monat = "";
		switch (date("n",$myDate))
		{
			case "1":
			$monat = "Januar";
			break;
			case "2":
			$monat = "Februar";
			break;
			case "3":
			$monat = "M�rz";
			break;
			case "4":
			$monat = "April";
			break;
			case "5":
			$monat = "Mai";
			break;
			case "6":
			$monat = "Juni";
			break;
			case "7":
			$monat = "Juli";
			break;
			case "8":
			$monat = "August";
			break;
			case "9":
			$monat = "September";
			break;
			case "10":
			$monat = "Oktober";
			break;
			case "11":
			$monat = "November";
			break;
			case "12":
			$monat = "Dezember";
			break;
		}
	
		$wochentag = "";
		switch (date("w",$myDate))
		{
			case "0":
			$wochentag = "Sonntag";
			break;
			case "1":
			$wochentag = "Montag";
			break;
			case "2":
			$wochentag = "Dienstag";
			break;
			case "3":
			$wochentag = "Mittwoch";
			break;
			case "4":
			$wochentag = "Donnerstag";
			break;
			case "5":
			$wochentag = "Freitag";
			break;
			case "6":
			$wochentag = "Samstag";
			break;
		}
	
		return $wochentag . ", " . date("d", $myDate) . ". $monat ". date(" Y, G:i", $myDate) . " Uhr";
	}	

} // class helper



class dirInfo
{
	var $dir;
	var $hdl=false;
	var $fileArray=false;
	var $fileWalker=0;
	var $dirSize=0;
	var $numberOfFiles=0;

	function dirInfo($newDir)
	{
		if (!is_dir($newDir)) die ("Ung�ltiges Verzeichnis!");
		$this->dir = $newDir;
		$this->fileArray = $this->GetDirArray($this->dir);
	}

	function getNextFile()
	{
		if (!$this->fileArray) $this->fileArray = $this->GetDirArray($this->dir);
		if ($this->fileWalker<count($this->fileArray))
		{
			return $this->fileArray[$this->fileWalker++];
		}
		else return false;
	}

	function getDirSize()
	{
		return getNiceSize($this->dirSize);
	}

	function getNumberOfFiles()
	{
		return $this->numberOfFiles;
	}


	function GetDirArray($sPath)
	{
		//Load Directory Into Array
		$retVal = array();
		$handle=opendir($sPath);
		while ($file = readdir($handle))
        	if ($file != "." && $file != ".." && !is_dir($sPath . "/" . $file))
        	{
        		$retVal[count($retVal)] = $file;
        		$this->numberOfFiles++;
        		$this->dirSize = $this->dirSize + filesize($sPath . "/" . $file);
        	}

		//Clean up and sort
		closedir($handle);
		natcasesort($retVal);
		$retVal = array_values($retVal);
		return $retVal;
	}

} // class dirInfo


class fileInfo
{
	var $file;
	var $hdl;

	var $filename = "";
	var $extension = "";
	var $noExtension = "";
	var $chgDate = "";
	var $size = 0;

	var $niceChgDate = "";
	var $niceSize = "";
	var $niceType = "";

	function fileInfo($newFile)
	{
		if (!file_exists($newFile)) die ($newFile . " ist keine g�ltige Datei!");
		$this->file = $newFile;
		$this->getProperties();
	}

	function getProperties()
	{
		$this->filename = basename($this->file);
		$this->extension = substr(strrchr($this->file, "."), 1);
		$this->noExtension = basename($this->file, $this->extension);
		$this->chgDate = filemtime($this->file);
		$this->size = filesize($this->file);

		$this->niceChgDate = getNiceDate($this->chgDate);
		$this->niceSize = getNiceSize($this->size);
		$this->niceType = $this->getNiceType();
	}

	function getNiceType()
	{
		switch (strtoupper($this->extension))
		{
			case "JPG":
			case "JPEG":
			return "JPEG-Bild";
			case "GIF":
			return "GIF-Bild";
			case "DOC":
			return "Word-Dokument";
			case "PDF":
			return "PDF-Dokument";
			default:
			return strtoupper($this->extension) . "-Datei";
		}
	}

} // class fileInfo
?>
