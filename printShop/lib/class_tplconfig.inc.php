<?php
class tplconfig
{
	var $tblName = TBL_TPLCONFIG;
	var $id=false;
	var $name;
	var $value="";
	var $websiteId = "";

	function tplconfig($cId = "new", $websiteId=false) 
	{
		if ($websiteId===false)
		{
			if (isset($_SESSION['sess_webId'])&&($_SESSION['sess_webId']>0))
			$this->websiteId = $_SESSION['sess_webId'];
			else die("class tplconfig: websiteId nicht gesetzt!");
		} else $this->websiteId = $websiteId;
		$this->name	= $cId;
		$this->getValueFromDb();
	}

	function getValueFromDb()
	{
		$sql = "SELECT id, value FROM " . $this->tblName . " WHERE name = '" . $this->name . "' AND websiteId = " . $this->websiteId . ";";
		
		$res = new dbquery($sql);
		if ($res->getRowCount()>0)
		{
			$row = $res->getNextRow();
			$this->value = $row['value'];
			$this->id = $row['id'];
		}
	}

	function getValue()
	{
		return $this->value;
	}

	function setValue($v)
	{
		$this->value = $v;
		$v = webhelper::niceDbString($v);
		if (!$this->id)
		{
			$sql = "INSERT INTO " . $this->tblName . " (websiteId, name, value) VALUES (" . $this->websiteId . ", '" . $this->name . "','" . $v . "');";
			$res = new dbquery($sql);
			$this->id = $res->getInsertId();
		} else {
			$sql = "UPDATE " . $this->tblName . " SET value = '" . $v . "' WHERE id = " . $this->id . ";";
			$res = new dbquery($sql);
		}
	}
	function __toString() {
		return strval($this->value);
	}
}
?>
