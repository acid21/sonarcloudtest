<?php

class order
{
	var $tblName = "sg_orders";
	var $id;
	var $fields = array("mail","summe","payment","shipping","readr_name","readr_adresse","readr_telefon","readr_email","lfadr_name","lfadr_adresse","lfadr_telefon","lfadr_email","kommentar");

	function order($cId = "new") 
	{
		$this->id 			= $cId; 
		if ($this->id != "new") $this->getData();
	}

	function addArtikel($artikelId,$preis=1,$menge=1,$beschreibung="")
	{
		$t = new orderArticle();
		$t->orderId = $this->id;
		$t->artikelId = $artikelId;
		$t->menge = $menge;
		$t->preis = $preis;
		$t->beschreibung = $beschreibung;
		$t->save();
	}
	
	function save()
	{
		$sql = "";
		$data = $this->getDataAsSQL();
		if ($this->id == "new")
		{
			$data = $this->getDataAsSQL();
			$sql = "INSERT INTO " . $this->tblName . " SET " . $data . ";";
			$query = new dbquery($sql);
			$this->id = mysql_insert_id($query->dbHandle);
		} else {
			$sql = "UPDATE " . $this->tblName . " SET " . $data . " WHERE id = $this->id;";
			$res = new dbquery($sql);
		}
	}

	function getDataAsSQL()
	{
		$s = "";
		foreach($this->fields as $f)
		{
			$s .= "$f = '" . $this->$f . "', ";
		}
		
		$s = substr($s, 0, -2);
		return $s;	
	}
	function getData()
	{
		$sql = "SELECT * FROM $this->tblName WHERE id = " . $this->id . ";";
		$res = new dbquery($sql);
		$row = $res->getNextRow();
		foreach($this->fields as $f)
		{
			$this->$f = $row[$f];
		}
	}
	function delete()
	{
		$sql = "DELETE FROM $this->tblName WHERE id = $this->id LIMIT 1;";
		$res = new dbquery($sql);
		//$row = $res->getNextRow();
	}
	
}

?>
