<?php

class benutzer
{
	var $tblName = TBL_BENUTZER;
	var $id;
	var $fields = array("vorname","nachname","login", "passwort", "passwortCrypt","aktiv","isAdmin");
	var $access = array();

	function benutzer($cId = "new") 
	{
		$this->id 			= $cId; 	
		if ($this->id != "new") $this->getData();
	}

	function save()
	{
		$sql = "";
		$data = $this->getDataAsSQL();
		if ($this->id == "new")
		{
			$data = $this->getDataAsSQL();
			$sql = "INSERT INTO " . $this->tblName . " SET " . $data . ";";
			$query = new dbquery($sql);
			$this->id = mysql_insert_id($query->dbHandle);
		} else {
			$sql = "UPDATE " . $this->tblName . " SET " . $data . " WHERE id = $this->id;";
			$res = new dbquery($sql);
		}
		if (DEBUG) echo $sql;
	}

	function getDataAsSQL()
	{
		$s = "";
		foreach($this->fields as $f)
		{
			$s .= "$f = '" . $this->$f . "', ";
		}
		
		$s = substr($s, 0, -2);
		return $s;	
	}
	function getData()
	{
		$sql = "SELECT * FROM $this->tblName WHERE id = " . $this->id . ";";
		$res = new dbquery($sql);
		$row = $res->getNextRow();
		foreach($this->fields as $f)
		{
			$this->$f = $row[$f];
		}
		$this->getAccess();
	}
	function delete()
	{
		$sql = "DELETE FROM $this->tblName WHERE id = $this->id LIMIT 1;";
		$res = new dbquery($sql);
		//$row = $res->getNextRow();
	}
	function resetAccess()
	{
		if (!is_numeric($this->id)) return false;
		$res = new dbquery("DELETE FROM " . TBL_RECHTE . " WHERE userId = " . $this->id);
	}
	function addAccess($s)
	{
		if (!is_numeric($this->id)) return false;
		$res = new dbquery("INSERT INTO " . TBL_RECHTE . " (userId, strukturId, access) VALUES (" . $this->id . ", " . $s . ", 1);");
	}
	function getAccess()
	{
		if (!is_numeric($this->id)) return false;
		$res = new dbquery("SELECT * FROM " . TBL_RECHTE . " WHERE userId = " . $this->id);
		while($row=$res->getNextRow())
		{
			if ($row['access']=="1")
			array_push($this->access,$row['strukturId']);
		}
	}
	function hasAccess($s)
	{
		return (in_array($s,$this->access));
	}
	
	function hasCmsAccess()
	{
		return (count($this->access)>0);
	}
	
}

?>
