<?php
class dbquery
{
	var $server;
	var $datenbank;
	var $user;
	var $pass;
	var $dbHandle;
	var $queryString;
	var $recordSet;
	var $iteratorCount;


	function dbquery($queryString, $db=MYSQL_DB)
	{
		$this->server 			= MYSQL_HOST;
		$this->datenbank 		= $db;
		$this->user 			  = MYSQL_USER;
		$this->pass 			  = MYSQL_PW;
		$this->queryString	= $queryString;

		$this->connect();
	}


	function connect()
	{
		@$this->dbHandle = mysql_connect($this->server, $this->user, $this->pass);
		if ($this->dbHandle)
		{
			mysql_select_db($this->datenbank, $this->dbHandle); # DB auswählen
			$this->query();
		}
		else die("Datenbankverbindung konnte nicht hergestellt werden!");
	}


	function query()
	{

		$this->recordSet = mysql_query($this->queryString, $this->dbHandle);
		$this->iteratorCount=0;
		//echo $this->queryString;
	}


	function getRowCount()
	{
		return mysql_num_rows($this->recordSet);
	}


	function getNextRow()
	{
		$this->iteratorCount++;
		return mysql_fetch_assoc($this->recordSet);
	}
	
	function getNextRowNum()
	{
		$this->iteratorCount++;
		return mysql_fetch_row($this->recordSet);
		
	}

	function hasNext()
	{
		return !($this->iteratorCount>=$this->getRowCount());

	}

	function resetZeiger()
	{
		// recordset-zeiger zurücksetzen!!!
		$this->iteratorCount=0;
		mysql_data_seek($this->recordSet,0);
	}

  function getField()
  {
    return mysql_fetch_field($this->recordSet);
  }

  function getInsertId()
  {
    return mysql_insert_id($this->dbHandle);
  }

} // class dbquery

?>