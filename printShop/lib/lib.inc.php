<?php
define("INCLUDE_DIR", $_SERVER['DOCUMENT_ROOT']. "/printShop/lib/");

require_once(INCLUDE_DIR."config.php");

define("FONTDIR", "/kunden/staats.de/webseiten/shop/schriften/");

define("DPI", 0.3527);


function cmyk2rgb($c,$m,$y,$k)
{
    //C'=255-R, M'=255-G, Y'=255-B, K=min(C',M',Y'), C=C'-K, M=M'-K, Y=Y'-K
    If (($c + $k) < 255) $r = 255 - ($c + $k);
 Else $r = 0;
If (($m + $k) < 255) $g = 255 - ($m + $k);
 Else $g = 0;
If (($y + $k) < 255) $b = 255 - ($y + $k);
 Else $b = 0;
return $r . ";
" . $g . ";
" . $b;
}
/* * Modul-Konfiguration * */
define("NEWS_HAS_DATUM", true);
define("NEWS_HAS_TEASER", true);
define("NEWS_HAS_RTE", true);
define("NEWS_ASSETS_HAS_FILES",true);
define("NEWS_ASSETS_HAS_BILDER",true);
define("NEWS_ASSETS_BILDER_MAXCOUNT",1);
define("RTE_ASSETS_HAS_FILES",0);
define("TBL_KUNDEN", "staats_v2_kunden");
define("TBL_VORLAGEN", "staats_v2_vorlagen");
define("TBL_TEXTDATA", "staats_v2_daten_text");
define("TBL_BESTELLUNGEN", "staats_v2_bestellungen");
define("TBL_BESTELLDATA", "staats_v2_bestelldaten");
define("TBL_KATEGORIEN", TBL_PREFIX . "kategorien");
define("TBL_BENUTZER", TBL_PREFIX . "cms_benutzer");
define("TBL_RECHTE", TBL_PREFIX . "rechte");
define("TBL_STRUKTUR", TBL_PREFIX . "struktur");
define("TBL_WEBSITES", TBL_PREFIX . "websites");
define("TBL_NEWS", TBL_PREFIX . "news");
define("TBL_SPRACHEN", TBL_PREFIX . "sprachen");
define("TBL_TPLCONFIG", TBL_PREFIX . "tpl_config");
define("TBL_TEMPLATES", TBL_PREFIX . "templates");
define("TBL_CONTENTTYPES", TBL_PREFIX . "contenttypes");
define("TBL_GALLERY_PICS", TBL_PREFIX . "gallery_pics");
define("TBL_ORDER", TBL_PREFIX . "orders");
define("TBL_ORDER_ITEMS", TBL_PREFIX . "order_articles");
define("PATH_CMS_PREFIX","../");
define("PATH_HEADERBILDER", "assets/header");
define("DATA_PATH", "");
define("WEBSITE_ID","2");
define("URL_SUFFIX",".html");
define("URL_CMS",".");
define("PFAD_SHOPBILDER", "shopbilder/");
define("CMS_STARTSEITE","index.php?s=1");
define("CMS_MAINFILE","index.php?");
define("CMS_RTE_WIDTH","550");
define("CMS_RTE_HEIGHT","450");
define("CMS_RTE_DOCBASE","/");
define("CMS_ITEMS_PER_PAGE",50);
define("CMS_BGCOLOR_1", "#ebf2f9");
define("CMS_BGCOLOR_2", "#ffffff");
define("WEB_ITEMS_PER_PAGE",30);
define("KATEGORIE_ANGEBOTE", 52);
define("SPAMPROTECT_EMAIL_OFFSET",2);
define("CRLF","\r\n");
define("MWST",0.19);
define("EMAIL_REGISTER","kurtz@acid21.com");
define("EMAIL_BESTELLUNG", "kurtz@acid21.com");
$arrContainerCss=array(	array("id"=>"backgroundimage","cssName"=>"background-image", "name"=>"Hintergrundbild","typ"=>""),	array("id"=>"backgroundcolor", "cssName"=>"background-color", "name"=>"Hintergrundfarbe","typ"=>""),	array("id"=>"padding", "cssName"=>"padding", "name"=>"Padding","typ"=>""),	array("id"=>"margin", "cssName"=>"margin", "name"=>"Margin","typ"=>""),	array("id"=>"verticalalign", "cssName"=>"vertical-align", "name"=>"vertical-align","typ"=>""),	array("id"=>"bordertop", "cssName"=>"border-top", "name"=>"border-top","typ"=>""),	array("id"=>"borderright", "cssName"=>"border-right", "name"=>"border-right","typ"=>""),	array("id"=>"borderbottom", "cssName"=>"border-bottom", "name"=>"border-bottom","typ"=>""),	array("id"=>"borderleft", "cssName"=>"border-left", "name"=>"border-left","typ"=>""));
$arrFontCss=array(	array("id"=>"margin","cssName"=>"margin", "name"=>"Margin","typ"=>""),	array("id"=>"padding", "cssName"=>"padding", "name"=>"Padding","typ"=>""),	array("id"=>"verticalalign", "cssName"=>"vertical-align", "name"=>"vertical-align","typ"=>""),	array("id"=>"color", "cssName"=>"color", "name"=>"Color","typ"=>""),	array("id"=>"fontsize", "cssName"=>"font-size ", "name"=>"font-size","typ"=>""),	array("id"=>"fontfamily", "cssName"=>"font-family ", "name"=>"font-family","typ"=>""),	array("id"=>"fontstyle", "cssName"=>"font-style", "name"=>"font-style","typ"=>""),	array("id"=>"fontvariant", "cssName"=>"font-variant", "name"=>"font-variant","typ"=>""),	array("id"=>"fontweight", "cssName"=>"font-weight", "name"=>"font-weight","typ"=>""),	array("id"=>"wordspacing", "cssName"=>"word-spacing", "name"=>"word-spacing","typ"=>""),	array("id"=>"letterspacing", "cssName"=>"letter-spacing", "name"=>"letter-spacing","typ"=>""),	array("id"=>"textdecoration", "cssName"=>"text-decoration", "name"=>"text-decoration","typ"=>""),	array("id"=>"texttransform", "cssName"=>"text-transform", "name"=>"text-transform","typ"=>""));
$arrNaviCss=array(	array("id"=>"backgroundimage","cssName"=>"background-image", "name"=>"Hintergrundbild","typ"=>""),	array("id"=>"backgroundcolor", "cssName"=>"background-color", "name"=>"Hintergrundfarbe","typ"=>""),	array("id"=>"padding", "cssName"=>"padding", "name"=>"Padding","typ"=>""),	array("id"=>"margin", "cssName"=>"margin", "name"=>"Margin","typ"=>""),	array("id"=>"bordertop", "cssName"=>"border-top", "name"=>"border-top","typ"=>""),	array("id"=>"borderright", "cssName"=>"border-right", "name"=>"border-right","typ"=>""),	array("id"=>"borderbottom", "cssName"=>"border-bottom", "name"=>"border-bottom","typ"=>""),	array("id"=>"borderleft", "cssName"=>"border-left", "name"=>"border-left","typ"=>""),	array("id"=>"color", "cssName"=>"color", "name"=>"Color","typ"=>""),	array("id"=>"fontsize", "cssName"=>"font-size ", "name"=>"font-size","typ"=>""),	array("id"=>"fontfamily", "cssName"=>"font-family ", "name"=>"font-family","typ"=>""),	array("id"=>"fontstyle", "cssName"=>"font-style", "name"=>"font-style","typ"=>""),	array("id"=>"fontvariant", "cssName"=>"font-variant", "name"=>"font-variant","typ"=>""),	array("id"=>"fontweight", "cssName"=>"font-weight", "name"=>"font-weight","typ"=>""),	array("id"=>"wordspacing", "cssName"=>"word-spacing", "name"=>"word-spacing","typ"=>""),	array("id"=>"letterspacing", "cssName"=>"letter-spacing", "name"=>"letter-spacing","typ"=>""),	array("id"=>"textdecoration", "cssName"=>"text-decoration", "name"=>"text-decoration","typ"=>""),	array("id"=>"texttransform", "cssName"=>"text-transform", "name"=>"text-transform","typ"=>""));
importLibs();
$h = new webhelper();
function importLibs(){	$vipImport = array("class_assets.inc.php");
	foreach($vipImport as $file)	{		if (file_exists(INCLUDE_DIR . $file))			require_once(INCLUDE_DIR . $file);
	}	if ($handle = opendir(INCLUDE_DIR)) {	    while (false !== ($file = readdir($handle)))	    {	        if ($file != "." && $file != "..")	        {	            if(($file!="lib.inc.php")&&(!in_array($file,$vipImport))&&(strtolower(substr($file,strlen($file)-8))==".inc.php"))	            {	           		require_once(INCLUDE_DIR . $file);
	            }	        }	    }	    closedir($handle);
	}}
	function removeDelimiter($s){	$s = str_replace(array("<",">","{","}"),array("","","","",""),$s);
	return $s;
}
function getTplConfig(){	$arr=array();
	$dir = dir("../_templates/srisri");
	while(false!==($row=$dir->read()))	{		if ($row=="."||$row==".."||!is_file("../_templates/srisri/" . $row)) continue;
		if ($row!="default.css")continue;
		$f = file_get_contents("../_templates/srisri/" . $row);
		$suchmuster = '/<[A-Z_]>/';
		$suchmuster = '/<[A-Z0-9a-z_:]+>/';
		$treffer=array();
		preg_match_all($suchmuster, $f, $treffer);
		$treffer=array_map("removeDelimiter",$treffer);
		$arr = array_merge($arr,$treffer[0]);
	}	$arr = array_unique($arr);
	sort($arr);
	return $arr;
}
function showNaviPath($n){	$out = "";
	$s = new struktur($n);
	$out .= "<p style=\"float: left;
 padding-top: 2px;
\"><a href=\"" . $s->seo_url . URL_SUFFIX . "\" style=\"color: #df4a2ev\"><img style=\"margin: 0px 6px 0px 7px;
\" src=\"images/icon_arr_r.gif\" />" . $s->name . "</a></p>";
	if ($s->parent>0)	{		$s = new struktur($s->parent);
		$out = "<p style=\"float: left;
 padding-top: 2px;
\"><a href=\"" . $s->seo_url . URL_SUFFIX . "\"><img style=\"margin: 0px 6px 0px 7px;
\" src=\"images/icon_arr_r.gif\" />" . $s->name . "</a></p>".$out;
	}	$out = "<p style=\"float: left;
 padding-top: 2px;
\"><a href=\"./\"><img style=\"margin: 0px 6px 0px 0px;
\" src=\"images/icon_home.gif\" />Hirsch</a></p>" . $out;
	echo $out;
}
function getKategorien(){	$sql = "SELECT * FROM " . TBL_KATEGORIEN . " ORDER BY titel ASC;
";
	$res = new dbquery($sql);
	return $res;
}
function checkLogin(){	if (!empty($_SESSION['sess_id'])&&$_SESSION['sess_id']>0)	{		define("LOGGED_IN",true);
	} else define("LOGGED_IN",false);
}
function checkLoginFe(){	if (!empty($_SESSION['sess_fe_id'])&&$_SESSION['sess_fe_id']>0)	{		define("LOGGED_IN",true);
	} else define("LOGGED_IN",false);
}
function validateLoginFe($u,$p){	$u = trim($u);
	$p = trim($p);
	$sql = "SELECT * FROM " . TBL_KUNDEN . " WHERE login_name = '" . $u . "';";
 //  AND webAktiv = 1
    	$res = new dbquery($sql);
	if ($res->getRowCount()>0)	{		$row = $res->getNextRow();
		if (!empty($p)&&($row['login_name']===$u)&&($row['passwort']===$p))		return $row;
		else return false;
	} else return false;
}
function doLoginFe(){	$u = trim($_POST['un']);
	$p = trim($_POST['pw']);
	$d = validateLoginFe($u,$p);
	if ($d)	{		$_SESSION['sess_fe_id'] = $d['id'];
		$_SESSION['sess_fe_login_name'] = $d['login_name'];
		$_SESSION['sess_fe_firmenname'] = $d['firmenname'];
		$_SESSION['sess_fe_ansprechpartner'] = $d['ansprechpartner'];
		$_SESSION['sess_fe_strasse'] = $d['strasse'];
		$_SESSION['sess_fe_ort'] = $d['ort'];
		$_SESSION['sess_fe_telefon'] = $d['telefon'];
		$_SESSION['sess_fe_fax'] = $d['fax'];
		$_SESSION['sess_fe_email'] = $d['email'];
	} else return false;
}
function validateLogin($u,$p){	$u = trim($u);
	$p = trim($p);
	$sql = "SELECT * FROM " . TBL_BENUTZER . " WHERE login = '" . $u . "' AND aktiv = 1;";
 //  AND webAktiv = 1
    $res = new dbquery($sql);
	if ($res->getRowCount()>0)	{		$row = $res->getNextRow();
		$passwort = webhelper::cryptPw($p);
		if (!empty($p)&&($row['login']===$u)&&($row['passwortCrypt']===$passwort))		return $row;
		else return false;
	} else return false;
}
function doLogin(){	$u = trim($_POST['un']);
	$p = trim($_POST['pw']);
	$d = validateLogin($u,$p);
	if ($d)	{		$_SESSION['sess_id'] = $d['id'];
		$_SESSION['sess_vorname'] = $d['vorname'];
		$_SESSION['sess_nachname'] = $d['nachname'];
		$_SESSION['sess_isAdmin'] = ($d['isAdmin']==1);
	} else return false;
}
function includeJsAndCss(){	includeJs();
	includeCss();
}
function includeJs($p=""){	if (empty($p)) $p = PFAD_ROOT;
	echo "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . $p . "lib/helper.js\"></script>";
	echo "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . $p . "lib/prototype1603.js\"></script>";
}
function includeCss(){}function niceStr($s){	$s = htmlspecialchars($s);
	return $s;
}
function showHTML($sHTML){	$sHTML = stripslashes($sHTML);
	//$sHTML=ereg_replace("&","&amp; ",$sHTML);
	//$sHTML=ereg_replace("<","&lt; ",$sHTML);
	//$sHTML=ereg_replace(">","&gt; ",$sHTML);
	return $sHTML;
}
function splitMysqlDate($d){	echo $d;
}
function noUmlauteStr($s){	$s = str_replace(" ", "_", $s);
	$s = str_replace("�", "ae", $s);
	$s = str_replace("�", "ue", $s);
	$s = str_replace("�", "oe", $s);
	$s = str_replace("�", "Ae", $s);
	$s = str_replace("�", "Oe", $s);
	$s = str_replace("�", "Ue", $s);
	$s = str_replace("�", "ss", $s);
	$s = str_replace(",", "_", $s);
	$s = str_replace(".", "", $s);
	$s = str_replace("/", "_", $s);
	$s = str_replace("-", "_", $s);
	$s = str_replace("+", "und", $s);
	return $s;
}
function zufallsstring($length) {	$possible_charactors = "ABCDEFGHIJKLMNPQRSTUVWXYZ1239456789";
	$string = "";
	while(strlen($string)<$length) {		$string .= substr($possible_charactors,(rand()%(strlen($possible_charactors))),1);
	}	return($string);
}
function ImgIsJPEG($file){	$imgInfo = getimagesize($file);
	if ($imgInfo[2] == 2) return true;
	return false;
}function ImgIsGIF($file){	$imgInfo = getimagesize($file);
	if ($imgInfo[2] == 1) return true;
	return false;
}function ImgGetHeight($file){	$imgInfo = getimagesize($file);
	return $imgInfo[1];
}function ImgGetWidth($file){	$imgInfo = getimagesize($file);
	return $imgInfo[0];
}
function showResizedPic($datei, $max_width = 999999, $max_height = 999999){
    if (!file_exists($datei)) die("Datei nicht gefunden!");
	if (!ImgIsJPEG($datei) && !ImgIsGIF($datei)) die("Dateityp nicht unterst�tzt");
	$format = GetImageSize($datei);
	$breite = ImgGetWidth($datei);
	$hoehe = ImgGetHeight($datei);
	$faktor1 = $max_width/$breite;
	$faktor2 = $max_height/$hoehe;
	if ($faktor1>$faktor2) $faktor=$faktor2;
 else $faktor=$faktor1;
	if ($faktor>1) $faktor = 1;
	if (ImgIsJPEG($datei))	{		$altes_bild = ImageCreateFromJPEG($datei);
		$neues_bild = imagecreatetruecolor($breite*$faktor,$hoehe*$faktor);
		ImageCopyResampled($neues_bild,$altes_bild,0,0,0,0,$breite*$faktor,$hoehe*$faktor,$breite,$hoehe);
		ob_start();
 // start a new output buffer
        	ImageJPEG($neues_bild,"",80);
   		$ImageData = ob_get_contents();
   		$ImageDataLength = ob_get_length();
   		ob_end_clean();
 // stop this output buffer
        header("Content-type: image/jpeg") ;
		header("Content-Length: ".$ImageDataLength);
		echo $ImageData;
	}	if (ImgIsGIF($datei))	{		$altes_bild = ImageCreateFromGIF($datei);
		//$neues_bild = imagecreatetruecolor($breite*$faktor,$hoehe*$faktor);
		$colorTransparent = imagecolortransparent($altes_bild);
		$neues_bild = imagecreate($breite*$faktor,$hoehe*$faktor);
		imagepalettecopy($neues_bild,$altes_bild);
		imagefill($neues_bild,0,0,$colorTransparent);
		imagecolortransparent($neues_bild, $colorTransparent);
		ImageCopyResampled($neues_bild,$altes_bild,0,0,0,0,$breite*$faktor,$hoehe*$faktor,$breite,$hoehe);
		ob_start();
 // start a new output buffer
        	ImageGIF($neues_bild);
   		$ImageData = ob_get_contents();
   		$ImageDataLength = ob_get_length();
   		ob_end_clean();
 // stop this output buffer
        	header("Content-type: image/gif") ;
		header("Content-Length: ".$ImageDataLength);
		echo $ImageData;
	}}
	function saveResizedPic($datei, $newFile, $max_width = 999999, $max_height = 999999){	if (!file_exists($datei)) die("Datei nicht gefunden!");
	if (!ImgIsJPEG($datei) && !ImgIsGIF($datei)) die("Dateityp nicht unterst�tzt");
	$format = GetImageSize($datei);
	$breite = ImgGetWidth($datei);
	$hoehe = ImgGetHeight($datei);
	$faktor1 = $max_width/$breite;
	$faktor2 = $max_height/$hoehe;
	if ($faktor1>$faktor2) $faktor=$faktor2;
 else $faktor=$faktor1;
	if ($faktor>1) $faktor = 1;
	if ($faktor==1)	{		copy($datei, $newFile);
	} else {		if (ImgIsJPEG($datei))		{			$altes_bild = ImageCreateFromJPEG($datei);
			$neues_bild = imagecreatetruecolor($breite*$faktor,$hoehe*$faktor);
			ImageCopyResampled($neues_bild,$altes_bild,0,0,0,0,$breite*$faktor,$hoehe*$faktor,$breite,$hoehe);
	   		ImageJPEG($neues_bild,$newFile,80);
		}		if (ImgIsGIF($datei))		{			$altes_bild = ImageCreateFromGIF($datei);
			//$neues_bild = imagecreatetruecolor($breite*$faktor,$hoehe*$faktor);
			$colorTransparent = imagecolortransparent($altes_bild);
			$neues_bild = imagecreate($breite*$faktor,$hoehe*$faktor);
			imagepalettecopy($neues_bild,$altes_bild);
			imagefill($neues_bild,0,0,$colorTransparent);
			imagecolortransparent($neues_bild, $colorTransparent);
			ImageCopyResampled($neues_bild,$altes_bild,0,0,0,0,$breite*$faktor,$hoehe*$faktor,$breite,$hoehe);
	   		ImageGIF($neues_bild, $newFile);
		}	}}
		function getNiceSize($mySize){	$einheit = "Byte";
	$nkz=0;
	if ($mySize > 950)	{		$mySize = $mySize/1024;
		$einheit = "KB";
		$nkz=2;
	}	if ($mySize > 950)	{		$mySize = $mySize/1024;
		$einheit = "MB";
		$nkz=2;
	}	if ($mySize > 950)	{		$mySize = $mySize/1024;
		$einheit = "GB";
		$nkz=2;
	}	return number_format($mySize,$nkz,",",".") . " " . $einheit;
}function getNiceDate($myDate){	$monat = "";
	switch (date("n",$myDate))	{		case "1":		$monat = "Januar";
		break;
		case "2":		$monat = "Februar";
		break;
		case "3":		$monat = "M�rz";
		break;
		case "4":		$monat = "April";
		break;
		case "5":		$monat = "Mai";
		break;
		case "6":		$monat = "Juni";
		break;
		case "7":		$monat = "Juli";
		break;
		case "8":		$monat = "August";
		break;
		case "9":		$monat = "September";
		break;
		case "10":		$monat = "Oktober";
		break;
		case "11":		$monat = "November";
		break;
		case "12":		$monat = "Dezember";
		break;
	}	$wochentag = "";
	switch (date("w",$myDate))	{		case "0":		$wochentag = "Sonntag";
		break;
		case "1":		$wochentag = "Montag";
		break;
		case "2":		$wochentag = "Dienstag";
		break;
		case "3":		$wochentag = "Mittwoch";
		break;
		case "4":		$wochentag = "Donnerstag";
		break;
		case "5":		$wochentag = "Freitag";
		break;
		case "6":		$wochentag = "Samstag";
		break;
	}	return $wochentag . ", " . date("d", $myDate) . ". $monat ". date(" Y, G:i", $myDate) . " Uhr";
}
class img {	var $src;
	var $type;
	var $width;
	var $height;
	function img($src)	{		$this->src = $src;
		$this->getProperties();
	}	function getProperties()	{		$imgInfo = getimagesize($this->src);
		if ($imgInfo[2] == 2) $this->type = "jpg";
		elseif ($imgInfo[2] == 1) $this->type = "gif";
		else $this->type = "unsupported";
		$this->width = $imgInfo[0];
		$this->height = $imgInfo[1];
	}	function saveAs($datei,$maxWidth=999999,$maxHeight=999999,$format=false)	{		$faktor1 = $maxWidth/$this->width;
		$faktor2 = $maxHeight/$this->height;
		if ($faktor1>$faktor2) $faktor=$faktor2;
 else $faktor=$faktor1;
		if ($faktor>1) $faktor = 1;
 // Vergr��ern verhindern
 if ($this->type=="jpg")		{			$altes_bild = ImageCreateFromJPEG($this->src);
			$neues_bild = imagecreatetruecolor($this->width*$faktor,$this->height*$faktor);
			ImageCopyResampled($neues_bild,$altes_bild,0,0,0,0,$this->width*$faktor,$this->height*$faktor,$this->width,$this->height);
	   		ImageJPEG($neues_bild,$datei,80);
		}		if ($this->type=="gif")		{			$altes_bild = ImageCreateFromGIF($this->src);
			//$neues_bild = imagecreatetruecolor($breite*$faktor,$hoehe*$faktor);
			$colorTransparent = imagecolortransparent($altes_bild);
			$neues_bild = imagecreate($this->width*$faktor,$this->height*$faktor);
			imagepalettecopy($neues_bild,$altes_bild);
			imagefill($neues_bild,0,0,$colorTransparent);
			imagecolortransparent($neues_bild, $colorTransparent);
			ImageCopyResampled($neues_bild,$altes_bild,0,0,0,0,$this->width*$faktor,$this->height*$faktor,$this->width,$this->height);
	   		ImageGIF($neues_bild, $datei);
		}	}}
