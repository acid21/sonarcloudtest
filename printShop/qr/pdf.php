<?php
define("DPI", 0.3527);
require_once("lib/qr_pdf.php");
$pdf = pdf_new();
pdf_open_file($pdf);

// Creates a 50mm x 40mm PDF page
pdf_begin_page($pdf, 50 / DPI, 40 / DPI);

$contact = array(
    "firstname"=>"Vorname",
    "lastname"=>"Nachname",
    "title"=>"Anrede",
    "organization"=>"",
    "role"=>"Position",
    "url"=>"Website",

    "email"=>"",
    "email2"=>"Alternative E-Mail",

    "phone"=>"Telefon",
    "fax"=>"",
    "mobile"=>"Mobil",

    "street"=>"Straße und Haus-Nr.",
    "zip"=>"Mobil",
    "city"=>"",
    "state"=>"Bundesland",
    "country"=>"Land"
);

$qr_placed = placeQrPDF($pdf, array(
        "contact" => $contact,
        "qr" => array(
            "left" => "20", // in mm
            "top" => "10", // in mm
            "size" => "30", // in mm
            "filename" => dirname(__FILE__).DIRECTORY_SEPARATOR."/output/qr2.png", // absolute path
            "fg_color" => "#000", // black
            //"bg_color" => "#FFF", // white
            "bg_color" => "-1" // transparent
        )
    )
);

if(!$qr_placed)
    die("something went wrong, QR-code was not placed");

pdf_end_page($pdf);
pdf_close($pdf);

$pdf_bin_data = pdf_get_buffer($pdf);

header("Content-type: application/pdf");
header("Content-disposition: inline; filename=dummy.pdf");
header("Content-length: " . strlen($pdf_bin_data));

echo $pdf_bin_data;