<?php

// function taken from http://www.php.net/manual/de/function.http-build-query.php
// to convert array to query string
if(!function_exists('http_build_query')) {
    function http_build_query( $formdata, $numeric_prefix = null, $key = null ) {
        $res = array();
        foreach ((array)$formdata as $k=>$v) {
            $tmp_key = urlencode(is_int($k) ? $numeric_prefix.$k : $k);
            if ($key) {
                $tmp_key = $key.'['.$tmp_key.']';
            }
            if ( is_array($v) || is_object($v) ) {
                $res[] = http_build_query($v, null, $tmp_key);
            } else {
                $res[] = $tmp_key."=".urlencode($v);
            }
        }
        return implode("&", $res);
    }
}

/**
 * Inserts a QR-Code into a PDF page
 * @param resource $pdf A resource created by pdf_new()
 * @param array $data An Array containing the contact and dimensions of the QR-Code
 * @return bool
 */
function placeQrPDF(&$pdf, $data){
    $query_string = http_build_query($data);
    $url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["REQUEST_URI"]) . "/qr/lib/qr_service.php5?".$query_string;

    $handler = fopen($url,"r");
    if(!$handler) return FALSE;
    $result = "";
    while (!feof($handler))
        $result.=fread($handler,8192);
    fclose($handler);
    if($result[0] == '1'){
        $result = explode(',', $result);

        if(!file_exists($result[1]))
            // something went terribly wrong, the png file does not exist
            return FALSE;

        $qr = $data["qr"];
        $img_index = pdf_open_image_file($pdf, "png", $result[1]);

        $img_size_pt = pdf_get_value($pdf, "imagewidth", $img_index); // QR-code : width=height
        $page_height_pt = pdf_get_value($pdf, "pageheight");
        $img_target_size_pt = $qr["size"] / DPI; // mm -> pt
        $img_x_pt = $qr["left"] / DPI; // mm -> pt
        $img_y_pt =$page_height_pt -  ($qr["top"] / DPI + $img_target_size_pt);
        $img_scale_factor =  $img_target_size_pt / $img_size_pt;

        pdf_place_image($pdf, $img_index, $img_x_pt, $img_y_pt, $img_scale_factor);


        return TRUE;
    }
    return FALSE;
}

/*
 * array(
            "name" => "Panasenko", // required
            "surname" => "Nikolai", // optional
            "title" => "Prof. Dr.", //optional
            "formattedName" => "Nikolai Panasenko", // optional
            "organization" => "ACID21 GmbH", // required
            "role" => "Programmierer", // optional, Type purpose: To specify information concerning the role, occupation, or business category of the object the vCard represents.
            "org_title" => "Entwicklungsabteilung", // optional, Type purpose: To specify the job title, functional position or function of the object the vCard represents.
            "url" => "www.acid21.com/", // optional
            "email" => array( // optional
                "test@acid21.com",
                "test2@acid21.com"
            ),
            "tel" => array( // optional
                "home,voice" => "+49 30 1234567",
                "work,voice" => "+49 30 1234569",
                "work,fax" => "+49 30 1234560",
                "cell" => "+49 16 1234567"
            ),
            "address" => array( // optional
                "home" => array(
                    "post_office_box" => "",
                    "extended_address" => "",
                    "street_address" => "Homburgerstrasse 53",
                    "locality" => "Berlin",
                    "region" => "Berlin",
                    "postal_code" => "10365",
                    "country" => "Deutschland"
                ),

                "work" => array(
                    "post_office_box" => "",
                    "extended_address" => "",
                    "street_address" => "Albertstrasse 53",
                    "locality" => "München",
                    "region" => "Bayern",
                    "postal_code" => "17098",
                    "country" => "Deutschland"
                ),
            ),
        )
 */


/*
$contact = array(
    "name" => "Panasenko", // Nachname
    "firstname" => "Nikolai", // Vorname
    "title" => "Prof. Dr.", //Anrede
    "organization" => "ACID21 GmbH", // Firma
    "role" => "Programmierer", // Position
    "url" => "www.acid21.com/", // Website
    "email" => array(
        "test@acid21.com", // E-Mail
        "test2@acid21.com" // Alternative E-Mail
    ),
    "tel" => array( // optional
        "home,work,voice" => "+49 30 1234567", //Telefon
        "home,work,fax" => "+49 30 1234560", //Fax
        "cell" => "+49 16 1234567" //Mobil
    ),
    "address" => array(
        "street_address" => "Homburgerstrasse 53", //Straße und Haus-Nummer
        "locality" => "Berlin", //Stadt
        "region" => "Berlin", //Bundesland
        "postal_code" => "10365", //PLZ
        "country" => "Deutschland" //Land
    ),
);
*/
