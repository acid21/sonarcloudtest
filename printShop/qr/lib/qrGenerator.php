<?php
require_once 'phpqrcode.php';
/**
 * Generates a VCard (Version 2.1) from a contact stored in an Array.
 * Generates a QR-Code and saves as .png image file.
 */
class QrGenerator {
	private $contactData = NULL;
	private $contactVCard = NULL;
	
	/**
 	* 
 	* Helper function: returns an array value if its key is set, otherwise returns ''
 	*/
	private function getArrayValueIfSet($arr, $key){
		return (isset($arr[$key])?$arr[$key]:"");	
	}

    /**
     * Parses an address stored in $data and returns a proper VCard address entry
     * @param array $data
     * @param string $key
     * @param string $field
     * @param string $default
     * @return string
     */
    private function parseAddressField($data, $key, $field, $default){
		if(!isset($data[$key])) return "";
		$data=$data[$key];
		if(gettype($data)==="array"){
			$result="";
			foreach ($data as $type=>$address){
				$break=FALSE;
				if(gettype($address)!=="array"){
					$obj=$data;
					$result.=$field.";TYPE=".$default.":";
					$break=true;
				}else {
					$obj=$address;
					$result.=$field.";TYPE=".(gettype($type)==="integer" ? $default:$type).":";
				}
				$result.=$this->getArrayValueIfSet($obj,"post_office_box").";".
						 $this->getArrayValueIfSet($obj,"extended_address").";".
						 $this->getArrayValueIfSet($obj,"street_address").";".
						 $this->getArrayValueIfSet($obj,"locality").";".
						 $this->getArrayValueIfSet($obj,"region").";".
						 $this->getArrayValueIfSet($obj,"postal_code").";".
						 $this->getArrayValueIfSet($obj,"country");
				$result.="\r\n";
				if($break) break;
			}
			return $result;
		}else
			return "";
	}

    /**
     * Generates a vCard entry
     * @param array $array the contact
     * @param string $array_key string the key of the field
     * @param string $vcard_field the name of the vCard field
     * @param string $vcard_default_type the default type for the vCard field
     * @return string
     */
    private function parseField($array, $array_key, $vcard_field, $vcard_default_type=""){
		if(!isset($array[$array_key])) return "";
		if(gettype($array[$array_key])==="array"){
			$result="";
			foreach ($array[$array_key] as $type=>$val)
				$result.=$vcard_field.";TYPE=".(gettype($type)==="integer" ? $vcard_default_type:$type).":".$val."\r\n";
			return $result;
		}else
			return $vcard_field.":".$array[$array_key]."\r\n";
	}

    /**
     * Parses a contact stored in an array and converts it to a VCard
     * @param array $data the contact
     * @return bool  TRUE if VCard was generated, FALSE if an error occurred
     */
    private function parseContact($data){
		$this->contactData=$data;
		$this->contactVCard=NULL;
		$error = FALSE;
		
		$vcardData="BEGIN:VCARD \r\n";
		$vcardData.="VERSION:2.1 \r\n";

		// required field <lastname>
		if(isset($data["lastname"])){
                        /*
               * Type special note: The structured type value corresponds, in
                 sequence, to the Family Name, Given Name, Additional Names, Honorific
                 Prefixes, and Honorific Suffixes.
             */
			$vcardData.="N:".$data["lastname"]; // Family Name
			$vcardData.=isset($data["firstname"])?";".$data["firstname"]:";"; // Given Name
			$vcardData.=";"; // Additional Names
			$vcardData.=isset($data["title"])?";".$data["title"]:";"; // Honorific Prefixes
			$vcardData.=";"; // Honorific Suffixes
			$vcardData.="\r\n";
		}else
			$error=TRUE;
		
		//optional field <formattedName>
		$vcardData.=$this->parseField($data,"formattedName","FN");

		//required field <organization>
		$vcardData.=$this->parseField($data,"organization","ORG");

		//optional field <role>
		$vcardData.=$this->parseField($data,"role","ROLE");

		//optional field <org_title>
		$vcardData.=$this->parseField($data,"org_title","TITLE");

		//optional fields <email>
		$vcardData.=$this->parseField($data,"email","EMAIL","internet");

        //optional fields <email2>
        $vcardData.=$this->parseField($data,"email2","EMAIL","internet");
		
		//optional fields <tel>
		$vcardData.=$this->parseField($data,"tel","TEL","voice");
		
		//optional field <address>
		$vcardData.=$this->parseAddressField($data,"address","ADR","intl,postal,parcel,work");

		//optional field <url>
		$vcardData.=$this->parseField($data,"url","URL");
		
		
		$vcardData.="END:VCARD";
		if(!$error) $this->contactVCard=$vcardData;
		return !$error;
	}

    /**
     * Convert a contact field from gunnars format to the format of this class
     * @param $array
     * @param $field
     * @param $new_field
     * @param $sub_field
     */
    function convertField(&$array, $field, $new_field, $sub_field){
        if(isset($array[$field])){
            $array[$new_field][$sub_field]=$array[$field];
            unset($array[$field]);
        }
    }

	/**
   * Sets the contact
	 * @param array $contact
   * @return bool TRUE if contact was successfully converted to a VCard or FALSE if an Error occurred, due to faulty argument
	 */
    public function setContact($contact){
		if(isset($contact) && gettype($contact)==="array"){
            // filter out all empty elements
            function empty_str($val){ return strlen($val) > 0;}
            $contact = array_filter($contact, "empty_str");

            // convert them
            $this->convertField($contact, "phone", "tel", "home,work,voice");
            $this->convertField($contact, "fax", "tel", "home,work,fax");
            $this->convertField($contact, "mobile", "tel", "cell");
            $this->convertField($contact, "street", "address", "street_address");
            $this->convertField($contact, "zip", "address", "postal_code");
            $this->convertField($contact, "city", "address", "locality");
            $this->convertField($contact, "state", "address", "region");
            $this->convertField($contact, "country", "address", "country");

			return $this->parseContact($contact);
		}else return FALSE;
	}


    /**
     * Returns the vCard
     * @return bool|string Returns the vCard representation of the previously supplied contact or FALSE if vCard is not available
     */
    public function getVCard(){
        return $this->contactVCard != NULL?$this->contactVCard:FALSE;
    }

    /**
     * Generates a QR-Code and saves it as a png file
     * @param $filename
     * @param $size
     * @param $foregroundColor
     * @param $bgColor
     * @return bool TRUE if QR-Code was successfully saved, FALSE if an error occurred
     */
    public function generatePNG($filename, $size, $foregroundColor, $bgColor){
		if($this->contactVCard!=NULL && is_writeable(dirname($filename))){
			QRcode::png($this->contactVCard, $filename, QR_ECLEVEL_L, $size, 0, false, $foregroundColor, $bgColor);
            return TRUE;
        }
		else 
			return FALSE;
	}
}