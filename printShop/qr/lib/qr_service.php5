<?php
// We do not allow requests originating from another host than this script's host
if($_SERVER["SERVER_ADDR"]!==$_SERVER["REMOTE_ADDR"]) die("0");

require_once 'qrGenerator.php';

$qr_config = $_GET["qr"];
$contact = $_GET["contact"];
// do some error checking first...
$target_file = $qr_config["filename"]; // we expect an absolute path
// can we write into the requested directory?
if(!is_writeable(dirname($target_file))) die(0);
else {
    if(file_exists($target_file))
        unlink($target_file);
}

$qrGenerator = new QrGenerator();
$qrGenerator->setContact($contact);
$qr_generated = $qrGenerator->generatePNG($target_file, 1, $qr_config["fg_color"], $qr_config["bg_color"]);
if($qr_generated){
    echo "1,".$target_file;;
}else{
    echo "0";
}