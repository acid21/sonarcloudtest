<?
if (empty($area)) $area=1;
?>

<script type="text/javascript">
<!--
function loeschen(id)
{
	x = confirm("M�chten Sie diesen Men�punkt wirklich l�schen?\nACHTUNG: Alle Untermen�punkte werden ebenfalls gel�scht!");
	if (x == true) {
	location.href="index.php?" + Base64.encode("a=struktur&b=del&id=" + id);
	}
}
//-->
</script>

<div id="content">
	<div id="content_tab_head">

		<div style="float:left; padding: 6px 6px 4px 10px;"><img src="images/icon_structure.gif" /></div>
		<div style="float:left; padding: 4px 10px 1px 0px;" class="title">Struktur bearbeiten</div>
	</div>
</div>
<div id="content_tab" class="content_tab">

<?
function showStruktur($area, $titel="Website")
{
?>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="480">
		<tr>
			<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><b><?=$titel?></b></td>
			<!--<td style="border-bottom: 1px solid #d4d5d4; border-left: none; border-right: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2">&nbsp;</td>-->
		</tr>
<?

$bgColor = "#ffffff";
$res = webhelper::getStrukturCms("",$area,L);
while($row=$res->getNextRow())
{
$bgColor = webhelper::getCmsBgColor($bgColor);
?>
		<tr>
			<td bgcolor="<? echo $bgColor; ?>" style="padding-top: 0; padding-bottom: 0; border: none;">
			<table class="clear_table" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding: 3px 0 0 0; width: 10px;"><? if (webhelper::hasSubNavi($row['id'])) { ?><img src="images/icon_tree_arr_dn.png" /><? } ?></td>
					<td style="	padding: 0 0 0 5px; height: 20px;"><a href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_tree_page.png" /></a></td>
					<td style="padding: 0; border: 0;" class="navi_tree"><p><a href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row['id'] . "&area=".$area); ?>"><?= webhelper::niceHtml($row['name']); ?></a></p></td>
				</tr>
			</table>
			</td>
			<td width="189" style="padding: 2px; border-left: 0;" bgcolor="<? echo $bgColor; ?>">
				<p class="main_button"><a style="width: auto;" title="Bearbeiten" href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_head_textedit.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="L&ouml;schen" href="javascript:loeschen('<?= $row['id']; ?>')"><img src="images/icon_head_del.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Seite oberhalb" href="<?= webhelper::encodeQs("a=struktur&b=addAbove&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_add_above.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Seite unterhalb" href="<?= webhelper::encodeQs("a=struktur&b=addBelow&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_add_below.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Unterseite" href="<?= webhelper::encodeQs("a=struktur&b=addSub&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_add_lower.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Ganz nach oben" href="<?= webhelper::encodeQs("a=struktur&b=moveToTop&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_uup.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Eine Position h&ouml;her" href="<?= webhelper::encodeQs("a=struktur&b=moveUp&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_up.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Eine Position tiefer" href="<?= webhelper::encodeQs("a=struktur&b=moveDown&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_dn.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Ganz nach unten" href="<?= webhelper::encodeQs("a=struktur&b=moveToBottom&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_ddn.gif"></a></p>
			</td>
	  </tr>
<?
$res2 = webhelper::getStrukturCms($row['id']);
while($row2=$res2->getNextRow())
{
	$bgColor = webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
		<td style="padding-left: 21px; padding-top: 0; padding-bottom: 0; border: none;" bgcolor="<? echo $bgColor; ?>">
			<table class="clear_table" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding: 3px 0 0 0; width: 10px;"><? if (webhelper::hasSubNavi($row2['id'])) { ?><img src="images/icon_tree_arr_dn.png" /><? } ?></td>
					<td style="	padding: 0 0 0 5px; height: 20px;"><a href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_tree_page.png" /></a></td>
					<td style="padding: 0; border: 0;" class="navi_tree"><p><a href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row2['id'] . "&area=".$area); ?>"><? echo $row2['name']; ?></a></p></td>
				</tr>
			</table>
		</td>
		<td style="padding: 2px; border-left: 0;" bgcolor="<?= $bgColor; ?>">
				<p class="main_button"><a style="width: auto;" title="Bearbeiten" href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_head_textedit.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="L&ouml;schen" href="javascript:loeschen('<?= $row2['id']; ?>');"><img src="images/icon_head_del.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Seite oberhalb" href="<?= webhelper::encodeQs("a=struktur&b=addAbove&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_add_above.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Seite unterhalb" href="<?= webhelper::encodeQs("a=struktur&b=addBelow&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_add_below.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Unterseite" href="<?= webhelper::encodeQs("a=struktur&b=addSub&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_add_lower.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Ganz nach oben" href="<?= webhelper::encodeQs("a=struktur&b=moveToTop&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_uup.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Eine Position h&ouml;her" href="<?= webhelper::encodeQs("a=struktur&b=moveUp&id=" . $row2['id'] . "&area=".$area); ?>>"><img src="images/icon_head_arr_up.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Eine Position tiefer" href="<?= webhelper::encodeQs("a=struktur&b=moveDown&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_dn.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Ganz nach unten" href="<?= webhelper::encodeQs("a=struktur&b=moveToBottom&id=" . $row2['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_ddn.gif"></a></p>
		</td>
	  </tr>
<?
$res3 = webhelper::getStrukturCms($row2['id']);
while($row3=$res3->getNextRow())
{
	$bgColor = webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
		<td style="padding-left: 42px; padding-top: 0; padding-bottom: 0; border: none;" bgcolor="<? echo $bgColor; ?>">
			<table class="clear_table" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding: 3px 0 0 0; width: 10px;"><? if (webhelper::hasSubNavi($row3['id'])) { ?><img src="images/icon_tree_arr_dn.png" /><? } ?></td>
					<td style="	padding: 0 0 0 5px; height: 20px;"><a href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row3['id'] . "&area=".$area); ?>"><img src="images/icon_tree_page.png" /></a></td>
					<td style="padding: 0; border: 0;" class="navi_tree"><p><a href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row3['id'] . "&area=".$area); ?>"><? echo $row3['name']; ?></a></p></td>
				</tr>
			</table>
		</td>
		<td style="padding: 2px; border-left: 0;" bgcolor="<? echo $bgColor; ?>">
				<p class="main_button"><a style="width: auto;" title="Bearbeiten" href="<?= webhelper::encodeQs("a=struktur&b=edit&id=" . $row3['id'] . "&area=".$area); ?>"><img src="images/icon_head_textedit.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="L&ouml;schen" href="javascript:loeschen('<?= $row3['id'] ?>');"><img src="images/icon_head_del.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Seite oberhalb" href="<?= webhelper::encodeQs("a=struktur&b=addAbove&id=" . $row3['id'] . "&area=".$area); ?>"><img src="images/icon_add_above.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Seite unterhalb" href="<?= webhelper::encodeQs("a=struktur&b=addBelow&id=" . $row3['id'] . "&area=".$area); ?>"><img src="images/icon_add_below.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Neue Unterseite"><img src="images/icon_add_lower_off.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Ganz nach oben" href="<?= webhelper::encodeQs("a=struktur&b=moveToTop&id=" . $row3['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_uup.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Eine Position h&ouml;her" href="<?= webhelper::encodeQs("a=struktur&b=moveUp&id=" . $row3['id'] . "&area=".$area); ?>>"><img src="images/icon_head_arr_up.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Eine Position tiefer" href="<?= webhelper::encodeQs("a=struktur&b=moveDown&id=" . $row3['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_dn.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Ganz nach unten" href="<?= webhelper::encodeQs("a=struktur&b=moveToBottom&id=" . $row3['id'] . "&area=".$area); ?>"><img src="images/icon_head_arr_ddn.gif"></a></p>
		</td>
	  </tr>
<? } } }
if ($res->getRowCount()==0)
{
?>
	<tr style="height:15px;">
		<td bgcolor="#ebf2f9" style="padding-top: 0; padding-bottom: 0; border: none; height:15px;" colspan="2">
			<p style="height:15px;"><a href="<?= webhelper::encodeQs("a=struktur&b=edit&id=new&area=".$area); ?>" style="height:15px;">Bitte klicken Sie hier, um einen ersten Men&uuml;punkt anzulegen.</a></p>
		</td>
	</tr>
<? } ?>
	</table>
<? }
showStruktur(2,"Shop"); 
echo "<p>&nbsp;</p>";
showStruktur(1,"Hauptnavigation");



?>

</div>