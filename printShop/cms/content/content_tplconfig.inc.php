<?
$data = new website(WEBSITE_ID);
$tpl = new template($data->templateId);
$tplItems = explode(",",$tpl->items);

?>
<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title"><? echo webhelper::niceHtml($s->name); ?> <a href="<?= DOMAIN . webhelper::getUrl($s->id, $s->seo_url); ?>" target="_blank">[<?= DOMAIN . webhelper::getUrl($s->id, $s->seo_url); ?>]</a></div>
		<div style="clear:both;">
			<div id="main_buttons" style="width:30px;">
				<p class="main_button"><a href="javascript:document.forms['formData'].submit();" title="Speichern"><img src="images/icon_head_save.png" /></a></p>
			</div>
			<div class="tab_aktiv" id="tabTemplate"><a href="javascript:toggleDisplay('Template');" title="Template"><img class="tab_img" src="images/icon_tab_edit.png" />Template</a></div>
			<div class="tab" id="tabSonstiges"><a href="javascript:toggleDisplay('Sonstiges');" title="Sonstiges"><img class="tab_img" src="images/icon_tab_properties.png" />Ger&uuml;st anpassen</a></div>
			<div class="tab" id="tabHeader"><a href="javascript:toggleDisplay('Header');" title="Header"><img class="tab_img" src="images/icon_tab_seo.png" />Header anpassen</a></div>
<? foreach($tplItems as $v)
{ ?>
<div class="tab" id="tab<?=$v?>"><a href="javascript:toggleDisplay('<?=$v?>');" title="<?=$v?>"><img class="tab_img" src="images/icon_tab_seo.png" /><?=$v?> anpassen</a></div>	
<? } ?>
			<div class="tab" id="tabContent"><a href="javascript:toggleDisplay('Content');" title="Content"><img class="tab_img" src="images/icon_tab_seo.png" />Content anpassen</a></div>
<? if ($data->hasTeaser) { ?>			<div class="tab" id="tabTeaser"><a href="javascript:toggleDisplay('Teaser');" title="Teaser"><img class="tab_img" src="images/icon_tab_seo.png" />Teaser anpassen</a></div><? } ?>
			<div class="tab" id="tabFooter"><a href="javascript:toggleDisplay('Footer');" title="Footer"><img class="tab_img" src="images/icon_tab_seo.png" />Footer anpassen</a></div>
		</div>
	</div>
</div>

<form method="POST" action="index.php?<? echo $_SERVER['QUERY_STRING']; ?>" name="formData"  ENCTYPE="multipart/form-data">
<div id="content_tab_Template" class="content_tab">
<input type="hidden" name="save" value="config">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0"  width="480">
	  <tr>
	  	<td colspan="3" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Layout ausw&auml;hlen</strong></td>
	  </tr>
<?
$bgColor = "#FFFFFF";

$res = new dbquery("SELECT * FROM " . TBL_TEMPLATES . " ORDER BY pos ASC");
while($row=$res->getNextRow()) {
if ($bgColor == "#FFFFFF") $bgColor = "#ebf2f9"; else $bgColor = "#FFFFFF";
?>
	   <tr>
	  	<td width="20"  bgcolor="<?= $bgColor ?>" style="border:none;"><input type="radio" name="templateId" value="<?= $row['id']; ?>" <?= (($data->templateId==$row['id']) ? " checked" : "") ?> style="width:15px;" id="radioTpl_<?= $row['id']; ?>"></td>
	  	<td width="120"  bgcolor="<?= $bgColor ?>" ><label for="radioTpl_<?= $row['id']; ?>"><img src="images/<?= $row['previewFile']; ?>"></label></td>
		<td width="300"  bgcolor="<?= $bgColor ?>"><label for="radioTpl_<?= $row['id']; ?>"><?= $row['titel'] ?></label></td>
	  </tr>
<? }
if ($bgColor == "#FFFFFF") $bgColor = "#ebf2f9"; else $bgColor = "#FFFFFF";
?>
<tr>
	  	<td width="20"  bgcolor="<?= $bgColor ?>" style="border:none;"><input type="checkbox" name="hasTeaser" value="true" <?= (($data->hasTeaser==1) ? " checked" : "") ?> style="width:15px;" id="cbTeaser"></td>
	  	<td width="420" colspan="2" bgcolor="<?= $bgColor ?>" ><label for="cbTeaser">mit Teaser</label></td>
	  </tr>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>

</div>
<?
function printRow($id,$name,$typ)
{
	$value = $id;
?>
	<tr>
		<td width="120"  bgcolor="#ffffff" style="border:none;"><?=$name?>:</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="<?=$id?>" value="<?=new tplconfig($id)?>"></td>
	</tr>
<?
}

function showFontCustomizer($s,$area)
{
	global $arrFontCss;
?>
<tr>
	<td colspan="2"  bgcolor="#ebf2f9" style="border:none;" style="height:25px;" height="25"><b><?= $s ?></b></td>
</tr>
<?
	foreach($arrFontCss as $v)
	{
		printRow("tplcfg_" . $area .  "_" . $s . "_" . $v['id'], "-- " . $v['name'] , $v['typ']);
	}
}

function showContainerCustomizer($s)
{
	global $arrContainerCss;
?>
	<tr>
		<td width="120"  bgcolor="#ebf2f9" style="border:none;"><b><?=strtoupper($s)?>:</b></td>
		<td width="300"  bgcolor="#ebf2f9">&nbsp;</td>
	</tr>
<?
	foreach($arrContainerCss as $v)
	{
		printRow("tplcfg_" . $s . "_" . $v['id'], "-- " . $v['name'] , $v['typ']);
	}
}
?>

<?
function showAreaCustomizer($area)
{
?>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0"  width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong><?= strtoupper($area) ?></strong></td>
	  </tr>
<? showFontCustomizer("stern",$area); ?>
<? showFontCustomizer("h1",$area); ?>
<? showFontCustomizer("p",$area); ?>
<? showFontCustomizer("a",$area); ?>
<? showFontCustomizer("ahover",$area); ?>
<? showFontCustomizer("h2",$area); ?>
<? showFontCustomizer("h3",$area); ?>

	</table>
<? } ?>
<?
function showNaviCustomizer($area)
{
?>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0"  width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong><?= strtoupper($area) ?></strong></td>
	  </tr>
<? showNaviItemCustomizer("stern",$area); ?>
<? showNaviItemCustomizer("ul",$area); ?>
<? showNaviItemCustomizer("li",$area); ?>
<? showNaviItemCustomizer("a",$area); ?>
<? showNaviItemCustomizer("liactive",$area); ?>
	</table>
<? } ?>
<?
function showNaviItemCustomizer($s,$area)
{
	global $arrNaviCss;
?>
<tr>
	<td colspan="2"  bgcolor="#ebf2f9" style="border:none;" style="height:25px;" height="25"><b><?= $s ?></b></td>
</tr>
<?
	foreach($arrNaviCss as $v)
	{
		printRow("tplcfg_" . $area .  "_" . $s . "_" . $v['id'], "-- " . $v['name'] , $v['typ']);
	}
}
?>

<div id="content_tab_Content" class="content_tab" style="display:none">
<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Container anpassen</strong></td>
	  </tr>
	<?= showContainerCustomizer("inhalt"); ?>
</table>
<? showAreaCustomizer("inhalt"); ?>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
<div id="content_tab_Sonstiges" class="content_tab" style="display:none">
<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Template anpassen</strong></td>
	  </tr>
	   <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Datei in &quot;images&quot; laden:</td>
		<td width="300"  bgcolor="#ffffff"><input type="file" name="file_images" value=""></td>
	  </tr>
</table>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Template anpassen</strong></td>
	  </tr>
	   <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Gesamtbreite Website (px):</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_breiteweb" value="<?= new tplconfig("tplcfg_main_breiteweb") ?>"></td>
	  </tr>
	   <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">H&ouml;he Header (px):</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_hoeheheader" value="<?= new tplconfig("tplcfg_main_hoeheheader") ?>"></td>
	  </tr>
<? if (in_array("hnavi",$tplItems)) { ?>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">H&ouml;he horizontale Navigation (px):</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_hoehehnav" value="<?= new tplconfig("tplcfg_main_hoehehnav") ?>"></td>
	  </tr>
<? } ?>
	   <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">H&ouml;he Footer (px):</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_hoehefooter" value="<?= new tplconfig("tplcfg_main_hoehefooter") ?>"></td>
	  </tr>
<? if (in_array("vnavi",$tplItems)) { ?>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Breite vertikale Navigation (px):</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_breitevnav" value="<?= new tplconfig("tplcfg_main_breitevnav") ?>"></td>
	  </tr>
<? } ?>
<? if ($data->hasTeaser) { ?>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Breite Teaser (px):</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_breiteteaser" value="<?= new tplconfig("tplcfg_main_breiteteaser") ?>"></td>
	  </tr>
<? } ?>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Horizontale Navigation (max. 2 Ebenen):</td>
		<td width="300"  bgcolor="#ffffff"><select name=""><option>Hauptnavigation</option><option>Unternavigation von vertikaler Navigation</option></select></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Vertikale Navigation:</td>
		<td width="300"  bgcolor="#ffffff"><select name=""><option>Hauptnavigation</option><option>Unternavigation von horizontaler Navigation</option></select></td>
	  </tr>
		<tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;"><b>Website:</b></td>
		<td width="300"  bgcolor="#ebf2f9">&nbsp;</td>
	  </tr>
	   <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">-- Hintergrundbild:</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_backgroundimage" value="<?= new tplconfig("tplcfg_main_backgroundimage") ?>"></td>
	  </tr>
	   <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">-- Hintergrundfarbe::</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="tplcfg_main_backgroundcolor" value="<?= new tplconfig("tplcfg_main_backgroundcolor") ?>"></td>
	  </tr>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>

<?
foreach($tplItems as $v)
{
?>
<div id="content_tab_<?=$v?>" class="content_tab" style="display:none">
<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Container anpassen</strong></td>
	  </tr>
	<?= showContainerCustomizer($v); ?>
</table>
	<? showNaviCustomizer($v); ?>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
<? } ?>

<div id="content_tab_Teaser" class="content_tab" style="display:none">
<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Container anpassen</strong></td>
	  </tr>
	<?= showContainerCustomizer("teaser"); ?>
</table>
	<? showAreaCustomizer("teaser"); ?>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>

<div id="content_tab_Header" class="content_tab" style="display:none">
<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Container anpassen</strong></td>
	  </tr>
	<?= showContainerCustomizer("header"); ?>
</table>
	<? showAreaCustomizer("header"); ?>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>

<div id="content_tab_Footer" class="content_tab" style="display:none">
<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Container anpassen</strong></td>
	  </tr>
	<?= showContainerCustomizer("footer"); ?>
</table>
	<? showAreaCustomizer("footer"); ?>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>


</form>