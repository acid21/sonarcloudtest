<?

$id = $_GET['id'];
$k = new news($id);

?>
<script type="text/javascript">
<!--
function loeschen(id)
{
	x = confirm("M�chten Sie diesen Eintrag wirklich l�schen?");
	if (x == true) {
	location.href="index.php?" + Base64.encode("s=<?= $s->id; ?>&b=del&id=" + id);
	}
}
//-->
</script>
<? echo $h->initializeTinymce(); ?>
<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title">Newsverwaltung</div>
		<div style="clear:both;">
			<div id="main_buttons" style="width:58px;">
				<p class="main_button"><a href="javascript:document.forms['formData'].submit();" title="Speichern"><img src="images/icon_head_save.png" /></a></p>
				<p class="main_button"><a href="javascript:loeschen('<? echo $id; ?>');" title="L&ouml;schen"><img src="images/icon_head_del.png" /></a></p>
			</div>
			<div class="tab_aktiv" id="tabdaten"><a href="javascript:toggleDisplay('daten');" title="Daten"><img class="tab_img" src="images/icon_tab_edit.png" />Daten</a></div>
<? if (NEWS_ASSETS_HAS_BILDER) { ?>			<div class="tab" id="tabbilder"><a href="javascript:toggleDisplay('bilder');" title="Bilder"><img class="tab_img" src="images/icon_tab_seo.png" />Bilder</a></div><? } ?>
<? if (NEWS_ASSETS_HAS_FILES) { ?>			<div class="tab" id="tabdownloads"><a href="javascript:toggleDisplay('downloads');" title="Downloads"><img class="tab_img" src="images/icon_tab_properties.png" />Downloads</a></div><? } ?>
			
		</div>
	</div>
</div>

<form method="POST" action="<?= webhelper::encodeQs("s=".$s->id . "&b=save&id=" . $id); ?>" name="formData" enctype="multipart/form-data">
<div id="content_tab_daten" class="content_tab">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="<? echo CMS_RTE_WIDTH; ?>">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Datensatz bearbeiten</strong></td>
	  </tr>
<?
if (NEWS_HAS_DATUM) {
if (!empty($k->datum))
{
	$datum = explode("-",$k->datum);
	$datum_t=$datum[2];
	$datum_m=$datum[1];
	$datum_j=$datum[0];
} else {
	$datum_t=strftime("%d");
	$datum_m=strftime("%m");
	$datum_j=strftime("%Y");
}
$bgColor=webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;">Datum:</td>
		<td width="300"  bgcolor="<?= $bgColor; ?>"><select name="datum_t" style="width:40px;"><? for ($i=1;$i<=31;$i++) echo " <option value=\"" . $i . "\"" . (($i==$datum_t) ? " selected" : "") . ">" . $i . "</option>"; ?></select>.<select name="datum_m" style="width:40px;"><? for ($i=1;$i<=12;$i++) echo " <option value=\"" . $i . "\"" . (($i==$datum_m) ? " selected" : "") . ">" . $i . "</option>"; ?></select>.<select name="datum_j" style="width:50px;"><? for ($i=strftime("%Y");$i>=2006;$i--) echo " <option value=\"" . $i . "\"" . (($i==$datum_j) ? " selected" : "") . ">" . $i . "</option>"; ?></select></td>
	  </tr>
<? } ?>
<? $bgColor=webhelper::getCmsBgColor($bgColor); ?>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;">Titel:</td>
		<td width="300"  bgcolor="<?= $bgColor; ?>"><input type="text" name="titel" value="<? echo $h->niceInputOut($k->titel); ?>"></td>
	  </tr>
<?
if (NEWS_HAS_TEASER) {
$bgColor=webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;">kurzer Text:</td>
		<td width="300"  bgcolor="<?= $bgColor; ?>"><textarea name="teaser"><? echo $h->niceInputOut($k->teaser); ?></textarea></td>
	  </tr>
<? } ?>
<?
if (NEWS_HAS_RTE) {
$bgColor=webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
	  	<td width="420" colspan="2" bgcolor="<?= $bgColor; ?>" style="border:none;"><textarea id="rteData" name="rteData" rows="15" cols="80" style="width: 80%"><?
echo htmlspecialchars($k->inhalt);
?></textarea></td>
</tr>
<? } ?>
	</table>

	<div style="padding-top: 10px;">
		<p class="wrap_button"><a title="Zur&uuml;ck zur &Uuml;bersicht" style="padding-left: 5px; padding-right: 4px;" href="<?= webhelper::encodeQs("s=".$s->id."&b=list"); ?>"><img src="images/icon_head_back.png" /></a></p>
		<span style="width:4px; float:left;">&nbsp;</span>
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();" onclick="showSave();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
<? if (NEWS_ASSETS_HAS_BILDER) { ?>	
<div id="content_tab_bilder" class="content_tab" style="display:none;">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="500">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Bilder</strong></td>
	  </tr>
<?
$i=0;
while($row=$k->getNextBild())
{
$i++;
$bgColor=webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
	  	<td width="500"  bgcolor="<? echo $bgColor; ?>" colspan="2" style="border:none;" ><a href="<?= "../assets/". webhelper::getTblDir($k->tblName) . "/" . $row['datei']; ?>" target="_blank"><?= $row['datei'] ?></a></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<? echo $bgColor; ?>" style="border:none;" >Titel:</td>
		<td width="380"  bgcolor="<? echo $bgColor; ?>" valign="bottom"><input type="text" name="attach_titel_<?= $row['id'] ?>" value="<?= webhelper::niceInputOut($row['titel']) ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<? echo $bgColor; ?>" style="border:none;" ><input type="checkbox" name="delAttach[]" value="<?= $row['id']; ?>" style="width:18px;" id="inptAttach<?= $row['id']; ?>"><label for="inptAttach<?= $row['id']; ?>"> Bild l&ouml;schen</label></td>
		<td width="380"  bgcolor="<? echo $bgColor; ?>" valign="bottom">&nbsp;</td>
	  </tr>
<?
}
if ($bgColor=="#ebf2f9") $bgColor = "#ffffff"; else $bgColor = "#ebf2f9";
if ($i<NEWS_ASSETS_BILDER_MAXCOUNT) {
?>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;" >Bild hinzuf&uuml;gen:</td>
		<td width="380"  bgcolor="<?= $bgColor; ?>" valign="bottom"><input type="file" name="bild_new_file" value=""></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;" >Titel:</td>
		<td width="380"  bgcolor="<?= $bgColor; ?>" valign="bottom"><input type="text" name="bild_new_titel" value=""></td>
	  </tr>
	  <tr>
	  	<td width="500"  bgcolor="<?= $bgColor; ?>" style="border:none;" colspan="2">Die maximale Dateigr&ouml;&szlig;e betr&auml;gt 2 MB.</td>
	  </tr>
<? } ?>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a title="Zur&uuml;ck zur &Uuml;bersicht" style="padding-left: 5px; padding-right: 4px;" href="<?= webhelper::encodeQs("s=".$s->id."&b=list"); ?>"><img src="images/icon_head_back.png" /></a></p>
		<span style="width:4px; float:left;">&nbsp;</span>
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();" onclick="showSave();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
<? } ?>
<? if (NEWS_ASSETS_HAS_FILES) { ?>	
<div id="content_tab_downloads" class="content_tab" style="display:none;">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="500">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Downloads</strong></td>
	  </tr>
<?
$bgColor = "#ffffff";
while($row=$k->getNextAnhang())
{
	if ($bgColor=="#ebf2f9") $bgColor = "#ffffff"; else $bgColor = "#ebf2f9";
?>
	  <tr>
	  	<td width="500"  bgcolor="<? echo $bgColor; ?>" colspan="2" style="border:none;" ><a href="<?= "../assets/". webhelper::getTblDir($k->tblName) . "/" .$row['datei']; ?>" target="_blank"><?= $row['datei'] ?></a></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<? echo $bgColor; ?>" style="border:none;" >Titel:</td>
		<td width="380"  bgcolor="<? echo $bgColor; ?>" valign="bottom"><input type="text" name="attach_titel_<?= $row['id'] ?>" value="<?= webhelper::niceInputOut($row['titel']) ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<? echo $bgColor; ?>" style="border:none;" ><input type="checkbox" name="delAttach[]" value="<?= $row['id']; ?>" style="width:18px;" id="inptAttach<?= $row['id']; ?>"><label for="inptAttach<?= $row['id']; ?>"> Datei l&ouml;schen</label></td>
		<td width="380"  bgcolor="<? echo $bgColor; ?>" valign="bottom">&nbsp;</td>
	  </tr>
<?
} 
if ($bgColor=="#ebf2f9") $bgColor = "#ffffff"; else $bgColor = "#ebf2f9";
?>
	  <tr>
	  	<td width="120"  bgcolor="<? echo $bgColor; ?>" style="border:none;" >Datei hinzuf&uuml;gen:</td>
		<td width="380"  bgcolor="<? echo $bgColor; ?>" valign="bottom"><input type="file" name="attach_new_file" value=""></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<? echo $bgColor; ?>" style="border:none;" >Titel:</td>
		<td width="380"  bgcolor="<? echo $bgColor; ?>" valign="bottom"><input type="text" name="attach_new_titel" value=""></td>
	  </tr>
	  <tr>
	  	<td width="500"  bgcolor="<? echo $bgColor; ?>" style="border:none;" colspan="2">Die maximale Dateigr&ouml;&szlig;e betr&auml;gt 2 MB.</td>
	  </tr>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a title="Zur&uuml;ck zur &Uuml;bersicht" style="padding-left: 5px; padding-right: 4px;" href="<?= webhelper::encodeQs("s=".$s->id."&b=list"); ?>"><img src="images/icon_head_back.png" /></a></p>
		<span style="width:4px; float:left;">&nbsp;</span>
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();" onclick="showSave();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
<? } ?>

</form>