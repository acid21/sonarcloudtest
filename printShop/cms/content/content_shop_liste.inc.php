<?
$kat = $_GET['kat'];
$f = "";

if (empty($kat)) $kat = 1;
if (!empty($kat))
{
	$f = " WHERE kategorie = " . $kat . " ";
}

$sql = "SELECT COUNT(*) AS anzahl FROM " . TBL_ARTIKEL . $f . ";";
$res= new dbquery($sql);
$row = $res->getNextRow();
$anzahl = $row['anzahl'];
$anzahlSeiten = ceil($anzahl/CMS_ITEMS_PER_PAGE);


$currentP = $_GET['p'];
if (is_numeric($_POST['p'])) $currentP = $_POST['p'];

if ($currentP=="last") $currentP=$anzahlSeiten;
if ($currentP>$anzahlSeiten) $currentP=$anzahlSeiten;
if ($currentP<1) $currentP=1;

$pPrev = $currentP-1;
$pNext = $currentP+1;

if ($pPrev<1) $pPrev=1;
if ($pNext>$anzahlSeiten) $pNext=$anzahlSeiten;

$hasPrev = ($pPrev<$currentP);
$hasNext = ($currentP<$anzahlSeiten);

$sqlLimit = "";

$currentP--;
if ($currentP<0) $currentP=0;
$sqlStart = $currentP*CMS_ITEMS_PER_PAGE;
$sqlLimit = " LIMIT " . $sqlStart . ", " . CMS_ITEMS_PER_PAGE;



?>
<script type="text/javascript">
<!--
function loeschen(id)
{
	x = confirm("M�chten Sie diesen Eintrag wirklich l�schen?");
	if (x == true) {
	location.href="index.php?s=<? echo $s->id; ?>&a=del&id=" + id;
	}
}
//-->
</script>
<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title"><? echo htmlentities($s->name); ?> <a href="<? echo DOMAIN . $s->seo_url . URL_SUFFIX; ?>" target="_blank">[<? echo DOMAIN . $s->seo_url . URL_SUFFIX; ?>]</a></div>
		<div style="clear:both;">
			<div style="width: 411px;" id="main_buttons">
				<p class="main_button"><a href="<?= webhelper::encodeQs("a=edit&s=" . $s->id . "&id=new&kat=" . $kat); ?>" title="Datensatz hinzuf&uuml;gen"><img src="images/icon_head_add.png" /></a></p>
				<!--<p class="main_button"><a style="width: auto;" href="#" title="Als CSV-Datei exportieren"><img src="images/icon_head_export_xls.png" /></a></p>//-->
				<span style="width:4px; float:left;">&nbsp;</span>
				<span style=" float:left;"><select name="hrsQKat" style="width: 168px; height: 19px; border: 1px solid #bcbbbc; font-size: 11px;" size="1" onchange="location.href='index.php?' + Base64.encode('s=<?= $s->id ?>&kat='+this.value);">
					<option>- Artikelgruppe -</option>
					<option>----------------------------------------</option>
<?
$sql = "SELECT * FROM " . TBL_KATEGORIEN . " ORDER BY titel ASC";
$res = new dbquery($sql);
while($row=$res->getNextRow())
{
?>
					<option value="<? echo $row['id']; ?>"<? if ($kat==$row['id']) echo " selected"; ?>><? echo htmlspecialchars($row['titel']); ?></option>
<? } ?>
				</select></span><span style="width:4px; float:left;">&nbsp;</span>
				<p class="main_button"><? if ($hasPrev) { ?><a style="width: auto;" href="index.php?<? echo $h->removeFromQuerystring($_SERVER['QUERY_STRING'],"p"); ?>&p=1" title="Erste Seite"><? } else echo "<span>"; ?><img src="images/icon_head_arr_ll<? if (!$hasPrev) echo "_off"; ?>.png" /><? if ($hasPrev) echo "</a>"; else echo "</span>"; ?></p>
				<p class="main_button"><? if ($hasPrev) { ?><a style="width: auto;" href="index.php?<? echo $h->removeFromQuerystring($_SERVER['QUERY_STRING'],"p"); ?>&p=<? echo $pPrev; ?>" title="Vorherige Seite"><? }else echo "<span>";  ?><img src="images/icon_head_arr_l<? if (!$hasPrev) echo "_off"; ?>.png" /><? if ($hasPrev) echo "</a>"; else echo "</span>"; ?></p>
				<form method="POST" action="index.php?<? echo $h->removeFromQuerystring($_SERVER['QUERY_STRING'],"p"); ?>" name="formGoto">
				<p style="padding: 4px 4px 0 3px; float: left;">Seite</p>
				<p style="float: left;"><input class="pageselect" style="_margin-top: 2px;" type="text" name="p" value="<? echo $currentP+1; ?>"></p>
				<p style="padding: 3px 3px 0 0; float: left;"><a href="javascript:document.forms['formGoto'].submit();" title="Seite anzeigen"><img src="images/bt_arr_next.png" /></a></p>
				<p style="padding: 4px 3px 0 4px; float: left;">von <? echo $anzahlSeiten; ?></p></form>
				<p class="main_button"><? if ($hasNext) { ?><a style="width: auto;" href="index.php?<? echo $h->removeFromQuerystring($_SERVER['QUERY_STRING'],"p"); ?>&p=<? echo $pNext; ?>" title="N&auml;chste Seite"><? } else echo "<span>"; ?><img src="images/icon_head_arr_r<? if (!$hasNext) echo "_off"; ?>.png" /><? if ($hasNext) echo "</a>"; else echo "</span>"; ?></p>
				<p class="main_button"><? if ($hasNext) { ?><a style="width: auto;" href="index.php?<? echo $h->removeFromQuerystring($_SERVER['QUERY_STRING'],"p"); ?>&p=last" title="Letzte Seite"><? } else echo "<span>"; ?><img src="images/icon_head_arr_rr<? if (!$hasNext) echo "_off"; ?>.png" /><? if ($hasNext) echo "</a>"; else echo "</span>"; ?></p>
			</div>
		</div>
	</div>
</div>

<div id="content_tab" class="content_tab">

	<table class="table" style="border: 1px solid #d4d5d4;" width="615" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="50" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2">&nbsp;</td>
		<td width="65" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2">&nbsp;</td>
		<td width="200" style="border-bottom: 1px solid #d4d5d4;  background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Artikel-Nr.</strong></td>
		<td width="300" style="border-bottom: 1px solid #d4d5d4; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Bezeichnung</strong></td>
	  </tr>
<?


$sql = "SELECT * FROM " . TBL_ARTIKEL . " $f ORDER BY pos ASC " . $sqlLimit . ";";
$res = new dbquery($sql);
$bgColor = "";
while($row=$res->getNextRow())
{
	if ($bgColor=="") $bgColor = " bgcolor=\"#ebf2f9\" "; else $bgColor="";
	$editUrl = webhelper::encodeQs("a=edit&s=" . $s->id . "&id=" . $row['id']);
?>
	  <tr>
		<td <? echo $bgColor; ?>style="border:none;"><a href="<?= $editUrl; ?>"><img src="images/icon_head_textedit.png" border="0"></a><a href="javascript:loeschen('<? echo $row['id']; ?>')"><img src="images/icon_head_del.png" border="0"></a></td>
		<td <? echo $bgColor; ?>style="border:none;"><p class="main_button"><a style="width: auto;" title="Ganz nach oben" href="<?= webhelper::encodeQs("a=sEdit&s=" . S_ID . "&kat=" . $kat . "&b=moveToTop&id=" . $row['id']); ?>"><img src="images/icon_head_arr_uup.gif"></a></p>
		<p class="main_button"><a style="width: auto;" title="Eine Position h&ouml;her" href="<?= webhelper::encodeQs("a=sEdit&s=" . S_ID . "&kat=" . $kat . "&b=moveUp&id=" . $row['id']); ?>"><img src="images/icon_head_arr_up.gif"></a></p>
		<p class="main_button"><a style="width: auto;" title="Eine Position tiefer" href="<?= webhelper::encodeQs("a=sEdit&s=" . S_ID . "&kat=" . $kat . "&b=moveDown&id=" . $row['id']); ?>"><img src="images/icon_head_arr_dn.gif"></a></p>
		<p class="main_button"><a style="width: auto;" title="Ganz nach unten" href="<?= webhelper::encodeQs("a=sEdit&s=" . S_ID . "&kat=" . $kat . "&b=moveToBottom&id=" . $row['id']); ?>"><img src="images/icon_head_arr_ddn.gif"></a></p></td>
		<td <? echo $bgColor; ?>><a href="<?= $editUrl; ?>"><? echo $row['nr']; ?></a>&nbsp;</td>
		<td <? echo $bgColor; ?>><a href="<?= $editUrl; ?>"><? echo $row['bezeichnung']; ?></a>&nbsp;</td>
	  </tr>
<? } ?>
	</table>
</div>