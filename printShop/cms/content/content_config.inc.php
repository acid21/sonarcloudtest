<?

$data = new website(WEBSITE_ID);

?>
<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title">Einstellungen</div>
		<div style="clear:both;">
			<div id="main_buttons" style="width:20px;">
				<p class="main_button"><a href="javascript:document.forms['formData'].submit();" title="Speichern"><img src="images/icon_head_save.png" /></a></p>
			</div>
		</div>
	</div>
</div>

<div id="content_tab" class="content_tab">
<form method="POST" action="index.php?<? echo $_SERVER['QUERY_STRING']; ?>" name="formData" ENCTYPE="multipart/form-data">
<input type="hidden" name="save" value="config">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0"  width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Layout anpassen</strong></td>
	  </tr>
	   <tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Name:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="name" value="<? echo $h->niceInputOut($data->name); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Domain:</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="domain" value="<? echo $h->niceInputOut($data->domain); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Pfad-Root:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="pfad_root" value="<? echo $h->niceInputOut($data->pfad_root); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Meta-Title:</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="meta_title" value="<? echo $h->niceInputOut($data->meta_title); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Meta-Description:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="meta_description" value="<? echo $h->niceInputOut($data->meta_description); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Meta-Keywords:</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="meta_keywords" value="<? echo $h->niceInputOut($data->meta_keywords); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Template:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="templateId" value="<? echo $h->niceInputOut($data->templateId); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Headerbild:</td>
		<td width="300"  bgcolor="#ffffff"><input type="file" name="file_headerbild" value=""></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Bild in &quot;images&quot; laden:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="file" name="file_images" value=""></td>
	  </tr>
	</table>
<p>&nbsp;</p>
<?
$configItems = getTplConfig();
?>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="550">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Template anpassen</strong></td>
	  </tr>
<?
$bgColor = "#ffffff";
foreach($configItems as $v)
{
	if ($bgColor=="#ffffff") $bgColor = "#ebf2f9"; else $bgColor = "#ffffff";
	$t = explode(":",$v);
	$name = $t[0];
	$k = $t[1];
?>
	   <tr>
	  	<td width="120"  bgcolor="<?=$bgColor?>" style="border:none;"><?= webhelper::niceHtml($name) ?>:</td>
<? if ($k=="color") { ?>
		<td width="300"  bgcolor="<?=$bgColor?>"><input type="text" name="tplcfg_<?=$v?>" value="<? echo new tplconfig($v); ?>"  onclick="showColorPicker(this,this)"></td>
<? } elseif ($k=="size") { ?>
		<td width="300"  bgcolor="<?=$bgColor?>"><input type="text" name="tplcfg_<?=$v?>" value="<? echo new tplconfig($v); ?>"></td>
<? } elseif ($k=="imagefile") { ?>
		<td width="300"  bgcolor="<?=$bgColor?>"><input type="file" name="tplcfg_<?=$v?>" value="<? echo new tplconfig($v); ?>"></td>
<? } else { ?>
	<td width="300"  bgcolor="<?=$bgColor?>"><input type="text" name="tplcfg_<?=$v?>" value="<? echo new tplconfig($v); ?>"></td>
<? } ?>
	  </tr>
<? } ?>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	<div>
</form>

</div>
