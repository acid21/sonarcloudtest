<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title"><? echo webhelper::niceHtml($s->name); ?> <a href="<?= DOMAIN . webhelper::getUrl($s->id, $s->seo_url); ?>" target="_blank">[<?= DOMAIN . webhelper::getUrl($s->id, $s->seo_url); ?>]</a></div>
		<div style="clear:both;">
			<div id="main_buttons" style="width:30px;">
				<p class="main_button"><a href="javascript:document.forms['formData'].submit();" title="Speichern"><img src="images/icon_head_save.png" /></a></p>
			</div>
			<div class="tab_aktiv" id="tabNormal"><a href="javascript:toggleDisplay('Normal');" title="Normal (WYSIWYG-Modus)"><img class="tab_img" src="images/icon_tab_edit.png" />Normal</a></div>
<? if (RTE_ASSETS_HAS_FILES) { ?>			<div class="tab" id="tabdownloads"><a href="javascript:toggleDisplay('downloads');" title="Downloads"><img class="tab_img" src="images/icon_tab_seo.png" />Downloads</a></div><? } ?>
			<div class="tab" id="tabSeo"><a href="javascript:toggleDisplay('Seo');" title="Suchmaschinen (Seitentitel und Meta-Tags)"><img class="tab_img" src="images/icon_tab_seo.png" />Suchmaschinen</a></div>
			<div class="tab" id="tabProperties"><a href="javascript:toggleDisplay('Properties');" title="Seiteneigenschaften"><img class="tab_img" src="images/icon_tab_properties.png" />Eigenschaften</a></div>
		</div>
	</div>
</div>

<form method="POST" action="<?= webhelper::encodeQs("a=sEdit&b=edit&s=" . $s->id) ?>" name="formData" ENCTYPE="multipart/form-data">
<input type="hidden" name="save" value="content">


<?= $h->initializeTinymce(); ?>

<div id="content_tab_Normal" class="content_tab">
<? if (!empty($statusBox)) { ?>
<div style="width:<?= CMS_RTE_WIDTH; ?>px; margin-bottom:7px;border:2px solid #00ff00;background-color:#f0fff0;background-image:url('images/icon_success.png');background-position-x:10px;background-position-y:50%;background-repeat:no-repeat;padding:0.2em 0.1em 0.2em 36px;"><?= strftime("%d.%m.%Y %H:%M:%S"); ?> Uhr: <?= webhelper::niceHtml($statusBox); ?></div>
<? } ?>
<? if ($s->isWeiterleitung()) { ?>
	<div id="divError" style="width:<?= CMS_RTE_WIDTH ?>px; margin-bottom:7px;border:2px solid #cc0000;background-color:#ffffcc;background-image:url('images/icon_error.png');background-repeat:no-repeat;background-position:7px center;padding:0.2em 5px 0.2em 36px;"><?= webhelper::niceHtml("Dieser Inhalt wird auf der Website nicht angezeigt, da der Men�punkt auf eine andere Seite weitergeleitet wird!"); ?></div>
<? } elseif (!empty($s->contentFile)&&file_exists("../content/".$s->contentFile)) { ?>
	<div id="divError" style="width:<?= CMS_RTE_WIDTH ?>px; margin-bottom:7px;border:2px solid #cc0000;background-color:#ffffcc;background-image:url('images/icon_error.png');background-position:7px center;background-repeat:no-repeat;padding:0.2em 5px 0.2em 36px;"><?= webhelper::niceHtml("Dieser Inhalt wird auf der Website nicht angezeigt, da eine andere Datei (\"" . $s->contentFile . "\") eingebunden wird!"); ?></div>
<? } ?>
	<textarea id="rteData" name="rteData" rows="15" cols="80" style="width: 80%"><?

echo $s->inhalt;

?></textarea>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
<div id="content_tab_Properties" class="content_tab" style="display:none;">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="420">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Seiteneigenschaften bearbeiten</strong></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="<? $bgColor=webhelper::getCmsBgColor(""); echo $bgColor; ?>" style="border:none;">Headerbild:</td>
		<td width="300" bgcolor="<?= $bgColor; ?>"><select name="selectBild1" onchange="updateTrUploadBild1(this.value);">
<option value="">-- Standard --</option>
<option value="--new--">-- Neue Datei hochladen --</option>
<?
$res = new dirInfo("../".PATH_HEADERBILDER);
while($row=$res->getNextFile())
{
	echo "<option value=\"" . $row . "\" " . (($row==$s->bild1) ? "selected" : "") . ">" . $row . "</option>";
}		
?></select></td>
	  </tr>
	  <tr id="trUploadBild1" style="display:none;">
	  	<td width="120" bgcolor="<? $bgColor=webhelper::getCmsBgColor(""); echo $bgColor; ?>" style="border:none;">Bild hochladen:</td>
		<td width="300" bgcolor="<?= $bgColor; ?>"><input type="file" name="fileHeaderbild"><br>Die Datei sollte im Format 820x240px vorliegen.</td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="<? $bgColor=webhelper::getCmsBgColor($bgColor); echo $bgColor; ?>" style="border:none;">Inhaltstyp:</td>
		<td width="300" bgcolor="<?= $bgColor; ?>"><select name="wlSwitch" onchange="updateTrWlTarget(this.value);">
	<option value="1"<? if (!$s->isWeiterleitung()) echo " selected" ; ?>>eigener Inhalt</option>
	<option value="2"<? if ($s->isWeiterleitung()) echo " selected" ; ?>>Weiterleitung</option>
</select></td>
	  </tr>
	  <tr<? if (!$s->isWeiterleitung()) echo " style=\"display:none;\""; ?> id="trWlTarget">
	  	<td width="120" bgcolor="<?= $bgColor; ?>" style="border:none;">Weiterleitung auf:</td>
		<td width="300" bgcolor="<?= $bgColor; ?>"><select name="wlTarget">
<?
$res = webhelper::getStrukturCms(0,1,1);
while($row=$res->getNextRow())
{
?>
	<option value="<?= $row['id']; ?>"<?= (($row['id']==$s->wlTarget) ? " selected" : "") ?>><?= webhelper::niceHtml($row['name']); ?></option>
<?
$res2 = webhelper::getStrukturCms($row['id']);
while($row2=$res2->getNextRow())
{
?>
	<option value="<?= $row2['id']; ?>"<?= (($row2['id']==$s->wlTarget) ? " selected" : "") ?>>--- <?= webhelper::niceHtml($row2['name']); ?></option>
<?
$res3 = webhelper::getStrukturCms($row2['id']);
while($row3=$res3->getNextRow())
{
?>
	<option value="<?= $row3['id']; ?>"<?= (($row3['id']==$s->wlTarget) ? " selected" : "") ?>>------ <?= webhelper::niceHtml($row3['name']); ?></option>

<? } } } ?>
</select></td>
	  </tr>
	</table>
	<p>&nbsp;</p>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="420">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Zugriffsschutz</strong></td>
	  </tr>
<?
if ($s->isProtectedByParent())
{
?>
	  <tr>
	  	<td width="420" bgcolor="#ebf2f9" style="border:none;">Zugriff durch &uuml;bergeordnete Seite gesch&uuml;tzt.</td>
	  </tr>
<?
} else {
?>
	  <tr>
	  	<td width="120" bgcolor="#ebf2f9" style="border:none;">Zugriff:</td>
		<td width="300" bgcolor="#ebf2f9"><select name="isProtected" onchange="updateTrAccess(this.value);"><option value="0">nicht gesch&uuml;tzt</option><option value="1"<? if ($s->isProtected()) echo " selected"; ?>>gesch&uuml;tzt</option></select></td>
	  </tr>
	 <tr <? if (!$s->isProtected()) echo " style=\"display:none;\""; ?> id="trAccess1">
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Benutzername:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="benutzername" value="<?= webhelper::niceInputOut($s->benutzername); ?>"></td>
	  </tr>
	  <tr <? if (!$s->isProtected()) echo " style=\"display:none;\""; ?> id="trAccess2">
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Passwort:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="passwort" value="<?= webhelper::niceInputOut($s->passwort); ?>"></td>
	  </tr>
<? } ?>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>

<div id="content_tab_Seo" class="content_tab" style="display:none;">
	<div id="divError" style="display:none;width:420px; margin-bottom:7px;border:2px solid #cc0000;background-color:#ffffcc;background-image:url('images/icon_error.png');background-position-x:10px;background-position-y:50%;background-repeat:no-repeat;padding:0.2em 0.1em 0.2em 36px;"><?= webhelper::niceHtml("Diese URL wird bereits verwendet!"); ?></div>

	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="420">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Seiteneigenschaften (SEO) bearbeiten</strong></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ebf2f9" style="border:none;">URL:</td>
		<td width="300" bgcolor="#ebf2f9"><input type="text" name="seo_url" value="<?= webhelper::niceInputOut($s->seo_url); ?>" onkeyup="return checkSeoUrl(this.value);"></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ffffff" style="border:none;">Titel:</td>
		<td width="300" bgcolor="#ffffff"><input type="text" name="seo_title" value="<?= webhelper::niceInputOut($s->seo_title); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ebf2f9" style="border:none;">Meta-Description:</td>
		<td width="300" bgcolor="#ebf2f9"><input type="text" name="seo_description" value="<?= webhelper::niceInputOut($s->seo_description); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ffffff" style="border:none;">Meta-Keywords:</td>
		<td width="300" bgcolor="#ffffff"><input type="text" name="seo_keywords" value="<?= webhelper::niceInputOut($s->seo_keywords); ?>"></td>
	  </tr>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>

<? if (RTE_ASSETS_HAS_FILES) { ?>
<div id="content_tab_downloads" class="content_tab" style="display:none;">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="500">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Downloads</strong></td>
	  </tr>
<?
$bgColor = "#ffffff";
while($row=$s->getNextAnhang())
{
	$bgColor=webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
	  	<td width="500"  bgcolor="<?= $bgColor; ?>" colspan="2" style="border:none;" ><a href="<?= "../assets/". webhelper::getTblDir($k->tblName) . "/" .$row['datei']; ?>" target="_blank"><?= $row['datei'] ?></a></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;" >Titel:</td>
		<td width="380"  bgcolor="<?= $bgColor; ?>" valign="bottom"><input type="text" name="attach_titel_<?= $row['id'] ?>" value="<?= webhelper::niceInputOut($row['titel']) ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;" ><input type="checkbox" name="delAttach[]" value="<?= $row['id']; ?>" style="width:18px;" id="inptAttach<?= $row['id']; ?>"><label for="inptAttach<?= $row['id']; ?>"> Datei l&ouml;schen</label></td>
		<td width="380"  bgcolor="<?= $bgColor; ?>" valign="bottom">&nbsp;</td>
	  </tr>
<?
} 
$bgColor=webhelper::getCmsBgColor($bgColor);
?>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;" >Datei hinzuf&uuml;gen:</td>
		<td width="380"  bgcolor="<?= $bgColor; ?>" valign="bottom"><input type="file" name="attach_new_file" value=""></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="<?= $bgColor; ?>" style="border:none;" >Titel:</td>
		<td width="380"  bgcolor="<?= $bgColor; ?>" valign="bottom"><input type="text" name="attach_new_titel" value=""></td>
	  </tr>
	  <tr>
	  	<td width="500"  bgcolor="<?= $bgColor; ?>" style="border:none;" colspan="2">Die maximale Dateigr&ouml;&szlig;e betr&auml;gt 2 MB.</td>
	  </tr>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
<? } ?>

</form>
