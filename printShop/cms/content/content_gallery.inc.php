<?php

if ($b=="save")
{
	$id = $_GET['id'];
	
	$k = new galleryPic($id);
	if ($id=="new") $k->parent = S_ID;
	$k->titel = $h->niceDbString($_POST['titel']);
	
	if (is_uploaded_file($_FILES['bild']['tmp_name']))
	{
		$v = $_FILES['bild'];	
		$dateiname = $k->id . "_" . webhelper::zufallsstring(6) . "_". preg_replace("/[^a-zA-Z0-9 _.]/","",$v['name']);
		move_uploaded_file($v['tmp_name'], "../assets/" . webhelper::getTblDir($k->tblName) . "/" . $dateiname);
		chmod("../assets/" . webhelper::getTblDir($k->tblName) . "/" . $dateiname, 0777);
		$k->datei = $dateiname;
		
		$pathInfo = pathinfo($dateiname);
		$dateinameV2 = str_replace(".".$pathInfo['extension'],"",$dateiname) . "_v2" . ".".$pathInfo['extension'];
		
		$k ->datei_v2 = $dateinameV2;
		$b = new img("../assets/" . webhelper::getTblDir($k->tblName) . "/" . $dateiname);
		$b->saveAs("../assets/" . webhelper::getTblDir($k->tblName) . "/" . $dateinameV2, 100, 100);
	}
	
	$k->save();
	if ($id=="new") $id=$k->id;
	
	$b = "list";
}
if ($b=="del")
{
	$id = $_GET['id'];
	$k = new galleryPic($id);
	$k->delete();
}
webhelper::moveDataset($id,TBL_GALLERY_PICS,$b,array("parent"=>S_ID));

if ($b=="edit") include("content/content_gallery_edit.inc.php");
else include("content/content_gallery_list.inc.php");
?>
