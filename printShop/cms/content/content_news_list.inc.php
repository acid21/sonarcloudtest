<script type="text/javascript">
<!--
function loeschen(id)
{
	x = confirm("M�chten Sie diesen Eintrag wirklich l�schen?");
	if (x == true) {
	location.href="index.php?" + Base64.encode("s=<?= $s->id; ?>&b=del&id=" + id);
	}
}
//-->
</script>
<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title">Newsverwaltung</div>
		<div style="clear:both;">
			<div id="main_buttons" style="width:20px;">
				<p class="main_button"><a href="<?= webhelper::encodeQs("a=sEdit&s=" . $s->id . "&b=edit&id=new"); ?>" title="Neuen Datensatz erstellen"><img src="images/icon_head_add.png" /></a></p>
			</div>
			<div class="tab_aktiv" id="tabNormal"><a href="javascript:toggleDisplay('Normal');" title="Normal (WYSIWYG-Modus)"><img class="tab_img" src="images/icon_tab_edit.png" />Normal</a></div>
			<div class="tab" id="tabSeo"><a href="javascript:toggleDisplay('Seo');" title="Suchmaschinen (Seitentitel und Meta-Tags)"><img class="tab_img" src="images/icon_tab_seo.png" />Suchmaschinen</a></div>
			<div class="tab" id="tabProperties"><a href="javascript:toggleDisplay('Properties');" title="Seiteneigenschaften"><img class="tab_img" src="images/icon_tab_properties.png" />Eigenschaften</a></div>
		</div>
	</div>
</div>

<div id="content_tab_Normal" class="content_tab">
	<div style="margin-top:10px;">
		<p class="wrap_button"><a href="<?= webhelper::encodeQs("a=sEdit&s=" . S_ID . "&b=rteEdit&id=" . $s->id); ?>"><img style="float:left;margin-right: 5px;" src="images/icon_tab_edit.png" /><b>Einleitung bearbeiten</b></a></p>
	</div>
	<div style="clear:both;margin-bottom:10px;"></div>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="720">
	  <tr>
	  <td width="70" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2">&nbsp;</td>
		<td width="200" style="border-bottom: 1px solid #d4d5d4; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Titel</strong></td>
		<td width="450" style="border-bottom: 1px solid #d4d5d4; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>kurzer Text</strong></td>
	  </tr>
<?
$sql = "SELECT * FROM " . TBL_NEWS . " WHERE parent = " . $s->id . " ORDER BY pos ASC, id DESC;";
$res= new dbquery($sql);
$bgColor = "";
while($row=$res->getNextRow())
{
	if ($bgColor=="") $bgColor = " bgcolor=\"#ebf2f9\" "; else $bgColor="";
?>
	  <tr>
	  <td width="70" <? echo $bgColor; ?>style="border:none;"><a href="<?= webhelper::encodeQs("s=" . $s->id . "&b=edit&id=".$row['id']); ?>"><img src="images/icon_head_textedit.png" border="0"></a><a href="javascript:loeschen('<? echo $row['id']; ?>')"><img src="images/icon_head_del.png" border="0"></a></td>
		<td width="200"  <? echo $bgColor; ?>><a href="<?= webhelper::encodeQs("s=" . $s->id . "&b=edit&id=".$row['id']); ?>"><? echo $row['titel']; ?></a>&nbsp;</td>
		<td width="450" <? echo $bgColor; ?>><a href="<?= webhelper::encodeQs("s=" . $s->id . "&b=edit&id=".$row['id']); ?>"><? echo nl2br($row['teaser']); ?>&nbsp;</td>
	  </tr>
<? } ?>

	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="<?= webhelper::encodeQs("a=sEdit&s=" . $s->id . "&b=edit&id=new"); ?>"><img style="float:left; margin-right: 5px;" src="images/icon_head_add.png" /><b>Neuen Datensatz erstellen</b></a></p>
	</div>
	
</div>

<form method="POST" action="#">
<div id="content_tab_Properties" class="content_tab" style="display:none;">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="420">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Seiteneigenschaften bearbeiten</strong></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ebf2f9" style="border:none;">Inhaltstyp:</td>
		<td width="300" bgcolor="#ebf2f9"><select name="contentType" onchange="updateTrWlTarget(this.value);">
	<option value="1"<? if ($s->contentType==1) echo " selected" ; ?>>eigener Inhalt</option>
	<option value="2"<? if ($s->contentType==2) echo " selected" ; ?>>Weiterleitung</option>
</select></td>
	  </tr>
	  <tr<? if (!$s->isWeiterleitung()) echo " style=\"display:none;\""; ?> id="trWlTarget">
	  	<td width="120" bgcolor="#ffffff" style="border:none;">Weiterleitung auf:</td>
		<td width="300" bgcolor="#ffffff"><select name="wlTarget">
<?
$res = webhelper::getStrukturCms(0,1,1);
while($row=$res->getNextRow())
{
?>
	<option value="<?= $row['id']; ?>"<?= (($row['id']==$s->wlTarget) ? " selected" : "") ?>><?= webhelper::niceHtml($row['name']); ?></option>
<?
$res2 = webhelper::getStrukturCms($row['id']);
while($row2=$res2->getNextRow())
{
?>
	<option value="<?= $row2['id']; ?>"<?= (($row2['id']==$s->wlTarget) ? " selected" : "") ?>>--- <?= webhelper::niceHtml($row2['name']); ?></option>
<?
$res3 = webhelper::getStrukturCms($row2['id']);
while($row3=$res3->getNextRow())
{
?>
	<option value="<?= $row3['id']; ?>"<?= (($row3['id']==$s->wlTarget) ? " selected" : "") ?>>------ <?= webhelper::niceHtml($row3['name']); ?></option>
<? } } } ?>
</select></td>
	  </tr>
	</table>
	<p>&nbsp;</p>
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="420">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Zugriffsschutz</strong></td>
	  </tr>
<?
if ($s->isProtectedByParent())
{
?>
	  <tr>
	  	<td width="420" bgcolor="#ebf2f9" style="border:none;">Zugriff durch &uuml;bergeordnete Seite gesch&uuml;tzt.</td>
	  </tr>
<?
} else {
?>
	  <tr>
	  	<td width="120" bgcolor="#ebf2f9" style="border:none;">Zugriff:</td>
		<td width="300" bgcolor="#ebf2f9"><select name="isProtected" onchange="updateTrAccess(this.value);"><option value="0">nicht gesch&uuml;tzt</option><option value="1"<? if ($s->isProtected()) echo " selected"; ?>>gesch&uuml;tzt</option></select></td>
	  </tr>
	 <tr <? if (!$s->isProtected()) echo " style=\"display:none;\""; ?> id="trAccess1">
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Benutzername:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="benutzername" value="<?= webhelper::niceInputOut($s->benutzername); ?>"></td>
	  </tr>
	  <tr <? if (!$s->isProtected()) echo " style=\"display:none;\""; ?> id="trAccess2">
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Passwort:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="passwort" value="<?= webhelper::niceInputOut($s->passwort); ?>"></td>
	  </tr>
<? } ?>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:submitDataForm();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>

<div id="content_tab_Seo" class="content_tab" style="display:none;">
	<div id="divError" style="display:none;width:420px; margin-bottom:7px;border:2px solid #cc0000;background-color:#ffffcc;background-image:url('images/icon_error.png');background-position-x:10px;background-position-y:50%;background-repeat:no-repeat;padding:0.2em 0.1em 0.2em 36px;"><?= webhelper::niceHtml("Diese URL wird bereits verwendet!"); ?></div>

	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="420">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Seiteneigenschaften (SEO) bearbeiten</strong></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ebf2f9" style="border:none;">URL:</td>
		<td width="300" bgcolor="#ebf2f9"><input type="text" name="seo_url" value="<?= webhelper::niceInputOut($s->seo_url); ?>" onkeyup="return checkSeoUrl(this.value);"></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ffffff" style="border:none;">Titel:</td>
		<td width="300" bgcolor="#ffffff"><input type="text" name="seo_title" value="<?= webhelper::niceInputOut($s->seo_title); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ebf2f9" style="border:none;">Meta-Description:</td>
		<td width="300" bgcolor="#ebf2f9"><input type="text" name="seo_description" value="<?= webhelper::niceInputOut($s->seo_description); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ffffff" style="border:none;">Meta-Keywords:</td>
		<td width="300" bgcolor="#ffffff"><input type="text" name="seo_keywords" value="<?= webhelper::niceInputOut($s->seo_keywords); ?>"></td>
	  </tr>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a href="javascript:submitDataForm();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	</div>
</div>
</form>