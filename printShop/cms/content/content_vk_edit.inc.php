<?
$kunde = new kunde($_GET['kunde']);


if ($_POST['do']=="updateRueckseite")
{
	$t= new vorlage($_GET['id']);
	$t->qr_active = (($_POST['qr_active']=="true") ? "1":"0");
	$t->qr_back = (($_POST['qr_back']=="true") ? "1":"0");
	$t->qr_breite = $_POST['qr_breite'];
	$t->qr_xpos = $_POST['qr_xpos'];
	$t->qr_ypos = $_POST['qr_ypos'];

	$t->qr_farbe = $_POST['qr_farbe_c'] . ";" . $_POST['qr_farbe_m'] . ";" . $_POST['qr_farbe_y'] . ";" . $_POST['qr_farbe_k'];
	$t->qr_bgcolor = $_POST['qr_bgcolor_c'] . ";" . $_POST['qr_bgcolor_m'] . ";" . $_POST['qr_bgcolor_y'] . ";" . $_POST['qr_bgcolor_k'];
	
	$temp = "";
	foreach($_POST['qrActiveField'] as $v)
		$temp.=",".$v;
	
	$t->qr_fields = $temp;
	
	$t->save();
}

if ($_POST['do']=="updateMasse")
{
	$t= new vorlage($_GET['id']);
	$t->name = $_POST['name'];
	$t->breite = $_POST['breite'];
	$t->hoehe = $_POST['hoehe'];
	$t->save();
}

if ($_POST['do']=="updateReAdre")
{
	$t= new vorlage($_GET['id']);
	$t->reAdresse = $_POST['reAdresse'];
	$t->save();
}

if (isset($_FILES['upload_schrift']))
{
	if (is_uploaded_file($_FILES['upload_schrift']['tmp_name']))
	{
		move_uploaded_file($_FILES['upload_schrift']['tmp_name'], "../../shop/schriften/" . strtoupper($_FILES['upload_schrift']['name']));
	}
}

if (isset($_FILES['upload_bgpdf']))
{
	if (is_uploaded_file($_FILES['upload_bgpdf']['tmp_name']))
	{
		$dateiname = $_GET['id'] . "_" . str_replace(" ", "", $_FILES['upload_bgpdf']['name']);
		# $dateiname = preg_replace("/[^a-zA-Z0-9 _.]/","",$_FILES['upload_bgpdf']['name']);
		move_uploaded_file($_FILES['upload_bgpdf']['tmp_name'], "../bg/" . $dateiname);
		chmod("../bg/" . $dateiname,0777);
		$t= new vorlage($_GET['id']);
		$t->bgPdf = $dateiname;
		$t->save();
	}
}


if (!empty($_GET['y']))
{
	$t = new textdata($_GET['updateText']);
	if ($_GET['y'] == "left") { $t->align = "left"; }
	if ($_GET['y'] == "center") { $t->align = "center"; }
	if ($_GET['y'] == "right") { $t->align = "right"; }
	$t->save();
}

if (is_numeric($_POST['updateText'])||$_POST['updateText']=="new")
{
	

	$t = new textdata($_POST['updateText']);

	
	if (empty($_POST['text_groesse'])) $textGroesse = 10; else $textGroesse = $_POST['text_groesse'];
	
	if ($_POST['text_fc']==="") $textFc = 75; else $textFc = $_POST['text_fc'];
	if ($_POST['text_fm']==="") $textFm = 68; else $textFm = $_POST['text_fm'];
	if ($_POST['text_fy']==="") $textFy = 67; else $textFy = $_POST['text_fy'];
	if ($_POST['text_fk']==="") $textFk = 90; else $textFk = $_POST['text_fk'];
	
	if ($_POST['text_x']!=0&&empty($_POST['text_x'])) $textX = 10; else $textX = $_POST['text_x'];
	if ($_POST['text_x']!=0&&empty($_POST['text_y'])) $textY = 10; else $textY = $_POST['text_y'];
	
	if (empty($_POST['text_schriftart'])) $schriftart = "ARIALBD"; else $schriftart = $_POST['text_schriftart'];
	
	//$farbe = $_POST['text_fc'] . ";" . $_POST['text_fm'] . ";" . $_POST['text_fy'] . ";" . $_POST['text_fk'];
	
	
	$textX = str_replace(",", ".", $textX);
	$textY = str_replace(",", ".", $textY);
	$textGroesse = str_replace(",", ".", $textGroesse);
	
	if ($_POST['updateText']=="new")
	{
		$myPosx_abhlb = 0;
	} else {
		$myPosx_abhlb = $_POST['posx_abhlb'];
		
		if ($myPosx_abhlb==$_POST['updateText'])
		{
			$myPosx_abhlb = 0;
		}
	}
	
	

	if ($_POST['updateText']=="new") $t->vorlagen_id = $_GET['id'];
	if ($_POST['updateText']=="new") $t->align = "left";
	
	$t->name= $_POST['text_name'];
	$t->beschreibung= $_POST['text_beschreibung'];
	$t->posx = $textX;
	$t->posx_abhlb = $myPosx_abhlb;
	$t->posy = $textY;
	$t->schrift = $schriftart;
	$t->schriftgroesse = $textGroesse;
	$t->farbe = $textFc . ";" . $textFm . ";" . $textFy . ";" . $textFk;
	$t->zusatz = $_POST['text_zusatz'];
	$t->zusatz_l = $_POST['text_zusatz_l'];
	$t->nameonly = $_POST['text_nameonly'];
	$t->statisch = $_POST['text_statisch'];
	
	$t->save();
	
	if ($myPosx_abhlb>0)
	{
		$res = new dbquery("UPDATE " . TBL_TEXTDATA . " SET posx_abhlb = " .$myPosx_abhlb." WHERE posx_abhlb = " . $t->id . " AND vorlagen_id = " . $_GET['id'] . ";");
		
	}
}

if (is_numeric($_GET['delId']))
{
	$t= new textdata($_GET['delId']);
	$t->delete();
	$res = new dbquery("UPDATE " . TBL_TEXTDATA . " SET posx_abhlb = 0 WHERE posx_abhlb = " . $_GET['delId'] . ";");
}

$vlg= new vorlage($_GET['id']);
?>
?>
<script type="text/javascript">
<!--


function loeschen(id)
{
	x = confirm("M�chten Sie diesen Text wirklich l�schen?");
	if (x == true) {
	location.href="index.php?" + Base64.encode("a=vk&b=edit&id=<? echo $_GET['id']; ?>&kunde=<? echo $kunde->id; ?>&delId=" + id);
	}
}

//-->
</script>
<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title">Visitenkarte bearbeiten</div>
		<div style="clear:both;">
			<div id="main_buttons" style="width:90px">
				<p class="main_button"><a href="javascript:document.forms['formData'].submit();" title="Speichern"><img src="images/icon_head_save.png" /></a></p>
				<!--<p class="main_button"><a href="javascript:loeschen('<? echo $id; ?>');" title="L&ouml;schen"><img src="images/icon_head_del.png" /></a></p>//-->
				<p class="main_button" ><a href="../showPdf.php?vid=<? echo $id; ?>&r=<? echo md5(microtime()); ?>" target="_blank" title="PDF-Vorschau anzeigen"><img src="images/icon_tab_preview.png" /></a></p>
			</div>
		</div>
	</div>
</div>

<div id="content_tab" class="content_tab">

<input type="hidden" name="save" value="go">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="1150">
	  <tr>
	  	<td colspan="1" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Textfelder bearbeiten</strong></td>
	  	<td colspan="1" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Name</strong></td>
	  	<td colspan="1" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>x-Pos</strong></td>
	  	<td colspan="1" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>y-Pos</strong></td>
	  	<td colspan="5" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Schriftgr&ouml;&szlig;e / -art / -farbe</strong></td>
	  </tr>
	  
	  
<?
function GetDirArray($sPath)
{
	//Load Directory Into Array
	$handle=opendir($sPath);
	while ($file = readdir($handle))
        if ($file != "." && $file != "..") { @$retVal[count($retVal)] = strtoupper($file); }

	//Clean up and sort
	closedir($handle);
	sort($retVal);
	return $retVal;
}

$schriften = GetDirArray("../../shop/schriften");
for ($i = 0; $i < count($schriften); $i++)
{
$html_schrift_auswahlbox .= "<option value=\"" . basename ($schriften[$i],".TTF") . "\">" . basename ($schriften[$i],".TTF") . "</option>";
}
$bgColor = "#ffffff";

$availIds = array();
$res = new dbquery("SELECT * FROM " . TBL_TEXTDATA . " WHERE vorlagen_id = " . $_GET['id'] . " ORDER BY id ASC");
while($row=$res->getNextRow())
{
	if ($row['posx_abhlb']>0) continue;
	$availIds[$row['id']] = $row['name'];
}
@$res->resetZeiger();

while($row=$res->getNextRow())
{
	
	$bgColor = webhelper::getCmsBgColor($bgColor);
	$farbe = explode(";", $row['farbe']);
	if (@$farbe[0] == "") $farbe[0] = 0;
	if (@$farbe[1] == "") $farbe[1] = 0;
	if (@$farbe[2] == "") $farbe[2] = 0;
	if (@$farbe[3] == "") $farbe[3] = 0;
?>
	  <form method="POST" action="<?echo  webhelper::encodeQs("a=vk&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>" name="form_<? echo $row['id']; ?>">
	  <input type="hidden" name="updateText" value="<?= $row['id']; ?>">
	  <input type="hidden" name="vid" value="13">
	  <tr>
	  <td width="340" bgcolor="<?= $bgColor; ?>" style="padding: 2px; border-left: 0;" bgcolor="<? echo $bgColor; ?>">
				<p class="main_button"><a style="width: auto;" title="Speichern" href="javascript:document.forms['form_<? echo $row['id']; ?>'].submit();"><img src="images/icon_head_save.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="L&ouml;schen" href="javascript:loeschen('<?= $row['id']; ?>')"><img src="images/icon_head_del.png"></a></p>
				<p class="main_button"><a style="width: auto;" title="Linksb&uuml;ndig" href="<?= webhelper::encodeQs("a=vk&updateText=" . $row['id'] . "&y=left&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>"><img src="images/bt_l_align<? if ($row['align'] == "left") echo "_on"; ?>.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Zentriert" href="<?= webhelper::encodeQs("a=vk&b=edit&updateText=" . $row['id'] . "&y=center&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>"><img src="images/bt_m_align<? if ($row['align'] == "center") echo "_on"; ?>.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Rechtsb&uuml;ndig" href="<?= webhelper::encodeQs("a=vk&b=edit&updateText=" . $row['id'] . "&y=right&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>"><img src="images/bt_r_align<? if ($row['align'] == "right") echo "_on"; ?>.gif"></a></p>
				<input type="checkbox" name="text_zusatz_l" value="true"<? if ($row['zusatz_l'] == "true") echo " checked"; ?> style="width:20px" title="Zusatz links"><input type="checkbox" name="text_zusatz" value="true"<? if ($row['zusatz'] == "true") echo " checked"; ?> style="width:20px" title="Zusatz rechts"><input type="checkbox" name="text_nameonly" value="true"<? if ($row['nameonly'] == "true") echo " checked"; ?> style="width:20px" title="Nur Name"><input type="checkbox" name="text_statisch" value="true"<? if ($row['statisch'] == "true") echo " checked"; ?> title="Statisch" style="width:20px">
				<? echo $row['id']; ?>
			</td>
	  	<td width="200"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_name" value="<? echo htmlspecialchars($row['name']); ?>" style="width:200px;"><br><input type="text" name="text_beschreibung" title="Beschreibung" value="<? echo htmlspecialchars($row['beschreibung']); ?>" style="width:200px;"></td>
		<td width="80"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_x" value="<? echo str_replace(".",",",$row['posx']); ?>" style="width:80px;"><br><select name="posx_abhlb"  style="width:80px;">
<option value="0">linksb. wie:</option>
<?
foreach($availIds as $k=> $v)
{
	if($k==$row['id']) continue;
?>
<option value="<? echo $k; ?>" title="<? echo htmlspecialchars($v); ?>"<? if ($row['posx_abhlb']==$k) echo " selected"; ?>><? echo htmlspecialchars($k . " - " . $v); ?></option>
<? } ?>	
</select></td>
		<td width="50"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_y" value="<? echo str_replace(".",",",$row['posy']); ?>" style="width:50px;"></td>
		<td width="50"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_groesse" value="<? echo str_replace(".",",",$row['schriftgroesse']); ?>" style="width:50px;"></td>
		<td width="50"  bgcolor="<?= $bgColor; ?>"><select name="text_schriftart">
		<option value="">-- Schriftart --</option>
<? echo str_replace("value=\"" . strtoupper($row['schrift']). "\">", "value=\"" . strtoupper($row['schrift']). "\" selected>", $html_schrift_auswahlbox); ?>
		</select></td>
		<td width="140"  bgcolor="<?= $bgColor; ?>">C: <input type="text" name="text_fc" value="<? echo $farbe[0]; ?>" style="width:40px;">
		M: <input type="text" name="text_fm" value="<? echo $farbe[1]; ?>" style="width:40px;"><br>
		Y: <input type="text" name="text_fy" value="<? echo $farbe[2]; ?>" style="width:40px;">
		K: <input type="text" name="text_fk" value="<? echo $farbe[3]; ?>" style="width:40px;"></td>
	  </tr>
	</form>
<? }

$bgColor = "#ffffff";
$bgColor = webhelper::getCmsBgColor($bgColor);
?>
	<tr>
	  	<td colspan="10" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Neuen Text einf&uuml;gen</strong></td>
	  </tr>
	  <form method="POST" action="<?= webhelper::encodeQs("a=vk&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>" name="form_saveNew">
	  <input type="hidden" name="updateText" value="new">
	  <tr>
	  <td width="340" bgcolor="<?= $bgColor; ?>" style="padding: 2px; border-left: 0;" bgcolor="<? echo $bgColor; ?>">
				<p class="main_button"><a style="width: auto;" title="Speichern" href="javascript:document.forms['form_saveNew'].submit();"><img src="images/icon_head_save.png"></a></p>
<!--	<p class="main_button"><a style="width: auto;" title="L&ouml;schen" href="javascript:loeschen('<?= $row['id']; ?>')"><img src="images/icon_head_del.png"></a></p>
			<p class="main_button"><a style="width: auto;" title="Linksb&uuml;ndig" href="<?= webhelper::encodeQs("a=struktur&b=addSub&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/bt_l_align.gif"></a></p>
				<p class="main_button"><a style="width: auto;" title="Zentriert" href="<?= webhelper::encodeQs("a=struktur&b=moveToTop&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/bt_m_align.gif"></a></p>
					<p class="main_button"><a style="width: auto;" title="Rechtsb&uuml;ndig" href="<?= webhelper::encodeQs("a=struktur&b=moveUp&id=" . $row['id'] . "&area=".$area); ?>"><img src="images/bt_r_align.gif"></a></p>
						<input type="checkbox" name="" value="" style="width:20px"><input type="checkbox" name="" value="" style="width:20px"><input type="checkbox" name="" value="" style="width:20px"><!--<input type="checkbox" name="" value="" style="width:20px">//-->	
			</td>
	  	<td width="200"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_name" value="" style="width:200px;"><br><input type="text" name="text_beschreibung" value="" style="width:200px;"></td>
		<td width="50"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_x" value="" style="width:50px;"></td>
		<td width="50"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_y" value="" style="width:50px;"></td>
		<td width="50"  bgcolor="<?= $bgColor; ?>"><input type="text" name="text_groesse" value="" style="width:50px;"></td>
		<td width="50"  bgcolor="<?= $bgColor; ?>"><select name="text_schriftart">
		<option value="">-- Schriftart --</option>
<?= $html_schrift_auswahlbox ?>
		</select></td>
		<td width="140"  bgcolor="<?= $bgColor; ?>">C: <input type="text" name="text_fc" value="" style="width:40px;">
		M: <input type="text" name="text_fm" value="" style="width:40px;"><br>
		Y: <input type="text" name="text_fy" value="" style="width:40px;">
		K: <input type="text" name="text_fk" value="" style="width:40px;"></td>
	  </tr>
	</form>
	</table>
	<p>&nbsp;</p>


	<form method="POST" ENCTYPE="multipart/form-data" action="<?= webhelper::encodeQs("a=vk&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>" name="formNewFont">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="450">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Neue Schriftart uploaden</strong></td>
	  </tr>
	  <tr>
	  	<td width="290"><input type="file" name="upload_schrift"></td>
	  	<td width="160"><p class="wrap_button"><a href="javascript:document.forms['formNewFont'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Schrift uploaden</b></a></p></td>
	  </tr>
	</table>
	</form>

	<p>&nbsp;</p>
	<form method="POST" ENCTYPE="multipart/form-data" action="<?= webhelper::encodeQs("a=vk&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>" name="formNewBgPdf">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="450">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Neue PDF-Datei als Hintergrund verwenden</strong></td>
	  </tr>
	  <tr>
	  	<td width="290"><input type="file" name="upload_bgpdf"></td>
	  	<td width="160"><p class="wrap_button"><a href="javascript:document.forms['formNewBgPdf'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Datei uploaden</b></a></p></td>
	  </tr>
	</table>
	</form>
	
	<p>&nbsp;</p>
	<form method="POST" ENCTYPE="multipart/form-data" action="<?= webhelper::encodeQs("a=vk&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>" name="formNewMasse">
	<input type="hidden" name="do" value="updateMasse">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="450">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Eigenschaften &auml;ndern</strong></td>
	  </tr>
	  <tr>
	  	<td width="160">Name:</td>
	  	<td width="290"><input type="text" name="name" value="<? echo webhelper::niceHtml($vlg->name); ?>" style="width:80px;"></td>
	  </tr>
	  <tr>
	  	<td width="160">Breite:</td>
	  	<td width="290"><input type="text" name="breite" value="<? echo $vlg->breite; ?>" style="width:80px;"> mm</td>
	  </tr>
	  <tr>
	  	<td width="160" bgcolor="#ebf2f9">H&ouml;he:</td>
	  	<td width="290" bgcolor="#ebf2f9"><input type="text" name="hoehe" value="<? echo $vlg->hoehe; ?>" style="width:80px;"> mm</td>
	  </tr>
	  <tr>
	  	<td colspan="2"><p class="wrap_button"><a href="javascript:document.forms['formNewMasse'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p></td>
	  </tr>
	</table>
	</form>
	
	<p>&nbsp;</p>
	<form method="POST" ENCTYPE="multipart/form-data" action="<?= webhelper::encodeQs("a=vk&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>" name="formRueckseite">
	<input type="hidden" name="do" value="updateRueckseite">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="450">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>QR-Code auf R&uuml;ckseite</strong></td>
	  </tr>
	  <tr>
	  	<td width="450" bgcolor="#ebf2f9" colspan="2"><input type="checkbox" name="qr_active" value="true" <?php if ($vlg->qr_active=="1") echo " checked "; ?>style="width:20px" id="qrActive"><label for="qrActive"> QR-Code aktiv</label></td>
	  </tr>
	  <tr>
	  	<td width="450" bgcolor="#ebf2f9" colspan="2"><input type="checkbox" name="qr_back" value="true" <?php if ($vlg->qr_back=="1") echo " checked "; ?>style="width:20px" id="qrBack"><label for="qrBack"> QR-Code auf R&uuml;ckseite anzeigen</label></td>
	  </tr>
	  <tr>
	  	<td width="160">Gr&ouml;&szlig;e:</td>
	  	<td width="290"><input type="text" name="qr_breite" value="<? echo $vlg->qr_breite; ?>" style="width:80px;"> mm</td>
	  </tr>
	  	  <tr>
	  	<td width="160" bgcolor="#ebf2f9">x-Pos:</td>
	  	<td width="290" bgcolor="#ebf2f9"><input type="text" name="qr_xpos" value="<? echo $vlg->qr_xpos; ?>" style="width:80px;"> mm</td>
	  </tr>
	  <tr>
	  	<td width="160">y-Pos:</td>
	  	<td width="290"><input type="text" name="qr_ypos" value="<? echo $vlg->qr_ypos; ?>" style="width:80px;"> mm</td>
	  </tr>
<?php 
$farbe = explode(";", $vlg->qr_farbe);
if (@$farbe[0] == "") $farbe[0] = 0;
if (@$farbe[1] == "") $farbe[1] = 0;
if (@$farbe[2] == "") $farbe[2] = 0;
?>
	  <tr>
	  	<td width="160" bgcolor="#ebf2f9">Farbe (RGB!):</td>
	  	<td width="290" bgcolor="#ebf2f9">R: <input type="text" name="qr_farbe_c" value="<? echo $farbe[0]; ?>" style="width:25px;"> G: <input type="text" name="qr_farbe_m" value="<? echo $farbe[1]; ?>" style="width:25px;"> B: <input type="text" name="qr_farbe_y" value="<? echo $farbe[2]; ?>" style="width:25px;"> </td>
	  </tr>
<?php 
$farbe = explode(";", $vlg->qr_bgcolor);
if (@$farbe[0] == "") $farbe[0] = 0;
if (@$farbe[1] == "") $farbe[1] = 0;
if (@$farbe[2] == "") $farbe[2] = 0;
if (@$farbe[3] == "") $farbe[3] = 0;
?>
	  <tr>
	  	<td width="160">Hintergrundfarbe:</td>
	  	<td width="290">C: <input type="text" name="qr_bgcolor_c" value="<? echo $farbe[0]; ?>" style="width:25px;"> M: <input type="text" name="qr_bgcolor_m" value="<? echo $farbe[1]; ?>" style="width:25px;"> Y: <input type="text" name="qr_bgcolor_y" value="<? echo $farbe[2]; ?>" style="width:25px;"> K: <input type="text" name="qr_bgcolor_k" value="<? echo $farbe[3]; ?>" style="width:25px;"> </td>
	  </tr>
	  </table>
	  <table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="450">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Aktive Felder</strong></td>
	  </tr>
<?php 

$qrFields = array(
	"firstname"=>"Vorname",
	"lastname"=>"Nachname",
	"title"=>"Anrede",
	"organization"=>"Firma",
	"role"=>"Position",
	"url"=>"Website",
	"email"=>"E-Mail",
	"email2"=>"Alternative E-Mail",
	"phone"=>"Telefon",
	"fax"=>"Telefax",
	"mobile"=>"Mobil",
	"street"=>"Stra�e und Haus-Nr.",
	"zip"=>"Mobil",
	"city"=>"Stadt",
	"state"=>"Bundesland",
	"country"=>"Land"
);

$i=0;
foreach($qrFields as $k=>$v)
{ $i++;
?>
	  <tr>
	  	<td width="450"<?php if ($i%2==0) echo ' bgcolor="#ebf2f9"'; ?>><input type="checkbox" name="qrActiveField[]" <?php if (strpos($vlg->qr_fields,$k)>0||$k=="lastname") echo " checked "; ?>value="<?php echo $k; ?>" style="width:20px" id="qrActiveField<?php echo $i; ?>"><label for="qrActiveField<?php echo $i; ?>"> <?php echo htmlentities($v); if ($k=="lastname") echo " (erforderlich)"; ?></label></td>
	  </tr>
<?php } ?>
	  
	  <tr>
	  	<td colspan="2"><p class="wrap_button"><a href="javascript:document.forms['formRueckseite'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p></td>
	  </tr>
<?php if ($vlg->qr_back=="1") { ?>
	  <tr>
	  	<td colspan="2"><p class="wrap_button"><a href="../showPdfQr.php?vid=<? echo $id; ?>&r=<? echo md5(microtime()); ?>" target="_blank"><img style="float:left; margin-right: 5px;" src="images/icon_tab_preview.png" /><b>PDF-Vorschau anzeigen</b></a></p></td>
	  </tr>
<?php } else { ?>
	  <tr>
	  	<td colspan="2"><p class="wrap_button"><a href="../showPdf.php?vid=<? echo $id; ?>&r=<? echo md5(microtime()); ?>" target="_blank"><img style="float:left; margin-right: 5px;" src="images/icon_tab_preview.png" /><b>PDF-Vorschau anzeigen</b></a></p></td>
	  </tr>
<?php } ?>
	</table>
	</form>
	
	<p>&nbsp;</p>
	<form method="POST" ENCTYPE="multipart/form-data" action="<?= webhelper::encodeQs("a=vk&b=edit&id=" . $_GET['id'] . "&kunde=".$kunde->id); ?>" name="formReadr">
	<input type="hidden" name="do" value="updateReAdre">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="450">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Rechnungsadresse &auml;ndern</strong></td>
	  </tr>
	  <tr>
	  	<td width="450"><textarea name="reAdresse" style="width:430px;height:70px;"><?= webhelper::niceHtml($vlg->reAdresse); ?></textarea></td>
	  </tr>
	  <tr>
	  	<td colspan="2"><p class="wrap_button"><a href="javascript:document.forms['formReadr'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p></td>
	  </tr>
	</table>
	</form>


	<div style="padding-top: 10px;">
		<!--<p class="wrap_button"><a title="Zur&uuml;ck zur &Uuml;bersicht" style="padding-left: 5px; padding-right: 4px;" href="<?= webhelper::encodeQs("a=vk&b=show&kunde=".$kunde->id); ?>"><img src="images/icon_head_back.png" /></a></p>
		<span style="width:4px; float:left;">&nbsp;</span>//-->
		<p class="wrap_button"><a href="../showPdf.php?vid=<? echo $id; ?>&r=<? echo md5(microtime()); ?>" target="_blank"><img style="float:left; margin-right: 5px;" src="images/icon_tab_preview.png" /><b>PDF-Vorschau anzeigen</b></a></p>
	<div>

</div>