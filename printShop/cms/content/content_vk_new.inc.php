<?
$kunde = new kunde($_GET['kunde']);

?>

<div id="content">
	<div id="content_tab_head">
		<div style="float:left; padding: 4px 3px 0px 10px;"><img src="images/icon_tree_page.png" /></div>
		<div style="float:left; padding: 4px 10px 0px 0px;" class="title">Neue Visitenkarte anlegen</div>
		<div style="clear:both;">
			<div id="main_buttons">
				<p class="main_button"><a href="javascript:document.forms['formData'].submit();" title="Speichern"><img src="images/icon_head_save.png" /></a></p>
			</div>
		</div>
	</div>
</div>

<div id="content_tab" class="content_tab">
<form method="POST" action="<? echo webhelper::encodeQs("a=vk&b=saveNew&kunde=".$kunde->id); ?>" name="formData">
	<table class="table" style="border: 1px solid #d4d5d4;" cellspacing="0" cellpadding="0" width="420">
	  <tr>
	  	<td colspan="2" style="border-bottom: 1px solid #d4d5d4; border-left: none; background-image: url('images/bg_navi.gif');" bgcolor="#f2f2f2"><strong>Datensatz bearbeiten</strong></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">Name:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="name" value="<? echo webhelper::niceInputOut($s->name); ?>"></td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ffffff" style="border:none;">Breite:</td>
		<td width="300"  bgcolor="#ffffff"><input type="text" name="breite" value="" style="width:80px;"> mm</td>
	  </tr>
	  <tr>
	  	<td width="120"  bgcolor="#ebf2f9" style="border:none;">H&ouml;he:</td>
		<td width="300"  bgcolor="#ebf2f9"><input type="text" name="hoehe" value="" style="width:80px;"> mm</td>
	  </tr>
	  <tr>
	  	<td width="120" bgcolor="#ffffff" style="border:none;">Vorlage:</td>
		<td width="300" bgcolor="#ffffff"><select name="vorlage">
		<option value="-1">-- keine Vorlage verwenden --</option>
		<?
$res = new dbquery("SELECT * FROM " . TBL_VORLAGEN . " WHERE kunde = " . $kunde->id);
while($row=$res->getNextRow())
{
?>
		<option value="<?= webhelper::niceHtml($row['id']); ?>"><?= webhelper::niceHtml($row['name']); ?></option>
<? } ?></select></td>
	  </tr>
	</table>
	<div style="padding-top: 10px;">
		<p class="wrap_button"><a title="Zur&uuml;ck zur &Uuml;bersicht" style="padding-left: 5px; padding-right: 4px;" href="<?= webhelper::encodeQs("a=vk&b=show&kunde=".$kunde->id); ?>"><img src="images/icon_head_back.png" /></a></p>
		<span style="width:4px; float:left;">&nbsp;</span>
		<p class="wrap_button"><a href="javascript:document.forms['formData'].submit();"><img style="float:left; margin-right: 5px;" src="images/icon_head_save.png" /><b>Speichern</b></a></p>
	<div>
</form>
</div>