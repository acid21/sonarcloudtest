<?
#error_reporting(E_ERROR | E_PARSE);ini_set("display_errors",0);error_reporting(0);
#error_reporting(E_ALL);ini_set("display_errors",1);
$textXpos = array();


function rgb2hex2rgb($c){
	if(!$c) return false;
	$c = trim($c);
	$out = false;
	if(preg_match("/^[0-9ABCDEFabcdef\#]+$/i", $c)){
		$c = str_replace('#','', $c);
		$l = strlen($c) == 3 ? 1 : (strlen($c) == 6 ? 2 : false);

		if($l){
			unset($out);
			$out[0] = $out['r'] = $out['red'] = hexdec(substr($c, 0,1*$l));
			$out[1] = $out['g'] = $out['green'] = hexdec(substr($c, 1*$l,1*$l));
			$out[2] = $out['b'] = $out['blue'] = hexdec(substr($c, 2*$l,1*$l));
		}else $out = false;

	}elseif (preg_match("/^[0-9]+(,| |.)+[0-9]+(,| |.)+[0-9]+$/i", $c)){
		$spr = str_replace(array(',',' ','.'), ':', $c);
		$e = explode(":", $spr);
		if(count($e) != 3) return false;
		$out = '#';
		for($i = 0; $i<3; $i++)
		$e[$i] = dechex(($e[$i] <= 0)?0:(($e[$i] >= 255)?255:$e[$i]));

		for($i = 0; $i<3; $i++)
		$out .= ((strlen($e[$i]) < 2)?'0':'').$e[$i];

		$out = strtoupper($out);
	}else $out = false;

	return $out;
}


function make_pdf($id)
{
	global $pdfdoc,$textXpos,$font,$pdf_h,$pdf_b,$dpi;
	$fontdir = FONTDIR;
	$dpi = DPI;

	$textXpos = array();

	$sql = "SELECT * FROM " . TBL_VORLAGEN . " WHERE id = $id";

	$res = new dbquery($sql);
	$daten = $res->getNextRow();


	#$f = fopen("test.pdf", "w");

    $pdfdoc = pdf_new();
    pdf_open_file($pdfdoc);
    $sql = "SELECT DISTINCT schrift FROM " . TBL_TEXTDATA . " WHERE vorlagen_id = $id";
	$res = new dbquery($sql);
	while ($row = $res->getNextRow())
	{
		$schriftFile = $row['schrift'];

		if (strtolower(substr($schriftFile,-3))!="otf") $schriftFile.=".TTF";

		@PDF_set_parameter($pdfdoc,"FontOutline","SCHRIFT" . $row['schrift'] . "==" . $fontdir . $schriftFile . "");
		$font[$row['schrift']] = PDF_findfont($pdfdoc,"SCHRIFT" . $row['schrift'],"winansi",1);
	}


    $pdf_b = $daten['breite'];
    $pdf_h = $daten['hoehe'];



    pdf_begin_page($pdfdoc, $pdf_b/$dpi, $pdf_h/$dpi);

#pdf_translate($pdfdoc, 3/$dpi, 3/$dpi);

    $farbe = explode(";", $daten['bgcolor']);
    $c = $farbe[0]/100;
    $m = $farbe[1]/100;
    $y = $farbe[2]/100;
    $k = $farbe[3]/100;

    pdf_setcolor($pdfdoc, "both", "cmyk", $c, $m, $y, $k);

#pdf_rect($pdfdoc, 0, 0, $pdf_b/$dpi, $pdf_h/$dpi);
#pdf_fill($pdfdoc);

	$sql = "SELECT * FROM " . TBL_TEXTDATA . " WHERE posx_abhlb = 0 AND vorlagen_id = $id ORDER BY id";
	$texte = new dbquery($sql);
	printTexte($texte);
	#print_r($textXpos);
	$sql = "SELECT * FROM " . TBL_TEXTDATA . " WHERE posx_abhlb <> 0 AND vorlagen_id = $id ORDER BY id";
	$texte = new dbquery($sql);
	printTexte($texte,true);



	$vlg = new vorlage($id);

	if ($vlg->qr_active==1&&$vlg->qr_back==0)
	{
		$dpi=DPI;
		$qrFields = array(
		"firstname"=>"Vorname",
						"lastname"=>"Nachname",
						"title"=>"Anrede",
						"organization"=>"Firma",
						"role"=>"Position",
						"url"=>"Website",
						"email"=>"E-Mail",
						"email2"=>"Alternative E-Mail",
						"phone"=>"Telefon",
						"fax"=>"Telefax",
						"mobile"=>"Mobil",
						"street"=>"Stra�e und Haus-Nr.",
						"zip"=>"Mobil",
						"city"=>"Stadt",
						"state"=>"Bundesland",
						"country"=>"Land"
				);
				foreach($qrFields as $k=>$v)
		{
		if (!(strpos($vlg->qr_fields,$k)>0))
		$_SESSION['sess_fe_qrfield' . $k] = "";
					else
		$_SESSION['sess_fe_qrfield' . $k] = $_POST['qrField_' . $k];
				}

				require_once("qr/lib/qr_pdf.php");
				$contact = array();
		foreach($qrFields as $k=>$v)
		{
		$contact[$k]=$v;
		}

		$farbe = explode(";", $vlg->qr_farbe);
		if (@$farbe[0] == "") $farbe[0] = 0;
		if (@$farbe[1] == "") $farbe[1] = 0;
		if (@$farbe[2] == "") $farbe[2] = 0;
		$fgcolor = rgb2hex2rgb($farbe[0] . "," . $farbe[1] . "," . $farbe[2]);
		#echo $fgcolor;

				$qr_placed = placeQrPDF($pdfdoc, array(
		"contact" => $contact,
		"qr" => array(
					            "left" => $vlg->qr_xpos, // in mm
		"top" => $vlg->qr_ypos, // in mm
		"size" => $vlg->qr_breite, // in mm
					            "filename" => dirname(__FILE__)."/temp/qr/qr2.png", // absolute path
		"fg_color" => $fgcolor, // black
		//"bg_color" => "#FFF", // white
		"bg_color" => "-1" // transparent
				)
		)
		);
	}



    pdf_end_page($pdfdoc);
    pdf_close($pdfdoc);

    $data = pdf_get_buffer($pdfdoc);

    return $data;
}

function printTexte($texte,$abh=false)
{
	global $pdfdoc,$textXpos,$font,$pdf_h,$pdf_b,$dpi;
    while($row = $texte->getNextRow())
    {
        $farbe = explode(";", $row['farbe']);
    	$c = $farbe[0]/100;
    	$m = $farbe[1]/100;
		$y = $farbe[2]/100;
		$k = $farbe[3]/100;

		pdf_setcolor($pdfdoc, "both", "cmyk", $c, $m, $y, $k);

		@pdf_setfont($pdfdoc, $font[$row['schrift']], $row['schriftgroesse']);

		#$posx = $row['posx'];
	    #$posy = $pdf_h - $row['posy'];

	    #$posy = $row['posy'];

#if ($row['align'] == 'left')
#{
    	if ($abh)
    	{
    		$posx = $textXpos[$row['posx_abhlb']];
    		$row['align'] = "left";
    	} else $posx = $row['posx'];
    	$posy = $pdf_h - $row['posy'];
    	$endx = $pdf_b - $posx;
    	$endy = $pdf_h - $row['posy'];
#}
#pdf_show_boxed($pdfdoc, $row['name'], $posx, $posy, $endx, $endy, $row['align']);

#if (isset($_GET['textfeld' . $row['uid']])) { $t = $_GET['textfeld' . $row['uid']]; } else { $t = $row['name']; }
        if (isset($_GET['textfeld' . $row['uid']])) { $t = stripslashes($_GET['textfeld' . $row['uid']]); } else { $t = $row['name']; }
        if (isset($_POST['textfeld' . $row['uid']])) { $t = stripslashes($_POST['textfeld' . $row['uid']]); } else { $t = $row['name']; }

		@pdf_setfont($pdfdoc, $font[$row['schrift']], $row['schriftgroesse']);

		$groesse = $row['schriftgroesse'];

		if ($row['align'] == "left")
		{
		 	while (pdf_show_boxed($pdfdoc, $t, 0, 0, ($pdf_b-$posx)/$dpi, $groesse/2/$dpi, "left", "blind") > 0)
		 	{
		 		$groesse = $groesse - 0.5;
		 		//$starty = $starty + 0.1;
				 @pdf_setfont($pdfdoc, $font[$row['schrift']], $groesse);
		 	}
    	}
	 	if ($row['align'] == "center")
	 	{
		 	while (pdf_show_boxed($pdfdoc, $t, 0, 0, ($pdf_b)/$dpi, $groesse/2/$dpi, "left", "blind") > 0)
			{
				$groesse = $groesse - 0.5;
			 	//$starty = $starty + 0.1;
				@pdf_setfont($pdfdoc, $font[$row['schrift']], $groesse);
		 	}
	 	}
		pdf_show_boxed($pdfdoc, $t, $posx/$dpi, $posy/$dpi, NULL, NULL, $row['align']);

		if (!$abh)
		{
			$myW = pdf_stringwidth($pdfdoc,$t,$font[$row['schrift']],$groesse)*$dpi;
			if ($row['align'] == "left") $myX=$posx;
			if ($row['align'] == "right") $myX=$row['posx']-$myW;
			if ($row['align'] == "center") $myX=$row['posx']-($myW/2);

			$textXpos[$row['id']] = $myX;
		}



#echo $row['name'];
    }
}


include("lib/lib.inc.php");

$vid=$_GET['vid'];
if (empty($vid)) $vid=109;
$data = make_pdf($vid);

$dateiname = "temp/pdf_" . md5(microtime()) . ".pdf";

$f = fopen($dateiname,"w");
fwrite($f, $data);
fclose($f);

$v = new vorlage($vid);
$fileBg = false;
if (!empty($v->bgPdf)&&file_exists("bg/".$v->bgPdf))
{
	$fileBg = $v->bgPdf;
}

if ($fileBg) $data = file_get_contents("http://files.acid21.com/staats-pdf-merge/index.php?a=http://www2.staats.de/printShop/bg/" . $fileBg . "&b=http://www2.staats.de/printShop/".$dateiname);



Header("Content-type: application/pdf");
header("Content-disposition: inline; filename=dummy.pdf");
header("Content-length: " . strlen($data));

echo $data;

?>

