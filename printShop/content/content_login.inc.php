<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html;" />
<link rel="stylesheet" type="text/css" href="cms/style.css" />
<title>Anmeldung</title>
<? includeJs("/"); ?>
<script type="text/javascript">
<!--
function init() {
	if ($('inptBenutzername')) $('inptBenutzername').focus();
}
//-->
</script>
</head>

<body style="background-color: #fff;" onload="init();">
<form method="POST" action="./">
<input type="hidden" name="do" value="login">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <tr>
    <td width="100%" align="center">


<table style="border: 1px solid #cbcccb;" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td style="padding: 6px 6px 0px 6px; font-size: 11px;">&nbsp;</td>
		<td align="right" style="padding: 7px 13px 20px 0px;"><p><img src="cms/images/logo.gif" /></p></td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 6px 13px 6px 13px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td style="padding: 9px 20px 18px 19px; border: 1px solid #e4e3e4;" bgcolor="#f2f2f2">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="padding-bottom: 5px; margin-left: -1px;" colspan="2">
									<p style="float: left; padding-right: 9px; _padding-right: 5px;"><img src="cms/images/icon_login.gif" /></p>
									<p style="color: #88151a; font-weight: bold; font-size: 11px; padding-top: 4px;">Visitenkartenbestellung</p>
								</td>
							</tr>
<? if ($errorMsg=="noRights") { ?>
	
	<tr>
								<td>&nbsp;</td>
								<td height="25" valign="top"><p style="color: #88151a; font-size: 11px; padding-top: 3px;">Sie haben keine Berechtigung, dieses Seite anzuzeigen!<br><a href="javascript:history.back();">zur&uuml;ck</a></p></td>
							</tr>
<? } else { ?>
<? if (!empty($errorMsg)) {  ?><tr>
								<td height="25" valign="top" colspan="2"><p style="color: #88151a; font-size: 11px; padding-top: 3px;"><?= $errorMsg ?></p></td>
							</tr><? } ?>
							<tr>
								<td><p style="font-size: 11px; margin: 0px 17px 6px 0px;">Benutzername:</p></td>
								<td><input style="width: 203px; height: 22px; margin-bottom: 6px; padding: 3px 3px 3px 4px; background-color: #fff; border: 1px solid #cbcccb;" id="inptBenutzername" type="text" name="un" value="" /></td>
							</tr>
							<tr>
								<td><p style="font-size: 11px; margin-bottom: 6px; ">Passwort:</p></td>
								<td><input style="width: 203px; height: 22px; margin-bottom: 6px; padding: 3px 3px 3px 4px; background-color: #fff; border: 1px solid #cbcccb;" type="password" name="pw" value="" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><p><input style="width: 101px; height: 22px; color: #fff; font-weight: bold; border: none;background-color: #4b79ac; background-image: url('cms/images/bg_button_login.gif');" type="submit" name="login" value="Anmelden" /></p></td>
							</tr>
<? } ?>
						</table>	
					</td>
				<tr>
					<td><p style="color: #a7a5a5; font-size: 11px; padding-top: 6px;">powered by <a style="color: #3974b1;" href="http://www.acid21.com" target="_blank">acid.21</a></p></td>
				</tr>	
				</tr>
			</table>
		</td>
 	</tr>
</table>

</td>
  </tr>
</table>
</form>
</body>

</html>
