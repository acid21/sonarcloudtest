<?

function rgb2hex2rgb($c){
	if(!$c) return false;
	$c = trim($c);
	$out = false;
	if(preg_match("/^[0-9ABCDEFabcdef\#]+$/i", $c)){
		$c = str_replace('#','', $c);
		$l = strlen($c) == 3 ? 1 : (strlen($c) == 6 ? 2 : false);

		if($l){
			unset($out);
			$out[0] = $out['r'] = $out['red'] = hexdec(substr($c, 0,1*$l));
			$out[1] = $out['g'] = $out['green'] = hexdec(substr($c, 1*$l,1*$l));
			$out[2] = $out['b'] = $out['blue'] = hexdec(substr($c, 2*$l,1*$l));
		}else $out = false;

	}elseif (preg_match("/^[0-9]+(,| |.)+[0-9]+(,| |.)+[0-9]+$/i", $c)){
		$spr = str_replace(array(',',' ','.'), ':', $c);
		$e = explode(":", $spr);
		if(count($e) != 3) return false;
		$out = '#';
		for($i = 0; $i<3; $i++)
		$e[$i] = dechex(($e[$i] <= 0)?0:(($e[$i] >= 255)?255:$e[$i]));

		for($i = 0; $i<3; $i++)
		$out .= ((strlen($e[$i]) < 2)?'0':'').$e[$i];

		$out = strtoupper($out);
	}else $out = false;

	return $out;
}
function make_pdfolf($id)
{

	$fontdir = FONTDIR;
	$dpi = DPI;

	$sql = "SELECT * FROM " . TBL_VORLAGEN . " WHERE id = $id";

	$res = new dbquery($sql);

	$daten = $res->getNextRow();


	$sql = "SELECT * FROM " . TBL_TEXTDATA . " WHERE vorlagen_id = $id ORDER BY id";

	$texte = new dbquery($sql);



    //$f = fopen("test.pdf", "w");

    $pdfdoc = pdf_new();

    pdf_open_file($pdfdoc);

    $sql = "SELECT DISTINCT schrift FROM " . TBL_TEXTDATA . " WHERE vorlagen_id = $id";

		$res = new dbquery($sql);

		while ($row = $res->getNextRow())
		{
			$schriftFile = $row['schrift'];

			if (strtolower(substr($schriftFile,-3))!="otf") $schriftFile.=".TTF";

			@PDF_set_parameter($pdfdoc,"FontOutline","SCHRIFT" . $row['schrift'] . "==" . $fontdir . $schriftFile . "");

			$font[$row['schrift']] = PDF_findfont($pdfdoc,"SCHRIFT" . $row['schrift'],"winansi",1);
		}


    $pdf_b = $daten['breite'];

    $pdf_h = $daten['hoehe'];



    pdf_begin_page($pdfdoc, $pdf_b/$dpi, $pdf_h/$dpi);

//    pdf_translate($pdfdoc, 3/$dpi, 3/$dpi);


    $farbe = explode(";", $daten['bgcolor']);

    $c = $farbe[0]/100;

    $m = $farbe[1]/100;

    $y = $farbe[2]/100;

    $k = $farbe[3]/100;

    pdf_setcolor($pdfdoc, "both", "cmyk", $c, $m, $y, $k);

 /*   pdf_rect($pdfdoc, 0, 0, $pdf_b/$dpi, $pdf_h/$dpi);

    pdf_fill($pdfdoc); */


    while($row = $texte->getNextRow())
    {
        $farbe = explode(";", $row['farbe']);

    $c = $farbe[0]/100;

    $m = $farbe[1]/100;

    $y = $farbe[2]/100;

    $k = $farbe[3]/100;

    pdf_setcolor($pdfdoc, "both", "cmyk", $c, $m, $y, $k);

	@pdf_setfont($pdfdoc, $font[$row['schrift']], $row['schriftgroesse']);

    $posx = $row['posx'];

    $posy = $pdf_h - $row['posy'];

    $posy = $row['posy'];

    //    if ($row['align'] == 'left')

    //    {

        	$posx = $row['posx'];

        	$posy = $pdf_h - $row['posy'];

        	$endx = $pdf_b - $row['posx'];

        	$endy = $pdf_h - $row['posy'];
    //    }

        //pdf_show_boxed($pdfdoc, $row['name'], $posx, $posy, $endx, $endy, $row['align']);

//        if (isset($_GET['textfeld' . $row['uid']])) { $t = $_GET['textfeld' . $row['uid']]; } else { $t = $row['name']; }
        if (isset($_GET['textfeld' . $row['id']])) { $t = stripslashes($_GET['textfeld' . $row['id']]); } else { $t = $row['name']; }
        if (isset($_POST['textfeld' . $row['id']])) {
        	$t = stripslashes($_POST['textfeld' . $row['id']]);
        	$_SESSION['sess_fe_textfeld' . $row['id']] = $_POST['textfeld' . $row['id']];

        	if (isset($_POST['textfeld' . $row['id'] . '_zusatz']))
			{
				$t = $t . stripslashes($_POST['textfeld' . $row['id']. '_zusatz']);
				$_SESSION['sess_fe_textfeld' . $row['id'] . '_zusatz'] = $_POST['textfeld' . $row['id']. '_zusatz'];
			}
			if (isset($_POST['textfeld' . $row['id'] . '_zusatz_l']))
			{
				$t = stripslashes($_POST['textfeld' . $row['id']. '_zusatz_l']) . $t;
				$_SESSION['sess_fe_textfeld' . $row['id'] . '_zusatz_l'] = $_POST['textfeld' . $row['id']. '_zusatz_l'];
			}
        } else { $t = $row['name']; }

		@pdf_setfont($pdfdoc, $font[$row['schrift']], $row['schriftgroesse']);

		$groesse = $row['schriftgroesse'];

	 if ($row['align'] == "left")
	 while (pdf_show_boxed($pdfdoc, $t, 0, 0, ($pdf_b-$posx)/$dpi, $groesse/2/$dpi, "left", "blind") > 0)
	 {
	 $groesse = $groesse - 0.5;
	 //$starty = $starty + 0.1;
	 @pdf_setfont($pdfdoc, $font[$row['schrift']], $groesse);
	 }
	 if ($row['align'] == "center")
	 while (pdf_show_boxed($pdfdoc, $t, 0, 0, ($pdf_b)/$dpi, $groesse/2/$dpi, "left", "blind") > 0)
	 {
		 $groesse = $groesse - 0.5;
		 //$starty = $starty + 0.1;
		 @pdf_setfont($pdfdoc, $font[$row['schrift']], $groesse);
	 }

       pdf_show_boxed($pdfdoc, $t, $posx/$dpi, $posy/$dpi, NULL, NULL, $row['align']);
        //echo $row['name'];
    }

    pdf_end_page($pdfdoc);
    pdf_close($pdfdoc);

    $data = pdf_get_buffer($pdfdoc);

    return $data;
}



$textXpos = array();
function make_pdf($id)
{
	global $pdfdoc,$textXpos,$font,$pdf_h,$pdf_b,$dpi;
	$fontdir = FONTDIR;
	$dpi = DPI;

	$textXpos = array();

	$sql = "SELECT * FROM " . TBL_VORLAGEN . " WHERE id = $id";

	$res = new dbquery($sql);
	$daten = $res->getNextRow();


	#$f = fopen("test.pdf", "w");

    $pdfdoc = pdf_new();
    pdf_open_file($pdfdoc);
    $sql = "SELECT DISTINCT schrift FROM " . TBL_TEXTDATA . " WHERE vorlagen_id = $id";
	$res = new dbquery($sql);
	while ($row = $res->getNextRow())
	{
		$schriftFile = $row['schrift'];

		if (strtolower(substr($schriftFile,-3))!="otf") $schriftFile.=".TTF";

		@PDF_set_parameter($pdfdoc,"FontOutline","SCHRIFT" . $row['schrift'] . "==" . $fontdir . $schriftFile . "");
		$font[$row['schrift']] = PDF_findfont($pdfdoc,"SCHRIFT" . $row['schrift'],"winansi",1);
	}


    $pdf_b = $daten['breite'];
    $pdf_h = $daten['hoehe'];



    pdf_begin_page($pdfdoc, $pdf_b/$dpi, $pdf_h/$dpi);

#pdf_translate($pdfdoc, 3/$dpi, 3/$dpi);

    $farbe = explode(";", $daten['bgcolor']);
    $c = $farbe[0]/100;
    $m = $farbe[1]/100;
    $y = $farbe[2]/100;
    $k = $farbe[3]/100;

    pdf_setcolor($pdfdoc, "both", "cmyk", $c, $m, $y, $k);

#pdf_rect($pdfdoc, 0, 0, $pdf_b/$dpi, $pdf_h/$dpi);
#pdf_fill($pdfdoc);

	$sql = "SELECT * FROM " . TBL_TEXTDATA . " WHERE posx_abhlb = 0 AND vorlagen_id = $id ORDER BY id";
	$texte = new dbquery($sql);
	printTexte($texte);
	#print_r($textXpos);
	$sql = "SELECT * FROM " . TBL_TEXTDATA . " WHERE posx_abhlb <> 0 AND vorlagen_id = $id ORDER BY id";
	$texte = new dbquery($sql);
	printTexte($texte,true);

	$vlg = new vorlage($id);

	if ($vlg->qr_active==1&&$vlg->qr_back==0)
	{
				$dpi=DPI;
				$qrFields = array(
				"firstname"=>"Vorname",
				"lastname"=>"Nachname",
				"title"=>"Anrede",
				"organization"=>"Firma",
				"role"=>"Position",
				"url"=>"Website",
				"email"=>"E-Mail",
				"email2"=>"Alternative E-Mail",
				"phone"=>"Telefon",
				"fax"=>"Telefax",
				"mobile"=>"Mobil",
				"street"=>"Stra�e und Haus-Nr.",
				"zip"=>"Mobil",
				"city"=>"Stadt",
				"state"=>"Bundesland",
				"country"=>"Land"
			);
			foreach($qrFields as $k=>$v)
			{
				if (!(strpos($vlg->qr_fields,$k)>0))
				$_SESSION['sess_fe_qrfield' . $k] = "";
				else
				$_SESSION['sess_fe_qrfield' . $k] = $_POST['qrField_' . $k];
			}

			require_once("qr/lib/qr_pdf.php");

			$contact = array();
			foreach($qrFields as $k=>$v)
			{
				$contact[$k]=$_SESSION['sess_fe_qrfield' . $k];
			}

			$farbe = explode(";", $vlg->qr_farbe);
			if (@$farbe[0] == "") $farbe[0] = 0;
			if (@$farbe[1] == "") $farbe[1] = 0;
			if (@$farbe[2] == "") $farbe[2] = 0;
			$fgcolor = rgb2hex2rgb($farbe[0] . "," . $farbe[1] . "," . $farbe[2]);
			#echo $fgcolor;

			$qr_placed = placeQrPDF($pdfdoc, array(
			        "contact" => $contact,
			        "qr" => array(
			            "left" => $vlg->qr_xpos, // in mm
			            "top" => $vlg->qr_ypos, // in mm
			            "size" => $vlg->qr_breite, // in mm
			            "filename" => dirname(__FILE__)."/../temp/qr/qr2.png", // absolute path
			            "fg_color" => $fgcolor, // black
			//"bg_color" => "#FFF", // white
			            "bg_color" => "-1" // transparent
			)
			)
			);

			if(!$qr_placed)
			die("something went wrong, QR-code was not placed");
	}

    pdf_end_page($pdfdoc);
    pdf_close($pdfdoc);

    $data = pdf_get_buffer($pdfdoc);

    return $data;
}

function printTexte($texte,$abh=false)
{
	global $pdfdoc,$textXpos,$font,$pdf_h,$pdf_b,$dpi;
    while($row = $texte->getNextRow())
    {
        $farbe = explode(";", $row['farbe']);
    	$c = $farbe[0]/100;
    	$m = $farbe[1]/100;
		$y = $farbe[2]/100;
		$k = $farbe[3]/100;

		pdf_setcolor($pdfdoc, "both", "cmyk", $c, $m, $y, $k);

		@pdf_setfont($pdfdoc, $font[$row['schrift']], $row['schriftgroesse']);

		#$posx = $row['posx'];
	    #$posy = $pdf_h - $row['posy'];

	    #$posy = $row['posy'];

#if ($row['align'] == 'left')
#{
    	if ($abh)
    	{
    		$posx = $textXpos[$row['posx_abhlb']];
    		$row['align'] = "left";
    	} else $posx = $row['posx'];
    	$posy = $pdf_h - $row['posy'];
    	$endx = $pdf_b - $posx;
    	$endy = $pdf_h - $row['posy'];
#}
#pdf_show_boxed($pdfdoc, $row['name'], $posx, $posy, $endx, $endy, $row['align']);

#if (isset($_GET['textfeld' . $row['uid']])) { $t = $_GET['textfeld' . $row['uid']]; } else { $t = $row['name']; }
        if (isset($_GET['textfeld' . $row['id']])) { $t = stripslashes($_GET['textfeld' . $row['id']]); } else { $t = $row['name']; }
        if (isset($_POST['textfeld' . $row['id']])) {
        	$t = stripslashes($_POST['textfeld' . $row['id']]);
        	$_SESSION['sess_fe_textfeld' . $row['id']] = $_POST['textfeld' . $row['id']];

        	if (isset($_POST['textfeld' . $row['id'] . '_zusatz']))
			{
				$t = $t . stripslashes($_POST['textfeld' . $row['id']. '_zusatz']);
				$_SESSION['sess_fe_textfeld' . $row['id'] . '_zusatz'] = $_POST['textfeld' . $row['id']. '_zusatz'];
			}
			if (isset($_POST['textfeld' . $row['id'] . '_zusatz_l']))
			{
				$t = stripslashes($_POST['textfeld' . $row['id']. '_zusatz_l']) . $t;
				$_SESSION['sess_fe_textfeld' . $row['id'] . '_zusatz_l'] = $_POST['textfeld' . $row['id']. '_zusatz_l'];
			}
        } else { $t = $row['name']; }
		@pdf_setfont($pdfdoc, $font[$row['schrift']], $row['schriftgroesse']);

		$groesse = $row['schriftgroesse'];

		if ($row['align'] == "left")
		{
		 	while (pdf_show_boxed($pdfdoc, $t, 0, 0, ($pdf_b-$posx)/$dpi, $groesse/2/$dpi, "left", "blind") > 0)
		 	{
		 		$groesse = $groesse - 0.5;
		 		//$starty = $starty + 0.1;
				 @pdf_setfont($pdfdoc, $font[$row['schrift']], $groesse);
		 	}
    	}
	 	if ($row['align'] == "center")
	 	{
		 	while (pdf_show_boxed($pdfdoc, $t, 0, 0, ($pdf_b)/$dpi, $groesse/2/$dpi, "left", "blind") > 0)
			{
				$groesse = $groesse - 0.5;
			 	//$starty = $starty + 0.1;
				@pdf_setfont($pdfdoc, $font[$row['schrift']], $groesse);
		 	}
	 	}
		pdf_show_boxed($pdfdoc, $t, $posx/$dpi, $posy/$dpi, NULL, NULL, $row['align']);

		if (!$abh)
		{
			$myW = pdf_stringwidth($pdfdoc,$t,$font[$row['schrift']],$groesse)*$dpi;
			if ($row['align'] == "left") $myX=$posx;
			if ($row['align'] == "right") $myX=$row['posx']-$myW;
			if ($row['align'] == "center") $myX=$row['posx']-($myW/2);

			$textXpos[$row['id']] = $myX;
		}

#echo $row['name'];
    }
}

$data = make_pdf($_SESSION['sess_fe_vId']);

$dateiname = "temp/pdf_" . md5(microtime()) . ".pdf";

$f = fopen($dateiname,"w");
fwrite($f, $data);
fclose($f);

$v = new vorlage($_SESSION['sess_fe_vId']);
$fileBg = false;
if (!empty($v->bgPdf)&&file_exists("bg/".$v->bgPdf))
{
	$fileBg = $v->bgPdf;
}

if ($fileBg) $data = file_get_contents("http://62.75.157.144/index.php?a=http://www.staats.de/printShop/bg/" . $fileBg . "&b=http://www.staats.de/printShop/".$dateiname);

$dateiname = "temp/pdf_" . md5(microtime()) . ".pdf";

$f = fopen($dateiname,"w");
fwrite($f, $data);
fclose($f);




function generateQr($vlg)
{
	global $textXpos,$font,$pdf_h,$pdf_b,$dpi;
	$qrFields = array(
		"firstname"=>"Vorname",
		"lastname"=>"Nachname",
		"title"=>"Anrede",
		"organization"=>"Firma",
		"role"=>"Position",
		"url"=>"Website",
		"email"=>"E-Mail",
		"email2"=>"Alternative E-Mail",
		"phone"=>"Telefon",
		"fax"=>"Telefax",
		"mobile"=>"Mobil",
		"street"=>"Stra�e und Haus-Nr.",
		"zip"=>"Mobil",
		"city"=>"Stadt",
		"state"=>"Bundesland",
		"country"=>"Land"
	);
	foreach($qrFields as $k=>$v)
	{
		if (!(strpos($vlg->qr_fields,$k)>0))
		$_SESSION['sess_fe_qrfield' . $k] = "";
		else
		$_SESSION['sess_fe_qrfield' . $k] = $_POST['qrField_' . $k];
	}

	require_once("qr/lib/qr_pdf.php");
	$pdf = pdf_new();
	pdf_open_file($pdf);



	// Creates a 50mm x 40mm PDF page
	pdf_begin_page($pdf, $pdf_b/$dpi, $pdf_h/$dpi);

	$farbe = explode(";", $vlg->qr_bgcolor);
	if (@$farbe[0] == "") $farbe[0] = 0;
	if (@$farbe[1] == "") $farbe[1] = 0;
	if (@$farbe[2] == "") $farbe[2] = 0;
	if (@$farbe[3] == "") $farbe[3] = 0;
	pdf_setcolor($pdf, "both", "cmyk", $farbe[0]/100,$farbe[1]/100,$farbe[2]/100,$farbe[3]/100);

	PDF_rect($pdf,0,0,$pdf_b/$dpi, $pdf_h/$dpi);
	PDF_fill($pdf);


	$contact = array();
	foreach($qrFields as $k=>$v)
	{
		$contact[$k]=$_SESSION['sess_fe_qrfield' . $k];
	}

	$farbe = explode(";", $vlg->qr_farbe);
	if (@$farbe[0] == "") $farbe[0] = 0;
	if (@$farbe[1] == "") $farbe[1] = 0;
	if (@$farbe[2] == "") $farbe[2] = 0;
	$fgcolor = rgb2hex2rgb($farbe[0] . "," . $farbe[1] . "," . $farbe[2]);
	#echo $fgcolor;

	$qr_placed = placeQrPDF($pdf, array(
	        "contact" => $contact,
	        "qr" => array(
	            "left" => $vlg->qr_xpos, // in mm
	            "top" => $vlg->qr_ypos, // in mm
	            "size" => $vlg->qr_breite, // in mm
	            "filename" => dirname(__FILE__)."/../temp/qr/qr2.png", // absolute path
	            "fg_color" => $fgcolor, // black
	//"bg_color" => "#FFF", // white
	            "bg_color" => "-1" // transparent
	)
	)
	);

	if(!$qr_placed)
	die("something went wrong, QR-code was not placed");

	pdf_end_page($pdf);
	pdf_close($pdf);

	$pdf_bin_data = pdf_get_buffer($pdf);

	return $pdf_bin_data;
}

# R�ckseite generieren
if ($v->qr_active=="1"&&$v->qr_back==1)
{
	$dateinameQr = "temp/pdf_" . md5(microtime()) . "_qr.pdf";
	$data = generateQr($v);
	$f = fopen($dateinameQr,"w");
	fwrite($f, $data);
	fclose($f);
}
?>

<script language="JavaScript">
<!--

function checkAgb()
{

	if (document.forms['formOrder'].agb_accept.checked == false)
	{
		alert("Sie m�ssen die AGB akzeptieren, um die Bestellung abschicken zu k�nnen!");
		return false;
	}
	else
	{
		return true;
	}
}

//-->
</script>
<form method="post" action="<? echo PFAD_ROOT ?>Order" name="formOrder" onSubmit="return checkAgb();">
<input type="hidden" name="pdf_filename" value="<? echo $dateiname; ?>">
<?php if ($v->qr_active=="1"&&$v->qr_back==1)
{
?>
<input type="hidden" name="pdf_filename_qr" value="<? echo $dateinameQr; ?>">
<?php
}
?>
<table cellpadding="0" cellspacing="0">
<tr class="shoptabhead">
<td width="600" colspan="4"><strong>Bitte &uuml;berpr&uuml;fen Sie die Druckvorlage:</strong>&nbsp;[<a href="<? echo PFAD_ROOT ?><? echo $dateiname ?>" target="_blank">in neuem Fenster &ouml;ffnen</a>]</td>
</tr>
    <tr>
      <td colspan="4" bgcolor="#FFFFFF" height="20"><iframe src="<? echo PFAD_ROOT ?><? echo $dateiname ?>" style="width:100%;height:250px;"></iframe></td>
    </tr>
<?php
if($v->qr_active=="1"&&$v->qr_back==1){
?>
    <tr class="shoptabhead">
		<td width="600" colspan="4"><strong>R�ckseite:</strong>&nbsp;[<a href="<? echo PFAD_ROOT ?><? echo $dateinameQr ?>" target="_blank">in neuem Fenster &ouml;ffnen</a>]</td>
	</tr>
    <tr>
      <td colspan="4" bgcolor="#FFFFFF" height="20"><iframe src="<? echo PFAD_ROOT ?><? echo $dateinameQr ?>" style="width:100%;height:250px;"></iframe></td>
    </tr>
<?php  } ?>

    <tr>
      <td colspan="4" bgcolor="#FFFFFF" height="20">&nbsp;<input type="button" onclick="self.location.href='<? echo PFAD_ROOT; ?>Anpassen?r=<? echo md5(microtime()); ?>';" name="submit" value="Daten korrigieren"></td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0">
<tr class="shoptabhead">
<td width="600" colspan="2"><strong>Bitte geben Sie Ihre Bestelldaten an:</strong></td>
</tr>


    <tr>
      <td>Wie viele Karten m�chten Sie bestellen?&nbsp;</td>
      <td><select size="1" name="anzahl">
    <option value="100">100</option>
    <option value="200">200</option>
    <option value="300">300</option>
    <option value="400">400</option>
    <option value="500">500</option>
    <option value="600">600</option>
    <option value="700">700</option>
    <option value="800">800</option>
    <option value="900">900</option>
    <option value="1000">1000</option>
  </select></td>
    </tr>
    <tr>
      <td>Bemerkungen:&nbsp;</td>
      <td><textarea style="width:400px;height:80px;" name="bemerkungen"></textarea></td>
    </tr>
    <tr>
      <td>Rechnungsadresse:&nbsp;</td>
      <td><textarea style="width:400px;height:80px;font-family:Arial;" name="rechnungsadresse">
<?
if (!empty($v->reAdresse)) echo webhelper::niceHtml($v->reAdresse);
else
{
?>
<? echo $_SESSION['sess_fe_firmenname'] . "\r\n"; ?>
<? echo $_SESSION['sess_fe_ansprechpartner'] . "\r\n";; ?>
<? echo $_SESSION['sess_fe_strasse'] . "\r\n";; ?>
<? echo $_SESSION['sess_fe_ort']; ?>

<?
}
 ?>
      </textarea></td>
    </tr>
    <tr>
      <td>Lieferadresse:<br>(falls abweichend)&nbsp;</td>
      <td><textarea style="width:400px;height:80px;font-family:Arial;" name="lieferadresse"></textarea></td>
    </tr>
    <tr>
      <td>Name der Bestellung:<br>(f&uuml;r sp&auml;tere Nachbestellungen)&nbsp;</td>
      <td><input type="text" name="nb_name" value="" style="width:400px;"></td>
    </tr>
    <tr>
      <td colspan="2"><input type="checkbox" name="agb_accept"> Hiermit bestelle ich verbindlich die von mir erstellte Visitenkarte. Ich habe die Druckvorlage �berpr�ft und sie kann ohne �nderungen an die Druckmaschine �bergeben werden. Die &gt;&nbsp;<a href="http://www.staats.de/agbs.html" target="_blank">AGB</a>&nbsp;&lt; der Firma Staats sind mir bekannt und ich habe sie akzeptiert.</td>
    </tr>
    <tr>
      <td colspan="4" bgcolor="#FFFFFF" height="20">&nbsp;<input type="submit" name="submit" value="Bestellung durchf&uuml;hren"></td>
    </tr>
</table>
</form>