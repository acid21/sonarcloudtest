<?php

use setasign\Fpdi\Fpdi;
use \setasign\Fpdi\PdfParser\StreamReader;

require_once('lib/fpdf/fpdf.php');
require_once('lib/setasign/Fpdi/src/autoload.php');


$url1 = $_GET["a"];
$url2 = $_GET["b"];

function merge($uri1, $uri2)
{
    $pdf = new Fpdi();
    $pdf->AddPage();

    // background
    $pdfString1 = file_get_contents($uri1);
    $pdf->setSourceFile(StreamReader::createByString($pdfString1));
    $tpl = $pdf->importPage(1);
    $pdf->useTemplate($tpl, ['adjustPageSize' => true]);

    // foreground
    $pdfString2 = file_get_contents($uri2);
    $pdf->setSourceFile(StreamReader::createByString($pdfString2));
    $tpl = $pdf->importPage(1);
    $pdf->useTemplate($tpl, ['adjustPageSize' => true]);

    $pdf->Output();
}
merge($url1,$url2);

// Example:
//http://localhost/pdfmerger/index.php?url1=http://www2.staats.de/printShop/bg/298_297_VK_VS.pdf&url2=http://www2.staats.de/printShop/temp/pdf_e15f2247b230c42c3268d6a1706e3d2f.pdf
?>